from item import Item
from phone import Phone
from keyboard import Keyboard

item1 = Keyboard("jscKeyboard", 1000, 3)

""" # Setting an Attribute
item1.name = "OtherItem"

# Getting an Attribute
print(item1.name)

item1.apply_increment(0.2)
item1.apply_discount()
print(item1.price) """

#item1.send_email()

item1.apply_discount()
print(item1.price)
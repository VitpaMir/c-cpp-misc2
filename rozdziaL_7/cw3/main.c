#include <stdio.h>
#include <stdlib.h>

int main()
{
    int waga, wzrost;

    scanf("%d %d", &waga, &wzrost);
    if  (waga < 45)
    {
        if (wzrost >= 182)
            printf("Jestes bardzo wysoki jak na swoja wage.\n");
        else if (wzrost < 182 && wzrost > 163)
            printf("Jestes wysoki jak na swoja wage.\n");
    } else if (waga < 135 && waga >= 45)
    {
        if (wzrost < 122)
            printf("Jestes dosc niski jak na swoja wage.\n");
        else
            printf("Twoja waga jest idealna.\n");
    }
    return 0;
}

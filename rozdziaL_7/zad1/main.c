#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
int main()
{
    char ch;
    int odstepy = 0;
    int nowalinia = 0;
    int wszznaki = 0;
    printf("Podaj jakies zdanie.\n");
    while ((ch = getchar()) != '#')
    {
        if (ch == ' ')
            odstepy++;
        if (ch == '\n')
            nowalinia++;
            wszznaki++;
    }
    printf("Liczba znakow %d, liczba odstepow %d i "
           "liczba znakow nowej linii %d\n", wszznaki, odstepy, nowalinia);
    return 0;
}

#include <stdio.h>
#include <stdlib.h>

#define WOLNY 17850.00
#define GLOWA 23900.00
#define WSPOLNE 29750.00
#define OSOBNE 14875.00
#define TAX1 0.15
#define TAX2 0.28

int main()
{
    int dochod;
    int podstawa;
    float podatek;
    char ch;

    printf("Program obliczajacy podatek dochodowy z 1988.\n");
    printf("Podaj swoj stan, a nastepnie podaj swoj dochod:\n");
    printf("1 - stan wolny, 2 - glowa rodziny,\n"
           "3 - malzenstwo rozliczenie wspolne, 4 - malzenstwo rozliczenie osobne\n");
    printf("Aby zakonczyc wpisz 5\n");
    while ((ch = getchar()) != '5')
    {
        switch (ch)
        {
        case '1' :
            podstawa = WOLNY;
            break;
        case '2' :
            podstawa = GLOWA;
            break;
        case '3' :
            podstawa = WSPOLNE;
            break;
        case '4' :
            podstawa = OSOBNE;
            break;
        default :
            printf("Wybierz od 1 do 5.\n");
        }
        printf("Teraz wpisz dochod, aby zmienic stan wpisz q.\n");
        while (scanf("%d", &dochod) == 1)
        {
            if (dochod <= podstawa)
            {
                podatek = TAX1 * (float) dochod;
            }
            else
                podatek = TAX1 * (float) dochod + TAX2 * ( (float) dochod - podstawa);
            printf("Twoj podatek to %.2f $.\n", podatek);
            printf("Wpisz q, aby wrocic do menu wyboru stanu.\n");
        }
        while (getchar() != '\n')
            continue;
        printf("Podaj swoj stan, a nastepnie podaj swoj dochod:\n");
        printf("1 - stan wolny, 2 - glowa rodziny,\n"
           "3 - malzenstwo rozliczenie wspolne, 4 - malzenstwo rozliczenie osobne\n");
        printf("Aby zakonczyc wpisz 5\n");
    }

    return 0;
}

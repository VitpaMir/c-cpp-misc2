#include <stdio.h>
#include <stdlib.h>

int main()
{
    char ch;
    int zmiany = 0;
    while ((ch = getchar()) != '#')
    {
        if (ch == '.')
        {
            putchar('!');
            zmiany++;
        }
        else if (ch == '!')
            {
                putchar('!');
                putchar('!');
                zmiany++;
            }
        else
            putchar(ch);
    }
    printf("\nDokonano %d zmian.\n", zmiany);
    return 0;
}

#include <stdio.h>
#include <stdlib.h>

int main()
{
    char ch;
    int zmiany = 0;
    printf("Podaj zdanie wykrzyknikowe i refleksyjne z wieloma kropkami.\n");
    while ((ch = getchar()) != '#')
    {
        switch (ch)
        {
            case '!' :
                putchar('!');
                putchar('!');
                zmiany++;
                break;
            case '.' :
                putchar('!');
                zmiany++;
                break;
            default :
                putchar(ch);
        }
    }
    printf("\nDokonano %d zmian.\n", zmiany);
    return 0;
}

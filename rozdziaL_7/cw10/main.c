#include <stdio.h>
#include <stdlib.h>

int main()
{
    char ch;

    while ((ch = getchar()) != '#')
    {
        if (ch != '\n')
        {
            printf("Krok 1\n");
            if (ch == 'b')
                break;
            else if (ch != 'c')
            {
                if (ch != 'g')
                    printf("Krok 2\n");
                printf("Krok 3\n");
            }

        }
    }
    printf("Koniec\n");
    return 0;
}

#include <stdio.h>
#include <stdlib.h>

int main()
{
    char ch;
    int znak = 0;
    printf("Podaj zdanie, zako�cz znakiem #.\n");
    while ((ch = getchar()) != '#')
    {
        znak++;
        printf("%d - %c ", ch, ch);
        if (znak % 8 == 0)
        {
            putchar('\n');
        }
    }
    return 0;
}

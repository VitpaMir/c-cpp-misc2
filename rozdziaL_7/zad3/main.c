#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int main()
{
    int liczba;
    int iloscp = 0;
    int iloscn = 0;
    float srednian = 0.0;
    float sredniap = 0.0;
    int suman = 0;
    int sumap = 0;
    printf("Podaj liczby calkowite, konczac zerem:\n");
    while (scanf("%d", &liczba) == 1)
    {
        if (liczba % 2 == 0)
        {
            iloscp++;
            sumap += liczba;
        }
        if (liczba % 2 != 0)
        {
            iloscn++;
            suman += liczba;
        }
        if (liczba == 0)
            break;
    }
    srednian = (float) suman/iloscn;
    sredniap = (float) sumap/iloscp;
    printf("W zdaniu jest %d liczb parzystych i ich srednia to %.2f "
           "i %d liczb nieparzystych i ich srednia to %.2f.\n", iloscp, sredniap, iloscn , srednian);
    return 0;
}

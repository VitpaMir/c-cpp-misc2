#include <stdio.h>
#include <stdlib.h>
#define STAWKA1 0.85
#define STAWKA2 0.8
#define STAWKA3 0.75

int main()
{
    int godzinypr;      // godziny pracy w tygodniu
    int podstawa = 40; //zl/godzine
    float podatek;
    float wynnetto;
    float wynnbrutto;
    printf("Podaj liczbe przepracowanych godzin:\n");
    printf("Aby zakonczyc wpisz q.\n");
    while (scanf("%d", &godzinypr) == 1)
    {
        if (godzinypr > 40)
        {
            wynnbrutto = 40 * podstawa;
            wynnbrutto += ((float) godzinypr - 40) * podstawa * 1.5;
            if (wynnbrutto <= 1200)
                wynnetto = wynnbrutto * STAWKA1;
            else if (wynnbrutto > 1200 && wynnbrutto <= 1800)
                wynnetto = 1200.0 * STAWKA1 + (wynnbrutto - 1200.0) * STAWKA2;
            else
                wynnetto = 1200.0 * STAWKA1 + 600.0 * STAWKA2 +
                                            (wynnbrutto - 1800.0) * STAWKA3;
            podatek = wynnbrutto - wynnetto;
        }
        else
            {
                wynnbrutto = (float) godzinypr * podstawa;
                if (wynnbrutto <= 1200)
                    wynnetto = wynnbrutto * STAWKA1;
                else
                    wynnetto = 1200.0 * STAWKA1 + (wynnbrutto - 1200.0) * STAWKA2;
                podatek = wynnbrutto - wynnetto;
            }
        printf("Przepracowales %d godzin, nalezy Ci si� wiec %.2f na reke, \n"
               "mimo ze brutto masz %.2f, bo placisz %.2f podatku.\n",
               godzinypr, wynnetto, wynnbrutto, podatek);
    }

    return 0;
}

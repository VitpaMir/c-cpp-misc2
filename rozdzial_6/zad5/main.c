#include <stdio.h>
#include <stdlib.h>

int main()
{
    int pierwsza;
    int ostatnia;
    int licznik;
    int n2;
    int n3;
    printf("Wpisz dwie liczby poczatka i konca tabeli kwafratow i szescianow: \n");
    scanf("%d %d", &pierwsza, &ostatnia);
    printf("Calkowita    kwadrat     szescian\n");
    for (licznik = pierwsza; licznik <= ostatnia; licznik++)
    {
        n2 = licznik * licznik;
        n3 = licznik * n2;
        printf("%8d %8d %12d\n", licznik, n2, n3);
    }
    return 0;
}

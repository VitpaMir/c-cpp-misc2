#include <stdio.h>
#include <stdlib.h>

int main()
{
    float pierwsza;
    float druga;
    float wynik;
    printf("Podaj dwie liczby zmiennoprzecinkowe, (ulamki dziesietne w sensie): \n");
    printf("Aby zakonczyc wpisz q.\n");
    while (scanf("%f %f", &pierwsza, &druga))
    {
        wynik = (pierwsza-druga)/(pierwsza*druga);
        printf("%.2f\n", wynik);
        printf("Dawaj dalej, jesli nie to wpisz q.\n");
    }
    return 0;
}

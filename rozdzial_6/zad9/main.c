#include <stdio.h>
#include <stdlib.h>
#define LICZBA 8
int main()
{
    int i;
    int tablica[LICZBA];
    printf("Wpisz osiem liczb calkowitych:\n");
    for (i = 0; i < LICZBA; i++)
        scanf("%d", &tablica[i]);
    printf("Normalny szyk:\n");
    for (i = 0; i < LICZBA; i++)
        printf("%d ", tablica[i]);
    printf("\nOdwrotna kolejnosc:\n");
    for (i = LICZBA - 1; i >= 0; i--)
        printf("%d ", tablica[i]);
    return 0;
}

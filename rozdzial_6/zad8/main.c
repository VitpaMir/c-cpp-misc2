#include <stdio.h>
#include <stdlib.h>
float podziel (float a, float b);

int main()
{
    float pierwsza;
    float druga;
    float wynik;
    printf("Podaj dwie liczby zmiennoprzecinkowe, (ulamki dziesietne w sensie): \n");
    printf("Aby zakonczyc wpisz q.\n");
    while (scanf("%f %f", &pierwsza, &druga))
    {
        wynik = podziel(pierwsza, druga);
        printf("%.2f\n", wynik);
        printf("Dawaj dalej, jesli nie to wpisz q.\n");
    }
    return 0;
}

float podziel (float a, float b)
{
    float wynik;
    wynik = (a-b)/(a*b);
    return wynik;
}

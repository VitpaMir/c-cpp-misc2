#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define ROZMIAR 40
int main()
{
    char linijka [ROZMIAR];
    char znaki;
    int i = 0;
    printf("Wpisz jakas linijke\n");\
    do
    {
        scanf("%c", &znaki);
        linijka[i] = znaki;
        i++;
    }
    while (znaki != '\n');
    for (i = strlen(linijka) - 1; i >= 0; i--)
        printf("%c", linijka[i]);
    return 0;
}

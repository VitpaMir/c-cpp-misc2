#include <stdio.h>
#include <stdlib.h>

int main()
{
    int i = 0; // lata
    float wklad_ewa = 100.0;
    float prp = 0.1;
    float kasia = 100.0;
    float ewa = 100.0;
    float prs = 0.05;
    printf("Ewa         Kasia       lata\n");
    do
    {
        ewa += prp*wklad_ewa;
        kasia += kasia*prs;
        i++;
        printf("%.2f        %.2f        %d\n", ewa, kasia, i);
    }
    while (ewa >= kasia);
    printf("Inwestycja Kasi przekroczy wartosc inwestycji Ewy po %d latach.\n", i);
    printf("Ewa = %.2f, Kasia = %.2f", ewa, kasia);
    return 0;
}

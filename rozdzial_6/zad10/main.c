#include <stdio.h>
#include <stdlib.h>

int main()
{
    int liczba;
    int i;
    int ujemna;
    float wynik1 = 0.0;
    float wynik2 = 0.0;

    printf("Podaj liczbe wyrazow ciagu:\n");
    scanf("%d", &liczba);
    printf("pierwsza    druga       licznik\n");

    for (i = 1, ujemna = -1; i <= liczba; i++, ujemna *= -1)
        {
            wynik1 += 1.0/i;
            wynik2 += 1.0/i*(-1)*ujemna;
            printf("%f     %f      %d\n", wynik1, wynik2, i);
        }

    printf("\nWyniki calosci sum:\n%f\n%f\n", wynik1, wynik2);

    return 0;
}

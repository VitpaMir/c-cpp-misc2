#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#define MAXTYT 40
#define MAXAUT 40
#define MAXKS 10
struct ksiazka
{
    char tytul[MAXTYT];
    char autor[MAXAUT];
    float wartosc;
};

int get_answer();
void show_books(struct ksiazka * list);
void add_book(struct ksiazka * list);
void delete_book (struct ksiazka * list);
void modify_book (struct ksiazka * list);

int main ()
{
    struct ksiazka bibl[MAXKS];
    
    char wybor;
    puts("Witaj w spisie ksiazek");
    while ((wybor = get_answer()) != 'q')
    {
        switch (wybor)
        {
        case 'a':
            add_book(bibl);
            break;
        case 'b':
            delete_book(bibl);
            break;
        case 'c' :
            show_books(bibl);
            break;
        case 'd' :
            modify_book(bibl);
            break;
        }
    }
    return 0;
}

int get_answer()
{
    int ans;
    printf("Wybierz opcje:\n");
    printf("A) Dodaj ksiazke    B) Usun ksiazke\n");
    printf("C) Wyswietl liste   D) Zmodyfikuj pozycje\n");
    printf("Q) KONIEC\n");
    ans = getchar();
    ans = tolower(ans);
    while (getchar() != '\n')
        continue;
    while ( (ans < 'a' || ans > 'd') && ans != 'q')
    {
        printf("Wpisz a, b, c, d lub q\n");
        ans = tolower(getchar());
        while (getchar() != '\n')
            continue;
    }
    return ans;
}

void show_books(struct ksiazka * list)
{
    int index;
    int licznik = 0;
    FILE *pksiazki;
    int rozmiar = sizeof (struct ksiazka);
    puts("Oto lista Twoich ksiazek:");
    if ((pksiazki = fopen("ksiazki.dat", "r+b")) == NULL)
    {
        fputs("Nie moge otworzyc pliku ksiazki.dat, bo jeszcze go nie utworzono, dodaj najpierw ksiazki do listy.\n", stderr);
        return;
    }
    rewind(pksiazki);
    while (licznik < MAXKS && fread(&list[licznik], rozmiar, 1, pksiazki) == 1)
        licznik++;
    rewind(pksiazki);
    if (licznik > 0 && licznik < 5)
        printf("Obecnie lista zawiera %d pozycje.\n", licznik);
    else if (licznik >= 5)
        printf("Obecnie lista zawiera %d pozycji.\n", licznik);
    for (index = 0; index < licznik; index++)
        printf("%d. %s, autor: %s, cena: %.2f zl\n", index + 1, list[index].tytul, list[index].autor, list[index].wartosc);
    fclose(pksiazki);
}

void add_book(struct ksiazka * list)
{
    FILE *pksiazki;
    int licznik = 0;
    int licznikp;
    int rozmiar = sizeof (struct ksiazka);
    if ((pksiazki = fopen("ksiazki.dat", "a+b")) == NULL)
    {
        fputs("Nie moge otworzyc pliku ksiazki.dat", stderr);
        exit(1);
    }
    rewind(pksiazki);
    while (licznik < MAXKS && fread(&list[licznik], rozmiar, 1, pksiazki) == 1)
        licznik++;
    rewind(pksiazki);
    licznikp = licznik;
    if (licznik == MAXKS)
    {
        fputs("Plik ksiazki.dat jest pelny.\n",stderr);
        return;
    }
    puts("Podaj nowe tytuly ksiazek.");
    puts("Aby zakonczyc, wcisnij [enter] na poczatku wiersza.");
    while (licznik < MAXKS && gets(list[licznik].tytul) != NULL && list[licznik].tytul[0] != '\0')
    {
        puts("Podaj autora.");
        gets(list[licznik].autor);
        puts("Teraz podaj wartosc.");
        scanf("%f", &list[licznik++].wartosc);
        while (getchar() != '\n')
            continue;
        if (licznik < MAXKS)
            puts("Podaj nastepny tytul.");
    }
    fwrite(&list[licznikp], rozmiar, licznik - licznikp, pksiazki);
    fclose(pksiazki);
}

void delete_book (struct ksiazka * list)
{
    FILE *pksiazki;
    int i, ind;
    int licznik = 0;
    int rozmiar = sizeof (struct ksiazka);
    if ((pksiazki = fopen("ksiazki.dat", "r+b")) == NULL)
    {
        fputs("Nie moge otworzyc pliku ksiazki.dat.", stderr);
        exit(1);
    }
    rewind(pksiazki);
    while (licznik < MAXKS && fread(&list[licznik], rozmiar, 1, pksiazki) == 1)
        licznik++;
    rewind(pksiazki);
    fclose(pksiazki);
    if ((pksiazki = fopen("ksiazki.dat", "w+b")) == NULL)
    {
        fputs("Nie moge otworzyc pliku ksiazki.dat.", stderr);
        exit(1);
    }
    puts("Podaj indeks ksiazek do usuniecia z listy.");
    puts("Aby zrezygnowac, wpisz q.");
    if (scanf("%d", &ind) == 0)
    {
        while (getchar() != '\n')
            continue;
        return;
    }
    printf("Usuwasz pozycje %d o indeksie %d, czyli %s, autora %s, o cenie %.2f.\n", ind, ind - 1,list[ind - 1].tytul, list[ind - 1].autor, list[ind - 1].wartosc);
    /* printf("Czy chcesz kontynuowac? Wpisz t - jesli tak, inna litere - jesli chcesz zrezygnowac i wrocic do menu.\n");
    if (getchar() == 't')
        continue;
    else
        break; */
    list[ind - 1].tytul[0] = '\0';
    list[ind - 1].autor[0] = '\0';
    list[ind - 1].wartosc = 0;
    for (i = ind - 1; i < licznik; i++)
    {
            strcpy(list[i].tytul, list[i + 1].tytul);
            list[i + 1].tytul[0] = '\0';
            strcpy(list[i].autor, list[i + 1].autor);
            list[i + 1].autor[0] = '\0';
            list[i].wartosc = list[i + 1].wartosc;
            list[i + 1].wartosc = 0;
    }
    printf("Wyglad struktury.\n");
    for (i = 0; i < licznik; i++)
    {
            printf("%d. %s, autora %s, o cenie %.2f.\n", i + 1, list[i].tytul, list[i].autor, list[i].wartosc);
    }
    puts("Usunieto.");
    while (getchar() != '\n')
        continue;
    fwrite(&list[0], rozmiar, licznik - 1, pksiazki);
    fclose(pksiazki);
}

void modify_book (struct ksiazka * list)
{
    FILE *pksiazki;
    int i, ind;
    int licznik = 0;
    char ans_ch;
    int rozmiar = sizeof (struct ksiazka);
    char n_tytul[MAXTYT];
    char n_autor[MAXAUT];
    float n_wartosc = 0.0;
    if ((pksiazki = fopen("ksiazki.dat", "r+b")) == NULL)
    {
        fputs("Nie moge otworzyc pliku ksiazki.dat", stderr);
        exit(1);
    }
    rewind(pksiazki);
    while (licznik < MAXKS && fread(&list[licznik], rozmiar, 1, pksiazki) == 1)
        licznik++;
    rewind(pksiazki);
    fclose(pksiazki);
    if ((pksiazki = fopen("ksiazki.dat", "w+b")) == NULL)
    {
        fputs("Nie moge otworzyc pliku ksiazki.dat.", stderr);
        exit(1);
    }
    puts("Podaj indeks ksiazki do modyfikacji z listy.");
    puts("Aby zrezygnowac, wpisz q.");
    scanf("%d", &ind);
    while (getchar() != '\n')
        continue;
    puts("Co chcesz zmienic? Wpisz t, jesli tytul, a - autora, w - wartosc lub r - jesli chcesz caly rekord.");
    scanf("%c", &ans_ch);
    while (getchar() != '\n')
        continue;
    if (ans_ch == 't')
    {
        list[ind - 1].tytul[0] = '\0';
        puts("Podaj nowy tytul.");
        gets(list[ind - 1].tytul);
    }
    else if (ans_ch == 'a')
    {
        list[ind - 1].autor[0] = '\0';
        puts("Podaj nowego autora.");
        gets(list[ind - 1].autor);
    }
    else if (ans_ch == 'w')
    {
        list[ind - 1].wartosc = 0;
        puts("Podaj nowa wartosc.");
        scanf("%f", &list[ind - 1].wartosc);
        while (getchar() != '\n')
            continue;
    }
    else if (ans_ch == 'r')
    {
        list[ind - 1].tytul[0] = '\0';
        list[ind - 1].autor[0] = '\0';
        list[ind - 1].wartosc = 0;
        puts("Podaj nowy tytul.");
        gets(list[ind - 1].tytul);
        puts("Podaj autora.");
        gets(list[ind - 1].autor);
        puts("Teraz podaj wartosc.");
        scanf("%f", &list[ind - 1].wartosc);
        while (getchar() != '\n')
            continue;
    }
    else
        printf("Wystapil blad sprobuj ponownie, wpisz t, a, w lub r.\n");
    fwrite(&list[0], rozmiar, licznik, pksiazki);
    fclose(pksiazki);
}
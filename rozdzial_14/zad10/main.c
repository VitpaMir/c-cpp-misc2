#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#define MAXL 40
#define YES 1
#define NO 0
#define MAXP 12
struct Person_data
{
    int id;
    char name[MAXL];
    char surname[MAXL];
    int flag;
};
void srtStruct(struct Person_data * ptrStr, int num);

int main()
{
    int i;
    struct Person_data grupa[MAXP] = 
    {
        {4, "Adrianna", "Kowalska", YES},
        {2, "Joanna", "Nowak", YES},
        {5, "Slawomir", "Nowakowski", YES},
        {1, "Irena", "Kawon", YES},
        {8, "Aaron", "Kowalski", YES},
        {9, "Pawel", "Nowy", YES},
        {10, "Igor", "Lelkowski", YES},
        {11, "Kamil", "Jakubowski", YES},
        {6, "Johny", "Krzysiek", YES},
        {12, "Czeslaw", "Zul", YES},
        {3, "Tomasz", "Komar", YES},
        {7, "Ilona", "Izyk", YES}
    };
    struct Person_data * wsklan;
    wsklan = grupa;
    srtStruct(wsklan, MAXP);
    printf("Poza funkcja.\n");
    for (i = 0; i < MAXP; i++)
        if (grupa[i].flag == YES)
            printf("%d. %s %s\n", grupa[i].id, grupa[i].name, grupa[i].surname);
    printf("Koniec programu.\n");
    return 0;
}

void srtStruct(struct Person_data * ptrStr, int num)
{
    struct Person_data temp;
    int dol, szuk;
    for (dol = 0; dol < num - 1; dol++)
    {
        for (szuk = dol + 1; szuk < num; szuk++)
        {
            if (strcmp(ptrStr[dol].name, ptrStr[szuk].name) > 0)
            {
                temp.flag = ptrStr[dol].flag;
                strcpy(temp.name, ptrStr[dol].name);
                strcpy(temp.surname, ptrStr[dol].surname);
                temp.id = ptrStr[dol].id;

                ptrStr[dol].flag = ptrStr[szuk].flag;
                strcpy(ptrStr[dol].name, ptrStr[szuk].name);
                strcpy(ptrStr[dol].surname, ptrStr[szuk].surname);
                ptrStr[dol].id = ptrStr[szuk].id;

                ptrStr[szuk].flag = temp.flag;
                strcpy(ptrStr[szuk].name, temp.name);
                strcpy(ptrStr[szuk].surname, temp.surname);
                ptrStr[szuk].id = temp.id;
            }
        }
    }
}
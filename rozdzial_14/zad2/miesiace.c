struct miesiac {
    char nazwa_miesiaca[12];
    char skrot[4];
    int liczba_dni;
    int numer_m;
};
struct Pawel {
    char imie[12];
    char nazwa[4];
};
struct miesiac miesiace[12] = {
    {"Styczen", "STY", 31, 1},
    {"Luty", "LUT", 28, 2},
    {"Marzec", "MAR", 31, 3},
    {"Kwiecien", "KWI", 30, 4},
    {"Maj", "MAJ", 31, 5},
    {"Czerwiec", "CZE", 30, 6},
    {"Lipiec", "LIP", 31, 7},
    {"Sierpien", "SIE", 31, 8},
    {"Wrzesien", "WRZ", 30, 9},
    {"Pazdziernik", "PAZ", 31, 10},
    {"Listopad", "LIS", 30, 11},
    {"Grudzien", "GRU", 31, 12},
};
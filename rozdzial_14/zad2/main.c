#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "miesiace.c"
int liczba_dni2(char nazwa_m[], int numer_d);

int main()
{   
    int dzien;
    char miesiac[12];
    int rok;
    printf("Podaj miesiac.\n");
    scanf("%s", &miesiac);
    printf("Podaj dzien.\n");
    scanf("%d", &dzien);
    printf("Podaj rok.\n");
    scanf("%d", &rok);
    liczba_dni2(miesiac, dzien);
    printf("%d\n", liczba_dni2(miesiac, dzien));
    return 0;
}

int liczba_dni2(char nazwa_m[], int numer_d)
{
    int dni;
    int i;
    int numer_m = 0;
    if (numer_d < 1 || numer_d > miesiace->liczba_dni)
    {
        printf("Podano bledna liczbe dni danego miesiaca.\n");
        exit(EXIT_FAILURE);
    }
    if (strlen(nazwa_m) == 3)
    {
        for (i = 0; i < strlen(nazwa_m); i++)
        {
            if (islower(nazwa_m[i]))
                nazwa_m[i] = toupper(nazwa_m[i]);
        }
        printf("%s\n", nazwa_m);
    }
    else if(strlen(nazwa_m) > 3)
    {  
        if(islower(nazwa_m[0]))
            nazwa_m[0] = toupper(nazwa_m[0]);
        for (i = 1; i < strlen(nazwa_m); i++)
        {
            if (isupper(nazwa_m[i]))
                nazwa_m[i] = tolower(nazwa_m[i]);
        }
    }
    for (i = 0, dni = 0; i < 12; i++)
    {
        if((strcmp(miesiace[i].nazwa_miesiaca, nazwa_m) == 0) 
            || (strcmp(miesiace[i].skrot, nazwa_m) == 0) 
            || (miesiace[i].numer_m == atoi(nazwa_m)))
        {
            numer_m = i + 1;
            break;
        }
    }
    for (i = 0, dni = 0; i < numer_m - 1; i++)
        dni += miesiace[i].liczba_dni;
    dni += numer_d;
    return dni;
}
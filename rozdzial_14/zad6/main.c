#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LENGTH 40
#define MAXPL 19

typedef enum {true = 1, false = 0} bool;

struct pers_data
{
    char name[LENGTH];
    char surname[LENGTH];
};
struct player
{
    int number;
    struct pers_data pl_data;
    int throws;
    int hits;
    int assists;
    int fauls;
    float eff;
    bool booked;
};
void show_structlist(const struct player * random);

int main()
{
    FILE *fp;
    int i;

    int index = 0; 
    int throw = 0;
    int hit = 0;
    int assist = 0;
    int faul = 0;
    char temp_name[LENGTH], temp_sname[LENGTH];
    int sum_throws = 0;
    int sum_hits = 0;
    int sum_assists = 0;
    int sum_fauls = 0;
    float team_eff = 0.0;
    struct player list[MAXPL];

    for (i = 0; i < 19; i++)
    {
        list[i].booked = false;
        list[i].number = 0;
        list[i].throws = 0;
        list[i].hits = 0;
        list[i].assists = 0;
        list[i].fauls = 0;
        list[i].eff = 0.0;
    }
    if ((fp = fopen("C:\\Users\\hj61t7\\OneDrive - Aptiv\\Dokumenty\\C\\c-cpp-misc2\\rozdzial_14\\zad6\\staty.txt", "rb")) == NULL)
    {
        fprintf(stderr, "File cannot be open!\n");
        exit(1);
    }
    while (fscanf(fp ,"%d %s %s %d %d %d %d", &index, temp_name, temp_sname, &throw, &hit, &assist, &faul) == 7)
    {
        if (list[index].booked == false && index >= 0 && index <= 18)
        {
            list[index].number = index;
            strcpy(list[index].pl_data.name, temp_name);
            strcpy(list[index].pl_data.surname, temp_sname);
            list[index].throws = throw;
            list[index].hits = hit;
            list[index].assists = assist;
            list[index].fauls = faul;
            list[index].booked = true;
        }
        else
        {
            for (i = 0; i < 19; i++)
            {
                if (list[index].booked = true && list[i].number == index && strcmp(list[i].pl_data.name, temp_name) == 0 && strcmp(list[i].pl_data.surname, temp_sname) == 0)
                {
                    list[index].throws += throw;
                    list[index].hits += hit;
                    list[index].assists += assist;
                    list[index].fauls += faul;
                }
            }
        }
    }
    for (i = 0; i < 19; i++)
    {
        list[i].eff = (float) list[i].hits / (float) list[i].throws;
        if (list[i].throws == 0)
            list[i].eff = 0.0;
    }
    /* for (i = 0; i < 19; i++)
    {
        printf("%d %s %s %d %d %d %d Eff: %.2f\n", list[i].number, list[i].pl_data.name, 
        list[i].pl_data.surname, list[i].throws, list[i].hits, list[i].assists, list[i].fauls, list[i].eff);
    } */
    show_structlist(list);
    for (i = 0; i < 19; i++)
    {
        sum_throws += list[i].throws;
        sum_hits += list[i].hits;
        sum_assists += list[i].assists;
        sum_fauls += list[i].fauls;
        team_eff = (float) sum_hits / sum_throws;
    }
    printf("%6s %10s %12s %10d %10d %10d %10d %15.2f\n", "OV", " ", " ", sum_throws, sum_hits, sum_assists, sum_fauls, team_eff);
    if (fclose(fp) != 0)
    {
        fprintf(stderr, "File cannot be saved properly!\n");
        exit(1);
    }
    return 0;
}

void show_structlist(const struct player * random)
{
    int i;
    printf("%s %10s %12s %10s %10s %10s %10s %15s\n", "Number", "Name", "Surname", "Throws", "Hits", "Assists", "Fauls", "Efficiency");
    for (i = 0; i < 19; i++)
    {
        printf("%6d %10s %12s %10d %10d %10d %10d %15.2f\n", random[i].number, random[i].pl_data.name, 
        random[i].pl_data.surname, random[i].throws, random[i].hits, random[i].assists, random[i].fauls, random[i].eff);
    }
}
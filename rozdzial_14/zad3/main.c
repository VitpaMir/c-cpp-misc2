#include <stdio.h>
#define MAXTYT 40
#define MAXAUT 40
#define MAXKS 100
//void sortuj( struct ksiazka * biblioteka, int rozmiar);
struct ksiazka
{
    char tytul[MAXTYT];
    char autor[MAXAUT];
    float wartosc;
};

int main()
{
    struct ksiazka bibl[MAXKS];
    int licznik = 0;
    int index;
    float suma = 0;

    printf("Podaj tytul ksiazki.\n");
    printf("Aby zakonczyc, wcisnij [enter]  na poczatku wiersza.\n");
    while (licznik < MAXKS && gets(bibl[licznik].tytul) != NULL
                             && bibl[licznik].tytul[0] != '\0')
    {
        printf("Teraz podaj autora.\n");
        gets(bibl[licznik].autor);
        printf("Teraz podaj wartosc.\n");
        scanf("%f", &bibl[licznik++].wartosc);
        while (getchar() != '\n')
            continue;
        if (licznik < MAXKS)
            printf("Podaj kolejny tytul.\n");
    }
    for(int i = 0; i < licznik - 1; i++)
    {
        int j;
        struct ksiazka temp;
        for (j = i + 1; j < licznik; j++)
        {
            if (bibl[j].tytul[0] < bibl[i].tytul[0])
            {
                temp = bibl[i];
                bibl[i] = bibl[j];
                bibl[j] = temp;
            }
        }
    }
    for (int i = 0; i < licznik; i++)
        suma += bibl[i].wartosc;
    
    printf("Oto lista Twoich ksiazek:\n");
    for (index = 0; index < licznik; index++)
        printf("%s, autor: %s, cena: %.2f zl\n", 
                    bibl[index].tytul, bibl[index].autor, bibl[index].wartosc);
    printf("Suma wartosci podanych ksiazek: %.2f\n", suma);
    return 0;
}

/* void sortuj( struct ksiazka * biblioteka, int rozmiar)
{
    int i, j;
    char *temp;
    printf("%c\n", biblioteka + 0);
    printf("%c\n", biblioteka + 1);
    printf("%c\n", biblioteka + 2);
    printf("%c\n", biblioteka + 3);

    for(i = 0; i < rozmiar - 1; i++)
    {
        for (j = i + 1; j < rozmiar; j++)
        {
            if (biblioteka + i > biblioteka[j])
            {
                temp = biblioteka[i];
                biblioteka[i] = biblioteka[j];
                biblioteka[j] = temp;
            }
        }
    } 
} */
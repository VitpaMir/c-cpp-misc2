#include <stdio.h>
struct paliwo kml_licz(struct paliwo struktura);

struct paliwo
{
    float odleglosc;
    float litry;
    float kml;
};

int main()
{
    struct paliwo nowa = 
    {
        712.0,
        60.6,
    };
    nowa = kml_licz(nowa);
    printf("%.2f %.2f %.2f\n",
             nowa.odleglosc, nowa.litry, nowa.kml);
    return 0;
}

struct paliwo kml_licz(struct paliwo struktura)
{
    struktura.kml = struktura.odleglosc / struktura.litry;
    return struktura;
}
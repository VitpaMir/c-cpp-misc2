/* Plik do zadania 9. Teraz tylko test paru funkcji zadania 8!!!! */


#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#define MAXL 40
#define YES 1
#define NO 0
#define MAXP 12
struct Person_data
{
    int id;
    char name[MAXL];
    char surname[MAXL];
    int flag;
};

void srtStruct(char * ptrStr[], int num);

int main()
{
    int i;
    int licznik = 0;
    struct Person_data passenger[MAXP];
    char *wsklan[MAXP];
    puts("Rejestracja do listy (struktur). Podaj imie.");
    while (licznik < MAXP && gets(passenger[licznik].name) != NULL && passenger[licznik].name[0] != '\0')
    {
        wsklan[licznik] = passenger[licznik].name;
        passenger[licznik].id = licznik + 1;
        passenger[licznik].flag = YES;
        puts("Teraz podaj nazwisko.");
        gets(passenger[licznik++].surname);
        if (licznik < MAXP)
            puts("Podaj nastepnego pasazera (tylko dla testow).");
    }
    printf("Kolejnosc dodania:\n");
    for (i = 0; i < licznik; i++)
        //if (passenger[i].flag == YES)
        printf("%d. %s %s\n", passenger[i].id, passenger[i].name, passenger[i].surname);
    printf("\n");
    printf("Kolejnosc alfabetyczna\n");
    srtStruct(wsklan, licznik);
    for (i = 0; i < licznik; i++)
        printf("%d. %s %s\n", passenger[i].id, wsklan[i], passenger[i].surname);
    return 0;
}

void srtStruct(char * ptrStr[], int num)
{
    char * temp;
    int dol, szuk;

    for (dol = 0; dol < num - 1; dol++)
    {
        for (szuk = dol + 1; szuk < num; szuk++)
        {
            if (strcmp(ptrStr[dol], ptrStr[szuk]) > 0)
            {
                temp = ptrStr[dol];
                ptrStr[dol] = ptrStr[szuk];
                ptrStr[szuk] = temp;
            }
        }
    }
}
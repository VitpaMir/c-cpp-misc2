#include <stdio.h>
#include "pozaziem.h"
void wyswietl_strukta(const struct bem * wskst);

int main()
{
    struct bem *wb;
    struct bem deb = {
    6,
    {"Berbnazel", "Gwolkapwolk"},
    "Arkturianin"
    };
    wb = &deb;
    printf("%d\n", deb.konczyny);
    printf("%s\n", wb->typ);
    printf("%s\n", wb->typ + 2);
    printf("%s\n%s\n", deb.tytul.nazw, wb->tytul.nazw);
    wyswietl_strukta(&deb);
    return 0;
}

void wyswietl_strukta(const struct bem * wskst)
{
    printf("%s %s jest %sem o %d konczynach.\n", 
            wskst->tytul.imie, wskst->tytul.nazw, wskst->typ, wskst->konczyny);
    printf("%s %s o tak to on\n", wskst->tytul.imie, wskst->tytul.nazw);
}
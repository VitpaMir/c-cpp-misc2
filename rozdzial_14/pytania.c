/* 
1. Nieprawidlowy
2. Wyswietli dane ze struktury za pomoca operatora. i wskaznika
3. struct miesiac {
    char nazwa_miesiaca[20];
    char skrot[3];
    int liczba_dni;
    int numer_m;
};
4. struct miesiac miesiace[12];
5. Jak nizej
6. SOCZEWKA tablica[10];
    tablica[2].ognisk = 500;
    tablica[2].rozwart = 2.0;
    tablica[2].marka = "Remarkatar";
7. a) pierwsza linijka wyswietli liczbe konczyn, druga: wb wskazuje na deb,
 a typ to Arkturianini to by było wyswietlone, trzecia: typ + 2: kturianin.
b) deb.typ lub deb->typ
c) w plikach pozaziem.h i pytanie7.c
8. a) willie.data_ur;
    b) wsk->data_ur;
    c) scanf("%d", &willie.data_ur);
    d) scanf("%d", &wsk->data_ur);
    e) willie.dane.imie[2];
    f) strlen(willie.dane.imie) + strlen(willie.dane.nazwisko)
9.  struct samochod {
    char nazwa_samochodu[10];
    int moc_silnika;
    float zuzycie_na_sto;
    float rozstaw_osi;
    int rocznik;
}
10. W pliku pytaniedziesiate.c
11. char * (*wf)(char * wch, char ch);
12. double fun1(double a, double b)
    double fun2(double c, double d)
    double fun3(double e, double f)
    double fun4(double g, double h)
    double (*wsk[4]) (double i, double j) = {fun1, fun2, fun3, fun4};
    lub typedef double (*wtyp) (double a, double b);
    wtyp wf[4] = {fun1, fun2, fun3, fun4};
*/
//Ponizej odpowiedz na 5:
#include <stdio.h>
#include "miesiace.c"
int liczba_dni(int numer_miesiaca);
extern struct miesiac miesiace[];
int main()
{   
    printf("%d\n", liczba_dni(3));
    return 0;
}

int liczba_dni(int numer_miesiaca)
{
    int dni = 0;
    int i;
    for (i = numer_miesiaca; i < 12; i++)
        dni += miesiace[i].liczba_dni;
    dni = 365 - dni;
    return dni;
}

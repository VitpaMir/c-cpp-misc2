#include <stdio.h>
#define DL 20
#define ROZMIAR 4
#define OCEN 5
struct daneos
{
    char imie[DL];
    char nazwisko[DL];
};
struct student
{
    struct daneos dane;
    float oceny[OCEN];
    float srednia;
};
void pobierz_oceny (const struct student * ws, int n);
float oblicz_srednia (float oceny[], int n);
void wyswietl_info (struct student * ws, int n);

int main()
{
    struct student grupa[ROZMIAR] = 
    {
        {{"Adrianna", "Kowalska"}},
        {{"Joanna", "Nowak"}},
        {{"Slawomir", "Nowakowski"}},
        {{"Irena", "Kawon"}}
    };
    pobierz_oceny(grupa, ROZMIAR);
    for (int i = 0; i < ROZMIAR; i++)
        grupa[i].srednia = oblicz_srednia(grupa[i].oceny, OCEN);
    wyswietl_info(grupa, ROZMIAR);
    return 0;
}

void pobierz_oceny (const struct student * ws, int n)
{
    int i, j;
    printf("Podaj piec ocen danego studenta.\n");
    for (i = 0; i < n; i++, ws++)
    {
        for (j = 0; j < OCEN; j++)
            scanf("%f", &ws->oceny[j]);
        printf("Podaj kolejne piec ocen kolejnego studenta.\n");
    }
}

float oblicz_srednia (float oceny[], int n)
{
    int i;
    float suma;
    for (i = 0, suma = 0.0; i < n; i++)
        suma += oceny[i];
    return suma / OCEN;
}

void wyswietl_info (struct student * ws, int n)
{
    int i, j;
    for (i = 0; i < n; i++, ws++)
    {
        printf("%s ", ws->dane.imie);
        printf("%s, ", ws->dane.nazwisko);
        printf("Oceny:");
        for (j = 0; j < OCEN; j++)
        {
            printf(" ");
            printf("%.1f", ws->oceny[j]);
        }
        printf(" Srednia: %.2f", ws->srednia);
        printf("\n");
    }
}
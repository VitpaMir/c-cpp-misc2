#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#define MAXL 40
#define YES 1
#define NO 0
#define MAXP 12
struct Person_data
{
    int id;
    char name[MAXL];
    char surname[MAXL];
    int flag;
};

int menu();
void check_fileExt();
void empties(struct Person_data * list);
void show_empList(struct Person_data * list);
void make_reserv(struct Person_data * list);
void delete_reserv(struct Person_data * list);
void srtStruct(struct Person_data * ptrStr);

int main()
{
    struct Person_data passenger[MAXP];
    check_fileExt(); //check if file with list exist, if not make empty file with empty list
    char wybor;
    puts("Witaj w lotach malopolskich");

    while ((wybor = menu()) != 'f')
    {
        switch (wybor)
        {
        case 'a':
            empties(passenger);
            break;
        case 'b':
            show_empList(passenger);
            break;
        case 'c' :
            srtStruct(passenger);
            break;
        case 'd' :
            make_reserv(passenger);
            break;
        case 'e' :
            delete_reserv(passenger);
            break;
        }
    }
    return 0;
}

int menu()
{
    int ans;
    printf("Wybierz opcje:\n");
    printf("a) Pokaz liczbe wolnych miejsc\n");
    printf("b) Pokaz liste wolnych miejsc\n");
    printf("c) Pokaz alfabetyczna liste miejsc\n");
    printf("d) Zarezerwuj miejsce dla klienta\n");
    printf("e) Usun rezerwacje miejsca\n");
    printf("f) KONIEC\n");
    ans = getchar();
    ans = tolower(ans);
    while (getchar() != '\n')
        continue;
    while ( (ans < 'a' || ans > 'e') && ans != 'f')
    {
        printf("Wpisz a, b, c, d, e lub f\n");
        ans = tolower(getchar());
        while (getchar() != '\n')
            continue;
    }
    return ans;
}

void check_fileExt()
{
    int i;
    FILE *fp;
    struct Person_data empty[MAXP];
    if ((fp = fopen("passengers.dat", "r+b")) == NULL)
    {
        for (i = 0; i < MAXP; i++)
        {
            empty[i].id = i + 1;
            empty[i].name[0] = '\0';
            empty[i].surname[0] = '\0';
            empty[i].flag = NO;
        }
        fp = fopen("passengers.dat", "wb");
        for (i = 0; i < MAXP; i++)
            fwrite(&empty[i], sizeof(struct Person_data), 1, fp);
    }
    fclose(fp);
}

void empties(struct Person_data * list)
{
    FILE *fp;
    int i;
    int counter = 0;
    if ((fp = fopen("passengers.dat", "r+b")) == NULL)
    {
        fputs("Nie moge otworzyc pliku passengers.dat.\n", stderr);
        return;
    }
    for (i = 0; i < MAXP; i++)
        fread(&list[i], sizeof(struct Person_data), 1, fp);
    for (i = 0; i < MAXP; i++)
    {
        if (list[i].flag == NO)
            counter++;
    }
    if (counter == 0)
        printf("Brak wolnych miejsc.\n");
    else
        printf("Jest %d wolnych miejsc\n", counter);
    fclose(fp);
}

void show_empList(struct Person_data * list)
{
    int i;
    FILE *fp;

    if ((fp = fopen("passengers.dat", "r+b")) == NULL)
    {
        fputs("Nie moge otworzyc pliku passengers.dat.\n", stderr);
        return;
    }
    for (i = 0; i < MAXP; i++)
        fread(&list[i], sizeof(struct Person_data), 1, fp);

    printf("Lista wolnych miejsc:\n");
    for (i = 0; i < MAXP; i++)
        if (list[i].flag == NO)
            printf("%d. %s %s\n", list[i].id, list[i].name, list[i].surname);
    fclose(fp);
}

void make_reserv(struct Person_data * list)
{
    FILE *fp;
    int i, licznik = 0;
    int cnty = 0;
    int id_given = 0;
    if ((fp = fopen("passengers.dat", "r+b")) == NULL)
    {
        fputs("Nie moge otworzyc pliku passengers.dat.\n", stderr);
        return;
    }
    rewind(fp);
    while (licznik < MAXP && fread(&list[licznik], sizeof(struct Person_data), 1, fp) == 1)
    {
        licznik++;
        if (list[licznik].flag == YES)
            cnty++;
    }
    rewind(fp);
    fclose(fp);
    if (cnty == MAXP)
    {
        fputs("Samolot jest pelny.\n",stderr);
        return;
    }
    if ((fp = fopen("passengers.dat", "w+b")) == NULL)
    {
        fputs("Nie moge otworzyc pliku passengers.dat.\n", stderr);
        return;
    }
    if (cnty > 0)
        cnty++;
    puts("Rezerwacja miejsca dla klienta:");
    puts("Podaj dane klienta, najpierw podaj imie:");
    gets(list[cnty].name);
    puts("Teraz podaj nazwisko.");
    gets(list[cnty].surname);
    puts("Prosze wybrac miejsce od 1 do 12:");
    scanf("%d", &id_given);
    while (!(id_given > 0 && id_given < 13))
    {
        printf("Wpisano nieprawidlowe miejsce.\nWprowadz numer miejsca raz jeszcze\n");
        while (getchar() != '\n')
            continue;
        scanf("%d", &id_given);
    }
    while (getchar() != '\n')
        continue;
    
    for (i = 0; i < MAXP; i++)
            while (list[i].flag == YES && id_given == list[i].id)
            {
                printf("Miejsce jest juz zajete. Prosze wybrac inne miejsce.\n");
                printf("By sprawdzic, jakie miejsca sa wolne, prosze wejsc w opcje b) menu.\n");
                scanf("%d", &id_given);
                while (getchar() != '\n')
                    continue;
            }
    //dodac sprawdzenie czy podana liczba miesci sie w zakresie od 1 do 12
    list[cnty].id = id_given;
    list[cnty].flag = YES;
    //dodac sprawdzenie czy miejsce nie jest juz zajete

    /* for (i = cnty + 1; i < MAXP; i++)
    {
        list[i].id = i + 1;
        list[i].name[0] = '\0';
        list[i].surname[0] = '\0';
        list[i].flag = NO;
    } */
    fwrite(&list[0], sizeof(struct Person_data), MAXP, fp);
    fclose(fp);
}

void delete_reserv(struct Person_data * list)
{
    FILE *fp;
    int i;
    int licznik = 0;
    int cnty = 0;
    int del_id = 0;

    if ((fp = fopen("passengers.dat", "r+b")) == NULL)
    {
        fputs("Nie moge otworzyc pliku passengers.dat.\n", stderr);
        return;
    }
    rewind(fp);
    while (licznik < MAXP && fread(&list[licznik], sizeof(struct Person_data), 1, fp) == 1)
    {
        licznik++;
        if (list[licznik].flag == YES)
            cnty++;
    }
    rewind(fp);
    fclose(fp);

    if ((fp = fopen("passengers.dat", "w+b")) == NULL)
    {
        fputs("Nie moge otworzyc pliku passengers.dat.\n", stderr);
        return;
    }
    printf("Podaj id pasazera do usuniecia:\n");
    scanf("%d", &del_id);
    if (del_id > 0 && del_id < 13)
    {
        for (i = 0; i < MAXP; i++)
        {
            if (list[i].id == del_id)
            {
                list[i].name[0] = '\0';
                list[i].surname[0] = '\0';
                list[i].flag = NO;
            }
        }
        printf("Usunieto pasazera.\n");
    }
    else
        printf("Nie udalo sie usunac. Podano bledny numer miejsca.\n");
    while (getchar() != '\n')
        continue;
    fwrite(&list[0], sizeof(struct Person_data), MAXP, fp);
    fclose(fp);
}

void srtStruct(struct Person_data * ptrStr)
{
    FILE *fp;
    struct Person_data temp;
    int dol, szuk, licznik, num, i;
    char c1,c2;
    if ((fp = fopen("passengers.dat", "r+b")) == NULL)
    {
        fputs("Nie moge otworzyc pliku passengers.dat.\n", stderr);
        return;
    }
    for (i = 0; i < MAXP; i++)
        fread(&ptrStr[i], sizeof(struct Person_data), 1, fp);
    fclose(fp);
    num = MAXP;

    for (dol = 0; dol < num - 1; dol++)
    {
        for (szuk = dol + 1; szuk < num; szuk++)
        {
            c1 = tolower(ptrStr[dol].name[0]);
            c2 = tolower(ptrStr[szuk].name[0]);
            if (c1 > c2)
            {
                temp.flag = ptrStr[dol].flag;
                strcpy(temp.name, ptrStr[dol].name);
                strcpy(temp.surname, ptrStr[dol].surname);
                temp.id = ptrStr[dol].id;

                ptrStr[dol].flag = ptrStr[szuk].flag;
                strcpy(ptrStr[dol].name, ptrStr[szuk].name);
                strcpy(ptrStr[dol].surname, ptrStr[szuk].surname);
                ptrStr[dol].id = ptrStr[szuk].id;

                ptrStr[szuk].flag = temp.flag;
                strcpy(ptrStr[szuk].name, temp.name);
                strcpy(ptrStr[szuk].surname, temp.surname);
                ptrStr[szuk].id = temp.id;
            }
        }
    }
    printf("Alfabetyczna lista rezerwacji:\n");
    for (i = 0; i < MAXP; i++)
        if (ptrStr[i].flag == YES)
            printf("%d. %s %s\n", ptrStr[i].id, ptrStr[i].name, ptrStr[i].surname);
}
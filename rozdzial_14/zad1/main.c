#include <stdio.h>
#include <string.h>
#include "miesiace.c"
int liczba_dni(char nazwa_m[]);
extern struct miesiac miesiace[];
int main()
{   
    printf("%d\n", liczba_dni("Maj"));
    return 0;
}

int liczba_dni(char nazwa_m[])
{
    int dni;
    int i;
    int numer_m = 0;
    for (i = 0, dni = 0; i < 12; i++)
    {
        if(strcmp(miesiace[i].nazwa_miesiaca, nazwa_m) == 0)
        {
            numer_m = i + 1;
            break;
        }
    }
    for (i = 0, dni = 0; i < numer_m; i++)
        dni += miesiace[i].liczba_dni;
    return dni;
}
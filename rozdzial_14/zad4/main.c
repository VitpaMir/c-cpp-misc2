#include <stdio.h>
#define DL 20
struct nazwa
{
    char imie1[DL];
    char imie2[DL];
    char nazwisko[DL];
};
struct person
{
    struct nazwa osobowe;
    long long pesel;
};
void wyswietlanie (const struct person *wsk_strk, int n);
void wyswietlanie2(struct person personalne[], int n);
int main()
{
    struct person tab_osob[3] = {
        {{"Andrzej", "Wiktor", "Mecherzynski"},
            89104535},
        {{"Pablito", "Andrejo", "Polanjski"},
            77103425},
        {{"Norbert", "", "Brzuchacz"},
            78901123}
    };
    wyswietlanie(tab_osob, 3);
    puts("");
    wyswietlanie2(tab_osob, 3);
    return 0;
}

void wyswietlanie (const struct person *wsk_strk, int n)
{
    int i;
    for (i = 0; i < n; i++, wsk_strk++)
    {
        printf("%s, ", wsk_strk->osobowe.nazwisko);
        printf("%s", wsk_strk->osobowe.imie1);
        if (wsk_strk->osobowe.imie2[0] != '\0')
            printf(" %c.", wsk_strk->osobowe.imie2[0]);
        printf(" -- %lu", wsk_strk->pesel);
        printf("\n");
    }
}

void wyswietlanie2(struct person personalne[], int n)
{
    int i;
    printf("W funkcji drugiej:\n");
    for (i = 0; i < n; i++)
    {
        printf("%s, ", personalne[i].osobowe.nazwisko);
        printf("%s", personalne[i].osobowe.imie1);
        if (personalne[i].osobowe.imie2[0] != '\0')
            printf(" %c.", personalne[i].osobowe.imie2[0]);
        printf(" -- %lu", personalne[i].pesel);
        printf("\n");
    }
}
#include <iostream>

using namespace std;

int main()
{
    int uczniowie, cukierki, x, y;
    cout << "Ilu uczniow liczy Twoja klasa: ";
    cin >> uczniowie;
    cout << "Ile masz cukierkow: ";
    cin >> cukierki;

    x = cukierki / (uczniowie - 1);
    y = cukierki % uczniowie;

    cout << "Cukierkow dla kazdego ucznia jest: " << x << endl;
    cout << "Cukierkow reszta: " << y;
    
    return 0;
}

#include <iostream>
#include <cstring>

int main()
{
    using namespace std;

    string imie;
    string nazwisko;
    string calosc;
    cout << "Podaj imie: ";
    getline(cin, imie);
    cout << "Podaj nazwisko: ";
    getline(cin, nazwisko);
    calosc = imie + ", " + nazwisko;
    cout << "Calosc: " << calosc;
    return 0;
}
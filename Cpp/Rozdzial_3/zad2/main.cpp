#include <iostream>

int main()
{
    using namespace std;
    
    string name;
    string dessert;

    cout << "Podaj swoje imie:\n";
    getline(cin, name);
    cout << "Podaj swoj ulubiony deser:\n";
    getline(cin, dessert);
    cout << "Mam dla Ciebie " << dessert;
    cout << ", " << name << ".\n";
    return 0;
}
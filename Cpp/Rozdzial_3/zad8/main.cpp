#include <iostream>
using namespace std;

struct Pizza
{
    string name;
    float diameter;
    float weight;
};

int main()
{
    Pizza *pizza = new Pizza;
    cout << "Podaj dane pizzy!" << endl;
    cout << "Podaj producenta: ";
    getline(cin, pizza->name);
    cout << "Podaj srednice pizzy w milimetrach: ";
    cin >> pizza->diameter;
    cout << "Podaj wage pizzy w gramach: ";
    cin >> pizza->weight;
    cout << endl;
    cout << "Pizza firmy: " << pizza->name << endl;
    cout << pizza->diameter << " milimetrow" <<endl;
    cout << pizza->weight << " gramow" << endl;
    delete pizza;
    return 0;
}
#include <iostream>
#include <cstring>

int main()
{
    using namespace std;
    const char ArSize = 20;
    char imie[ArSize];
    char nazwisko[ArSize];
    char calosc[2*ArSize];
    cout << "Podaj imie: ";
    cin.getline(imie, ArSize);
    cout << "Podaj nazwisko: ";
    cin.getline(nazwisko, ArSize);
    strcpy(calosc, imie);
    strcat(calosc, ", ");
    strcat(calosc, nazwisko);
    cout << "Calosc: " << calosc;
    return 0;
}
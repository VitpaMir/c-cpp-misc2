#include <iostream>

int main()
{
    using namespace std;

    const int ROZMIAR = 20;

    char imie[ROZMIAR];
    char nazwisko[ROZMIAR];
    int ocena;
    int wiek;

    cout << "Jak masz na imie? ";
    cin.getline(imie, ROZMIAR);
    cout << "Jak masz na nazwisko? ";
    cin.getline(nazwisko, ROZMIAR);
    cout << "Na jaka ocene zaslugujesz? "; 
    (cin >> ocena).get();
    cout << "Ile masz lat? ";
    (cin >> wiek).get();

    cout << "Nazwisko: " << nazwisko << ", " << imie << endl;
    cout << "Ocena: " << ocena - 1 << endl;
    cout << "Wiek: " << wiek;
    return 0;
}
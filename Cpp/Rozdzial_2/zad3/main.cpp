#include <iostream>
using namespace std;

int main ()
{
    float deg, min, sec;
    float deg_val;
    const int m_to_sec = 60;
    const int h_to_deg = 60;
    cout << "Podaj dlugosc geograficzna w stopniach, minutach i sekundach: ";
    cin >> deg;
    cin >> min;
    cin >> sec;
    deg_val = deg + min/60 + sec/3600;
    cout << deg << " stopni, " << min << " minut, " << sec <<" sekund" << " = " << deg_val << " stopni";

    return 0;
}
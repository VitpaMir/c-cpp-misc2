#include <iostream>

int main()
{
    using namespace std;

    const float cal_cm = 2.54f;
    const float stopa_cm = 0.032808f;
    int wzrost_cm;
    cout << "Podaj swoj wzrost w cm: ";
    cin >> wzrost_cm;
    cout << "Twoj wzrost: " << int (wzrost_cm*stopa_cm) << " stop ";
    cout << ((wzrost_cm*stopa_cm) - int (wzrost_cm*stopa_cm))*12 << " cali" << endl;

    return 0;
}
#include <iostream>

int main()
{
    using namespace std;
    
    long secs;
    int days, hours, mins, r_secs;
    const int days_hours = 24;
    const int hour_mins = 60;
    const int min_secs = 60;
    cout << "Podaj liczbe sekund do przeliczenia: ";
    cin >> secs;
    days = secs / (days_hours * hour_mins * min_secs);
    hours = (secs - days * days_hours * hour_mins * min_secs) / (hour_mins * min_secs);
    mins = (secs - days * days_hours * hour_mins * min_secs - hours * hour_mins * min_secs) / min_secs;
    r_secs = secs % min_secs;
    cout << secs << " sekund = " << days << " dni, " << hours << " godzin, " << mins << " minut, " << r_secs << " sekund." << endl;
    return 0;
}
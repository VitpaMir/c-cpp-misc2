#include <iostream>

int main()
{
    float zuzycie_kml;
    float zuzycie_gm;
    const float km_miles = 62.14f; // 100 km to tyle mil
    const float galon_litres = 3.875f; // 1 galon to tyle litrow
    std::cout << "Podaj zuzycie w litrach na 100 km: ";
    std::cin >> zuzycie_kml;
    zuzycie_gm = 1/((zuzycie_kml*1/galon_litres)/km_miles);
    std::cout << "Wynik : " << zuzycie_gm;
    return 0;
}
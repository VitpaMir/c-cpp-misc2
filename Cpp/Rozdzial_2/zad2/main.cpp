#include <iostream>


int main()
{
    using namespace std;

    const int stopy_cale = 12;
    const float m_cale = 0.0254;
    const float funty_kg = 2.2;
    float stopy, cale, funty;
    float wzrost_m, waga_kg;
    float bmi;

    cout << "Podaj wzrost w stopach i calach: ";
    cin >> stopy;
    cin >> cale;
    cout << "Podaj wage w funtach: ";
    cin >> funty;
    wzrost_m = (float (stopy*stopy_cale) + cale)*m_cale;
    waga_kg = funty / funty_kg;
    bmi = waga_kg/(wzrost_m*wzrost_m);
    cout << "Twoj wspolczynnik BMI: " << bmi << endl;

    return 0;
}
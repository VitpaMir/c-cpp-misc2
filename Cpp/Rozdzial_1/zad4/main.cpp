#include <iostream>
float C_to_F(float Ctemp);

int main()
{
    using namespace std;

    float Ctemp;
    float Ftemp;
    cout << "Podaj temperature w stopniach Celsjusza:" << endl;
    cin >> Ctemp;
    Ftemp = C_to_F(Ctemp);
    cout << "Twoja temperatura w stopniach Fahrenheita to " << Ftemp << endl;
    return 0;
}

float C_to_F (float celsjus)
{
    return celsjus*1.8 + 32;
}
#include <iostream>

int main ()
{
    using namespace std;
    const int ROK = 63240;
    float odl_lata;
    long odl_jedn;
    cout << "Prosze podac odleglosc w latach swietlnych: ";
    cin >> odl_lata;
    cout << endl;
    odl_jedn = ROK * odl_lata;
    cout << odl_lata << " lat swietlnych = " << odl_jedn << " jednostek astronomicznych." << endl;
    return 0;
}
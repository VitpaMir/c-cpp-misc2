#include <stdio.h>
#include <ctype.h>
#include <string.h>

int main(void)
{
    // int someInt = 36;
    char str[] = "46yu";
    // sprintf(str, "%d", someInt);
    int i = 0;
    while (i < strlen(str))
    {
        if (isdigit(str[i]))
        {
            i++;
            continue;
        }
        else
        {
            printf("Wrong input.\n");
            break;
        }
    }

    return 0;
}
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX 256
int main(int argc, char * argv[])
{
    FILE *wp;

    if (argc == 3)
    {
        if ((wp = fopen(argv[2], "r")) == NULL)
        {
            fprintf(stderr, "Nie moglem otworzyc pliku \"%s\".\n", argv[2]);
            exit(1);
        }
        while (fgets(argv[2], MAX, wp))
        {
            if (strchr(argv[2], *argv[1]) != NULL && *argv[2] != '\n')
                fputs(argv[2], stdout);
        }
    }
    else
    {
        printf("Sposob uzycia %s znak nazwa_pliku\n", argv[0]);
        exit(2);
    }
    fclose(wp);
    return 0;
}

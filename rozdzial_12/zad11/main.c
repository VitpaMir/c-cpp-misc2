#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
int main()
{
    long licznik, koniec;
    int i = 0;
    int j = 0;
    char ch;
    FILE *we, *wy;
    int tablica[20][30];
    char rysunek[20][31];
    if ((we = fopen("Wiersz.txt", "rb")) == NULL)
    {
        fprintf(stderr, "Nie udalo sie otworzyc pliku %s.\n", "Wiersz.txt");
        exit(1);
    }
    if ((wy = fopen("Dane.wy", "wb")) == NULL)
    {
        fprintf(stderr, "Nie udalo sie utworzyc pliku %s.\n", "Dane.wy");
        exit(2);
    }
    fseek(we, 0L, SEEK_END);
    koniec = ftell(we);
    rewind(we);
    for (licznik = 0L; licznik <= koniec; licznik++)
    {
        fseek(we, licznik, SEEK_SET);
        ch = getc(we);
        if (isdigit(ch))
        {
            tablica[i][j] = ch - 48;
            j++;
        }
        if (ch == '\n')
        {
            i++;
            j = 0;
        }
    }
    for (i = 0; i < 20; i++)
    {
        for (j = 0; j < 30; j++)
            printf("%d", tablica[i][j]);
        printf("\n");
    }
    for (i = 0; i < 20; i++)
    {
        for (j = 0; j < 30; j++)
        {
            if (tablica[i][j] == 0)
                rysunek[i][j] = ' ';
            else if (tablica[i][j] == 1)
                rysunek[i][j] = '.';
            else if (tablica[i][j] == 2)
                rysunek[i][j] = '\'';
            else if (tablica[i][j] == 3)
                rysunek[i][j] = ':';
            else if (tablica[i][j] == 4)
                rysunek[i][j] = '~';
            else if (tablica[i][j] == 5)
                rysunek[i][j] = '*';
            else if (tablica[i][j] == 6)
                rysunek[i][j] = '=';
            else if (tablica[i][j] == 7)
                rysunek[i][j] = '&';
            else if (tablica[i][j] == 8)
                rysunek[i][j] = '%';
            else if (tablica[i][j] == 9)
                rysunek[i][j] = '#';
        }
        rysunek[i][31] = '\0';
    }
    puts("");
    for (i = 0; i < 20; i++)
    {
        for (j = 0; j < 30; j++)
            printf("%c", rysunek[i][j]);
        printf("\n");
    }
    return 0;
}

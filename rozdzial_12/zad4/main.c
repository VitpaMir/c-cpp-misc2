#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define DLAN 50

int main()
{
    FILE *we, *wy;
    int ch;
    char nazwa[40];
    int licznik = 0;
    char plik_doc[DLAN];

    printf("Podaj nazwe pliku do redukcji:\n");
    while (gets(plik_doc) && plik_doc[0] != '\0')
    {
        if ((we = fopen(plik_doc, "r")) == NULL)
        {
            fprintf(stderr, "Blad otwarcia  %s\n", plik_doc);
            exit(EXIT_FAILURE); //jest ok tylko wywala program
        }                       //przy nieudanych probach
        strcpy(nazwa, plik_doc);
        strcat(nazwa, ".red");
        if ((wy = fopen(nazwa, "w")) == NULL)
        {
            fprintf(stderr, "Nie mozna utworzyc pliku wyjsciowego.\n");
            exit(EXIT_FAILURE);
        }
        while ((ch = getc(we)) != EOF)
            if (licznik++ % 3 == 0)
                putc(ch, wy);
        if (fclose(we) != 0 || fclose(wy) != 0)
            fprintf(stderr, "Blad przy zamykaniu plikow.\n");
        printf("Podaj kolejny plik do redukcji:\n");
    }
    return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define TAK 1
#define NIE 0
#define MAX 1024
#define DLAN 50
void usun_odst (char lancuch[], char lancuch2[]);

int main(int argc, char * argv[])
{
    int i, j;
    FILE *w1, *w2;
    char *wsk;
    char ch1, ch2;
    int nowa_linia;
    char ch1t[DLAN], ch2t[DLAN];
    int koniec1, koniec2;
    if (argc != 3)
    {
        fprintf(stderr, "Sposob uzycia: %s nazwa_pliku1 nazwa_pliku2\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    else
    {
        if ((w1 = fopen(argv[1], "r")) == NULL)
        {
            fprintf(stderr, "Nie mozna otworzyc pierwszego pliku.\n");
            exit(EXIT_FAILURE);
        }
        if ((w2 = fopen(argv[2], "r")) == NULL)
        {
            fprintf(stderr, "Nie mozna otworzyc drugiego pliku.\n");
            exit(EXIT_FAILURE);
        }
        while (!feof(w1) && !feof(w2))
        {
            fgets(ch1t, MAX, w1);
            i = strlen(ch1t) - 1;
            ch1t[i] = NULL;
            fprintf(stdout, "%s", ch1t);
            fgets(ch2t, MAX, w2);
            fprintf(stdout, " ");
            fprintf(stdout, "%s", ch2t);
            koniec1 = ftell(w1);
            koniec2 = ftell(w2);
        }
        if (fseek(w1, koniec1, SEEK_SET) != 0)
            fprintf(stderr, "Blad z fseek'iem.\n");
        while (fgets(ch1t, MAX, w1))
            fputs(ch1t, stdout);
        if (fseek(w2, koniec2, SEEK_SET) != 0)
            fprintf(stderr, "Blad z fseek'iem.\n");
        while (fgets(ch2t, MAX, w2))
            fputs(ch2t, stdout);
    }
    if (fclose(w1) != 0 || fclose(w2) != 0)
            fprintf(stderr, "Blad przy zamykaniu plikow.\n");
    return 0;
}

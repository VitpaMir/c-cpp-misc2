#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX 81

int main(int argc, char * argv[])
{
    FILE *wp;
    int i, j;
    int pliki;
    int licznik = 0;
    int wiersze = 0;
    char ch, znak;
    char tab[MAX][MAX];
    char *wsk[MAX];
    long koniec, licznik2;

    znak = argv[1][0];
    if (argc < 2)
    {
        fprintf(stderr, "Prosze wpisac %s nastepnie znak.\n"
                "Po nim ewentualne pliki ", argv[0]);
        fprintf(stderr, "do obliczenia liczby wystepowania tegoz znaku.\n");
        exit(1);
    }
    if (argc == 2)
    {
        fprintf(stdout, "Wybrales wejscie standardowe wpisz tekst.\n");
        while (gets(tab[wiersze]) != NULL && tab[wiersze][0] != '\0')
        {
            wsk[wiersze] = tab[wiersze];
            wiersze++;
        }
        for (i = 0; i < wiersze; i++)
        {
            for (j = 0; j < strlen(wsk[i]); j++)
            {
                if (wsk[i][j] == znak)
                    licznik++;
            }
        }
        fprintf(stdout, "Wejscie standardowe.\n");
        fprintf(stdout, "Liczba wystapien znaku %c to %d.\n", znak, licznik);
        fprintf(stdout, "Mozesz wpisywac dalej lub enter by zakonczyc.\n");
    }
    else
    {
        for (pliki = 2; pliki < argc; pliki++)
        {
            if ((wp = fopen(argv[pliki], "r")) == NULL)
            {
                fprintf(stderr, "Nie udalo sie otworzyc pliku %s.\n", argv[pliki]);
                continue;
            }
            fseek(wp, 0L, SEEK_END);
            koniec = ftell(wp);
            rewind(wp);
            licznik = 0;
            for (licznik2 = 1L; licznik2 < koniec; licznik2++)
            {
                fseek(wp, licznik2, SEEK_SET);
                ch = getc(wp);
                if (ch == znak)
                    licznik++;
            }
            fprintf(stdout, "Liczba wystapien znaku %c w pliku %s to %d\n", znak, argv[pliki], licznik);
            if (fclose(wp) != 0)
            {
                fprintf(stderr, "Nie udalo sie poprawnie zamknac pliku %s.\n", argv[pliki]);
                continue;
            }
        }
    }
    return 0;
}

#include <stdio.h>
#include <stdlib.h>
#define MAX 100

int main()
{
    FILE *wp;
    long ustaw = 0L;
    char sciezka[MAX];
    char buf[MAX];

    puts("Wpisz sciezke dostepu do dowolnego pliku tekstowego.");
    gets(sciezka);
    if ((wp = fopen(sciezka, "r")) == NULL)
    {
        fprintf(stderr ,"Nie mozna znalezc pliku o podanej sciezce/nazwie.\n");
        exit(1);
    }
    fprintf(stdout, "Podaj polozenie w pliku konkretnych danych do wyswietlenia.\n");
    while (fscanf(stdin, "%ld", &ustaw) == 1)
    {
        fseek(wp, ustaw, SEEK_SET);
        fgets(buf, MAX, wp);
        fputs(buf, stdout);
        puts("Aby zakonczyc wpisz dowolna wartosc nienumeryczna.");
    }
    fclose(wp);
    return 0;
}

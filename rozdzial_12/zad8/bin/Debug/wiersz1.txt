Nic dwa razy sie nie zdarza
i nie zdarzy. Z tej przyczyny
zrodzilismy sie bez wprawy
i pomrzemy bez rutyny.
Chocbysmy uczniami byli
najtepszymi w szkole swiata,
nie bedziemy repetowac
zadnej zimy ani lata.
#include <stdio.h>
#include <stdlib.h>
#define ROZMIAR 20
int main(int argc, char *argv[])
{
    FILE *wp;
    int i = 0;
    double dane;
    double suma = 0.0;

    if (argc == 1)
        wp = stdin;
    else if (argc == 2)
    {
        if ((wp = fopen(argv[1], "r")) == NULL)
        {
            fprintf(stderr, "Nie moglem otworzyc pliku \"%s\".\n", argv[1]);
            exit(1);
        }
    }
    else
    {
        printf("Sposob uzycia %s nazwa_pliku\n", argv[0]);
        exit(2);
    }
    while (fscanf(wp, "%lf", &dane) == 1)
    {
        suma += dane;
        i++;
    }
    if (i > 0)
        printf("Srednia z %d liczb to %lf.\n", i, suma / i);
    else
        printf("Brak prawidlowych danych.\n");
    fclose(wp);
    return 0;
}

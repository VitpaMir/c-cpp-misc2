#include <stdio.h>
#include <stdlib.h>
#define ROZMIAR 1024
int main(int argc, char *argv[])
{
    int i;
    FILE *wp;
    //char bufor[ROZMIAR];
    char ch;

    for (i = 1; i < argc; i++)
    {
        if ((wp = fopen(argv[i], "r")) == NULL)
        {
            fprintf(stderr, "Nie mozna otworzyc danego pliku \"%s\" otworzyc.\n", argv[i]);
            exit(1);
        }
        //fread(bufor, sizeof (char), ROZMIAR, wp);
        //printf("%s", bufor);
        //fprintf(stdout , "%s", bufor);
        //wyswietlaja czasami jakies niepozadane znaki nie wiedziec czemu
        while ((ch = getc(wp)) != EOF)
            putc(ch, stdout);
        printf("\n");
        fclose(wp);
    }
    return 0;
}

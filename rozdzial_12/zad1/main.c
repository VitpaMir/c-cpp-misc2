#include <stdio.h>
#include <stdlib.h>
#define MAX 512
int main(int argc, char * argv[])
{
    FILE *wz, *wd;
    char bufor[MAX];
    size_t n;
    if (argc != 3)
    {
        printf("Sposob uzycia: %s nazwa_pliku_zrodlowego nazwa_pliku_docelowego\n", argv[0]);
        exit(1);
    }
    else
    {
        if ((wz = fopen(argv[1], "rb")) == NULL)
        {
            printf("Nie mozna otworzyc pliku zrodlowego.\n");
            exit(2);
        }
        if ((wd = fopen(argv[2], "wb")) == NULL)
        {
            printf("Nie mozna otworzyc pliku docelowego.\n");
            exit(3);
        }
        n = fread(bufor, sizeof (char), MAX, wz); //na drugiej pozycji porcja danych
        fwrite(bufor, sizeof (char), n, wd);  //jako typ, inaczej zwracalo smieci na koncu
    }                                           //kopiowanego stringa, tam gdzie MAX moze
    fclose(wz);                                 //byc chyba byle ile byle sie zmiescilo
    fclose(wd);                     //jednak w fwrite warto by byla wartosc zwracana przez
    return 0;                       //fread wtedy nie ma smieci jesli MAX jest wieksze od
}                               //ilosci danych w pliku

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define TAK 1
#define NIE 0
#define MAX 1024
#define DLAN 50
int main(int argc, char * argv[])
{
    FILE *w1, *w2;
    char ch1[DLAN], ch2[DLAN];
    int koniec1, koniec2;
    if (argc != 3)
    {
        fprintf(stderr, "Sposob uzycia: %s nazwa_pliku1 nazwa_pliku2\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    else
    {
        if ((w1 = fopen(argv[1], "rb")) == NULL)
        {
            fprintf(stderr, "Nie mozna otworzyc pierwszego pliku.\n");
            exit(EXIT_FAILURE);
        }
        if ((w2 = fopen(argv[2], "rb")) == NULL)
        {
            fprintf(stderr, "Nie mozna otworzyc drugiego pliku.\n");
            exit(EXIT_FAILURE);
        }
        while (fgets(ch1, MAX, w1) && fgets(ch2, MAX, w2))
        {
            fputs(ch1, stdout);
            if (feof(w1) != 0)
                fputs("\n", stdout);
            fputs(ch2, stdout);
            if (feof(w2) != 0)
                fputs("\n", stdout);
            koniec1 = ftell(w1);
            koniec2 = ftell(w2);
        }
        if (fseek(w1, koniec1, SEEK_SET) != 0)
            fprintf(stderr, "Blad z fseek'iem.\n");
        while (fgets(ch1, MAX, w1))
            fputs(ch1, stdout);
        if (fseek(w2, koniec2, SEEK_SET))
            fprintf(stderr, "Blad z fseek'iem.\n");
        while (fgets(ch2, MAX, w2))
            fputs(ch2, stdout);


    }
    if (fclose(w1) != 0 || fclose(w2) != 0)
            fprintf(stderr, "Blad przy zamykaniu plikow.\n");
    return 0;
}

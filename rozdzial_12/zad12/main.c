#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#define ROZMIAR 30
#define ROZMIAR1 20
void jakosc (int tablica[][ROZMIAR]);

int main()
{
    long licznik, koniec;
    int i = 0;
    int j = 0;
    char ch;
    FILE *we, *wy;
    int tablica[ROZMIAR1][ROZMIAR];
    char rysunek[ROZMIAR1][31];
    if ((we = fopen("Wiersz.txt", "rb")) == NULL)
    {
        fprintf(stderr, "Nie udalo sie otworzyc pliku %s.\n", "Wiersz.txt");
        exit(1);
    }
    if ((wy = fopen("Dane.wy", "wb")) == NULL)
    {
        fprintf(stderr, "Nie udalo sie utworzyc pliku %s.\n", "Dane.wy");
        exit(2);
    }
    fseek(we, 0L, SEEK_END);
    koniec = ftell(we);
    rewind(we);
    for (licznik = 0L; licznik <= koniec; licznik++)
    {
        fseek(we, licznik, SEEK_SET);
        ch = getc(we);
        if (isdigit(ch))
        {
            tablica[i][j] = ch - 48;
            j++;
        }
        if (ch == '\n')
        {
            i++;
            j = 0;
        }
    }
    for (i = 0; i < 20; i++)
    {
        for (j = 0; j < ROZMIAR; j++)
            printf("%d", tablica[i][j]);
        printf("\n");
    }
    puts("");

    for (i = 0; i < ROZMIAR1; i++)
    {
        for (j = 0; j < ROZMIAR; j++)
        {
            if (tablica[i][j] == 0)
                rysunek[i][j] = ' ';
            else if (tablica[i][j] == 1)
                rysunek[i][j] = '.';
            else if (tablica[i][j] == 2)
                rysunek[i][j] = '\'';
            else if (tablica[i][j] == 3)
                rysunek[i][j] = ':';
            else if (tablica[i][j] == 4)
                rysunek[i][j] = '~';
            else if (tablica[i][j] == 5)
                rysunek[i][j] = '*';
            else if (tablica[i][j] == 6)
                rysunek[i][j] = '=';
            else if (tablica[i][j] == 7)
                rysunek[i][j] = '&';
            else if (tablica[i][j] == 8)
                rysunek[i][j] = '%';
            else if (tablica[i][j] == 9)
                rysunek[i][j] = '#';
        }
        rysunek[i][31] = '\0';
    }
    puts("");
    for (i = 0; i < ROZMIAR1; i++)
    {
        for (j = 0; j < ROZMIAR; j++)
            printf("%c", rysunek[i][j]);
        printf("\n");
    }
    puts("");
    jakosc(tablica);
    for (i = 0; i < 20; i++)
    {
        for (j = 0; j < ROZMIAR; j++)
            printf("%d", tablica[i][j]);
        printf("\n");
    }
    puts("");

    for (i = 0; i < ROZMIAR1; i++)
    {
        for (j = 0; j < ROZMIAR; j++)
        {
            if (tablica[i][j] == 0)
                rysunek[i][j] = ' ';
            else if (tablica[i][j] == 1)
                rysunek[i][j] = '.';
            else if (tablica[i][j] == 2)
                rysunek[i][j] = '\'';
            else if (tablica[i][j] == 3)
                rysunek[i][j] = ':';
            else if (tablica[i][j] == 4)
                rysunek[i][j] = '~';
            else if (tablica[i][j] == 5)
                rysunek[i][j] = '*';
            else if (tablica[i][j] == 6)
                rysunek[i][j] = '=';
            else if (tablica[i][j] == 7)
                rysunek[i][j] = '&';
            else if (tablica[i][j] == 8)
                rysunek[i][j] = '%';
            else if (tablica[i][j] == 9)
                rysunek[i][j] = '#';
        }
        rysunek[i][31] = '\0';
    }
    puts("");

    for (i = 0; i < ROZMIAR1; i++)
    {
        for (j = 0; j < ROZMIAR; j++)
            printf("%c", rysunek[i][j]);
        printf("\n");
    }
    return 0;
}

void jakosc (int tablica[][ROZMIAR])
{
    int i, j;
    for (i = 0; i < ROZMIAR1; i++)
    {
        for (j = 0; j < sizeof (tablica[i]) / sizeof (int); j++)
        {
            if ((i == 0 && j != 0) && (abs(tablica[i][j] - tablica [i][j + 1]) && abs(tablica[i][j] - tablica [i + 1][j]) && abs(tablica[i][j] - tablica[i][j - 1])) > 0)
                tablica[i][j] = (tablica[i][j - 1] + tablica[i][j + 1] + tablica[i + 1][j]) / 3;

            else if ((j == 0 && i != 0) && (abs(tablica[i][j] - tablica [i - 1][j]) && abs(tablica[i][j] - tablica [i + 1][j]) && abs(tablica[i][j] - tablica[i][j + 1])) > 0)
                tablica[i][j] = (tablica[i - 1][j] + tablica[i + 1][j] + tablica[i][j + 1]) / 3;

            else if (j == (sizeof (tablica[i]) / sizeof (int) - 1) && (abs(tablica[i][j] - tablica [i][j - 1]) && abs(tablica[i][j] - tablica [i + 1][j]) && tablica[i][j] - tablica[i - 1][j]) > 0)
                tablica[i][j] = (tablica[i - 1][j] + tablica[i + 1][j] + tablica[i][j - 1]) / 3;

            else if (i == (ROZMIAR1 - 1) && (abs(tablica[i][j] - tablica [i][j - 1]) && abs(tablica[i][j] - tablica [i - 1][j]) && tablica[i][j] - tablica[i][j + 1]) > 0)
                tablica[i][j] = (tablica[i - 1][j] + tablica[i][j - 1] + tablica[i][j + 1]) / 3;

            else if ((i == 0 && j == 0) && (abs(tablica[i][j] - tablica [i][j + 1]) && abs(tablica[i][j] - tablica [i + 1][j])) > 0)
                tablica[i][j] = (tablica[i][j + 1] + tablica[i + 1][j]) / 2;

            else if ((i == (ROZMIAR1 - 1) && j == 0) && (abs(tablica[i][j] - tablica [i][j + 1]) && abs(tablica[i][j] - tablica [i - 1][j])) > 0)
                tablica[i][j] = (tablica[i - 1][j] + tablica[i][j + 1]) / 2;

            else if ((i == 0 && j == (sizeof (tablica[i]) / sizeof (int) - 1)) && (abs(tablica[i][j] - tablica [i][j - 1]) && abs(tablica[i][j] - tablica [i + 1][j])) > 0)
                tablica[i][j] = (tablica[i][j - 1] + tablica[i + 1][j])/ 2;

            else if ((i == (ROZMIAR1 - 1) && j == (sizeof (tablica[i]) / sizeof (int))) && (abs(tablica[i][j] - tablica [i][j - 1]) && abs(tablica[i][j] - tablica [i - 1][j])) > 0)
                tablica[i][j] = (tablica[i - 1][j] + tablica[i][j - 1]) / 2;

            else if ((abs(tablica[i][j] - tablica [i - 1][j]) && abs(tablica[i][j] - tablica [i + 1][j]) && abs(tablica[i][j] - tablica[i][j + 1]) && abs(tablica[i][j] - tablica[i][j - 1])) > 0)
                tablica[i][j] = (tablica[i - 1][j] + tablica[i][j + 1] + tablica[i + 1][j] + tablica[i][j - 1]) / 4;
        }
        printf("\n");
    }
}

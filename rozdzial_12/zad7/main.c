#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#define MAX 40
#define TAK 1
#define NIE 0
int main()
{
    int i;
    FILE *wp;
    char slowa[MAX];
    int num = 1;
    char buffer[MAX];
    int w_slowie = TAK;
    if ((wp = fopen("slowka", "a+")) == NULL)
    {
        fprintf(stderr, "Nie udalo sie otworzyc pliku.\n");
        exit(1);
    }
    puts("Podaj slowa, ktore maja zostac dodane do pliku;");
    puts("Aby zakonczyc, wcisnij Enter na poczatku wiersza.");
    while (gets(slowa) != NULL && slowa[0] != '\0')
        fprintf(wp, "%s ", slowa);
    puts("Zawartosc pliku:");
    rewind(wp);
    while (fscanf(wp, "%s", slowa) == 1)
    {
        sprintf(buffer, "%d. ", num++);
        strcat(buffer, slowa);
        puts(buffer);
    }
    if (fclose(wp) != 0)
        fprintf(stderr, "Blad przy zamykaniu pliku.\n");
    return 0;
}

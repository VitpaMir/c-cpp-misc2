#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define ROZMIAR 100

int main(int argc, char * argv[])
{
    FILE *wp;
    char lancuch[ROZMIAR];
    char bufor[ROZMIAR];
    if (argc != 3)
    {
        fprintf(stderr, "Sposob uzycia zad9 \"lancuch\" Nazwa_pliku.\n");
        exit(1);
    }
    strcpy(lancuch, argv[1]);
    if ((wp = fopen(argv[2], "r")) == NULL)
    {
        fprintf(stderr, "Nie mozna otworzyc pliku \"%s\".\n", argv[2]);
        exit(2);
    }
    while (fgets(bufor, ROZMIAR, wp))
    {
        if (strstr(bufor, lancuch) != NULL)
            fputs(bufor, stdout);
    }
    if (fclose(wp) != 0)
    {
        fprintf(stderr, "Nie udalo sie poprawnie zamknac pliku \"%s\".\n", argv[2]);
        exit(3);
    }
    return 0;
}

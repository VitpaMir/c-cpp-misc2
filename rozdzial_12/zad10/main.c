#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#define ROZMIAR 100
#define TAK 1
#define NIE 0
void pobierz_slowo (FILE *fp, char slowo[]);

int main()
{
    FILE *wp;
    char ch;
    char tablica[ROZMIAR];

    if ((wp = fopen("Wiersz.txt", "r")) == NULL)
    {
        fprintf(stderr, "Nie udalo sie otworzyc pliku.\n");
        exit(EXIT_FAILURE);
    }
    pobierz_slowo(wp, tablica);
    puts(tablica);
    putc('|', stdout);
    while (!feof(wp))
    {
        ch = getc(wp);
        putc(ch, stdout);
    }
    if (fclose(wp) != 0)
        fprintf(stderr, "Nie udalo sie poprawnie zamknac.\n");
    return 0;
}

void pobierz_slowo (FILE *fp, char slowo[])
{
    char ch;
    int i = 0;
    int w_slowie = NIE;
    while (!feof(fp))
    {
        ch = getc(fp);
        if (isalpha(ch) && w_slowie == NIE)
            w_slowie = TAK;
        if (w_slowie == TAK)
            slowo[i] = ch;
        if (isspace(ch) && w_slowie == TAK)
            break;
        i++;
    }
    ungetc(' ', fp);
    slowo[i] = '\0';
}


from logging import logMultiprocessing
import logging


spam = 1
assert spam < 10, 'Zla wartosc.'

eggs = 'GOODbye'
bacon = 'goodbye'
assert eggs.lower() == bacon.lower(), 'Eggs is not equal to bacon'

# assert 0, 'Always triggers!'

# nad shebangiem:
""" import logging
logging.basicConfig(level=logging.DEBUG, format=' %(asctime)s - %(levelname)s
- %(message)s') """
""" import logging
logging.basicConfig(filename='myProgramLog.txt', level=logging.DEBUG, format='
%(asctime)s - %(levelname)s - %(message)s') """

# debug, info, warning, error, critical (uppercase)

# logging.disable()

# print is for user or communicate user, logging for programmer

#! python3
# pw.py - An insecure password locker program.
PASSWORDS = {'email': 'F7minlBDDuvMJuxESSKHFhTxFtjVB6',
'blog': 'VmALvQyKAxiVH5G8v01if1MLZF3sdt',
'luggage': '12345'}

import sys
import pyperclip
if len(sys.argv) < 2:
    print('Usage: python pw.py [account] - copy account password')
    sys.exit()

account = sys.argv[1]   # first command line arg is the account name

if account in PASSWORDS:
    pyperclip.copy(PASSWORDS[account])
    print('Password for ' + account + ' coppied to clipboard.')
else:
    print('There is no account named ' + account)

# nie dziala poprawnie, w sensie okno komend (lub win+r) nie odpala programu po wpisaniu pw costam
# nie znajduje jakiegos pliku main, cos moze byc z katalogiem ze jest w zlym pliku
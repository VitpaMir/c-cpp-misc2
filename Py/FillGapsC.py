#! python3
# FillGapsC.py - fill the filenames gaps in series of files in folder
# by creating new files in series
# e.g: spam001.txt, spam003.txt, spam004.txt etc... will be renamed
# to make tidied series of files 001, 002 etc.

import re, os, shutil
spamPath = 'C:\\Users\\hj61t7\\OneDrive - Aptiv\\Dokumenty\\C\\c-cpp-misc2'
# Creating (writing) text file, icrementing by 2
""" for i in range(1, 20):
    if i < 10:
        fName = 'spam00' + str(i) + '.txt'
    elif i >= 10 and i < 100:
        fName = 'spam0' + str(i) + '.txt'
    else:
        fName = 'spam' + str(i) + '.txt'
    newFile = open(fName, 'w')
    newFile.write('To jest plik numer ' + str(i))
    newFile.close() """
# Looking for this file in folder by regex
regFile = re.compile(r'''(
    (spam)
    ([0-9]{3})
    (.txt)
    )''', re.VERBOSE)
regNumber = re.compile(r'[0-9]{3}')

spamFiles = []
for filename in os.listdir():
    if (regFile.search(filename)) != None:
        spamFiles.append(regFile.search(filename).group())
print(spamFiles)
# checking files order
spamNumbers = []
for fname in spamFiles:
    spamNumbers.append(regNumber.search(fname).group())
print(spamFiles)
print(spamNumbers)
j = 1
for v in spamFiles:
    w = regNumber.search(v)
    w = int(w.group())
    while w != j:
        if j > 9:
            newFile = open(spamPath + '\\' + 'spam' + '0' + str(j) + '.txt', 'w')
            j = j + 1
        else:
            newFile = open(spamPath + '\\' + 'spam' + '00' + str(j) + '.txt', 'w')
            j = j + 1
    j = j + 1
# Display files in folder
""" spamNewnames = []
for filename in os.listdir():
    if (regFile.search(filename)) != None:
        spamNewnames.append(regFile.search(filename).group())
print(spamNewnames) """
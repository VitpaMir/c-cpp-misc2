import re

def strongCheck(Password):
    try:
        length = re.compile(r'\w{8,}')
        mo = length.search(Password)
        mo.group()
    except AttributeError:
        print('Password is too short!')
        return -1
    try:
        smallCaps = re.compile(r'[A-Z]+')
        m1 = smallCaps.search(Password)
        m1.group()
    except AttributeError:
        print('Password has no capital letters!')
        return -1
    try:
        smallCaps = re.compile(r'[a-z]+')
        m1 = smallCaps.search(Password)
        m1.group()
    except AttributeError:
        print('Password has no small letters!')
        return -1
    try:
        smallCaps = re.compile(r'[0-9]+')
        m1 = smallCaps.search(Password)
        m1.group()
    except AttributeError:
        print('Password has no digits!')
        return -1
    
    print('Password is strong!')
    return 0

print('Type your password to make shure, it is strong password.')
print('Strong password must have at least 8 characters, a least one capital letters and at least one digit!')
haslo = input()
strongCheck(haslo)

from typing import List


def PutString(ListToChange):
    NewString = ''
    for i in range(len(ListToChange)):
        if i == len(ListToChange) - 1 and len(ListToChange) != 1:
            NewString += ' and ' + str(ListToChange[i])
        else: 
            NewString += str(ListToChange[i])
        if i != len(ListToChange) - 2 and len(ListToChange) != 1:
            NewString += ', '
        else:
            continue
    print(NewString)
spam = ['apples', 'bananas', 'peaches', 'lemons']
PutString(spam)
print(spam)
print('End of program')
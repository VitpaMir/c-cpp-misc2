#! Python3

import re

def reStrip(string2cut, ch = None):
    if ch != None:
        whiteSp = re.compile(ch)
        string2cut = whiteSp.sub('', string2cut)
    else:
        whiteSp = re.compile(r' ')
        string2cut = whiteSp.sub('', string2cut)
    return string2cut

name = ' Hello-World '
name = reStrip(name, '-')
#print(name + '|') tylko dla widoku czy dobrze dziala
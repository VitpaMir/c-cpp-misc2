#! python3
# delUnneeded.py - deletes unnedeed files which size
# exceed 10 MB

import os

folder = 'C:\\Users\\hj61t7\\OneDrive - Aptiv\\Dokumenty\\MGO'
for foldername, subfolders, filenames in os.walk(folder):
    for filename in filenames:
        if os.path.getsize(os.path.join(foldername, filename)) > 10000000:
            print(os.path.join(foldername, filename))

import random
coin = ''
guesses = ('tails', 'heads')
while coin not in guesses:
    print('Guess the coin toss! Enter heads or tails:')
    coin = input()
    guess = guesses.index(coin)
toss = random.randint(0, 1) # 0 is tails, 1 is heads
if toss == guess:
    print('You got it!')
else:
    print('Nope! Guess again!')
    coin = input()
    guess = guesses.index(coin)
    if toss == guess:
        print('You got it!')
    else:
        print('Nope. You are really bad at this game.')
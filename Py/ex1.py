import re

try:
        numRegex = re.compile(r'''(
        \d{1,3}
        (,)
        (\d{3})?
        (,\d{3})?
        )''', re.VERBOSE)
        mo = numRegex.search('The adventures of 6756 batmans')
        print(mo.group())
except AttributeError:
    print('Error, object has no attribute group')
surRegex = re.compile(r'''(
    [A-Z]\w+
    \s
    Nakamoto
    )''', re.VERBOSE)
mo1 = surRegex.search('The adventures of Alice Nakamoto, the bestest')
print(mo1.group())

oddRegex = re.compile(r'''(
    (Alice|Bob|Carol)
    \s
    (eats|pets|throws)
    \s
    (apples|cats|baseballs)
    .
    )''', re.VERBOSE | re.IGNORECASE)
mo2 = oddRegex.search('Alice eats cats.')
print(mo2.group())
def displayInventory(inventory):
    print('Inventory:')
    num = 0
    for k, v in inventory.items():
        print(k + ':' + ' ' + str(v))
        num += v
    print('All items: ' + str(num))
def addToInventory(inventory, addedItems):
    i = 0
    for k in addedItems:
        inventory.setdefault(k, 0)
        inventory[k] = inventory[k] + 1
    return inventory

player = {'rope': 1, 'torch': 8, 'gold coin': 42, 'dagger': 1, 'arrow': 12}
dragonLoot = ['gold coin', 'dagger', 'gold coin', 'gold coin', 'ruby']
displayInventory(player)
player = addToInventory(player, dragonLoot)
displayInventory(player)
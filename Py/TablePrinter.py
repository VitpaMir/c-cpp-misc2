def PrintTable(Tab):
    colWidths = [0]*len(Tab)
    for i in Tab:
        for j in i:
            if len(j) > colWidths[Tab.index(i)]:
                colWidths[Tab.index(i)] = len(j)
            else:
                continue
    i = 0
    for i in range(len(Tab[i])):
        for j in range(len(Tab)):
            print(Tab[j][i].rjust(colWidths[j]) + ' ', end='')
        print('')

tableData = [['apples', 'oranges', 'cherries', 'banana'],
            ['Alice', 'Bob', 'Carol', 'David'],
            ['dogs', 'cats', 'moose', 'goose']]
PrintTable(tableData)

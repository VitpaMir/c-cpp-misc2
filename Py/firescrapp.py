from selenium import webdriver
from webdriver_manager.firefox import GeckoDriverManager
browser = webdriver.Firefox(executable_path=GeckoDriverManager().install())
browser.get('https://inventwithpython.com')
try:
    elem = browser.find_element_by_class_name('col-sm-12')
    print('Found <%s> element with that class name!' % (elem.tag_name))
except:
    print('Was not able to find an element with that name.')
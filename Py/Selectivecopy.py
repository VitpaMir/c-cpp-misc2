#! python3
# Selectivecopy.py - select proper type of files
# and copy them to chosen folder

import os, shutil
folder = 'C:\\Users\\hj61t7\\OneDrive - Aptiv\\Dokumenty\\MGO'
destFolder = 'C:\\Users\\hj61t7\\OneDrive - Aptiv\\Dokumenty\\newFolder'
for foldername, subfolders, filenames in os.walk(folder):
    for filename in filenames:
        if filename.lower().endswith('.jpg') | filename.lower().endswith('.png'):
            rootPath = os.path.join(foldername, filename)
            if not os.path.exists(destFolder):
                os.makedirs(destFolder)
                print('Utworzono folder newFolder')
            shutil.copy(rootPath, destFolder)
            # print(rootPath)
            if not os.path.exists(rootPath):
                print('Nie udalo sie sklecic sciezki!')
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int main()
{
    char ch;
    int male = 0;
    int duze = 0;

    while ((ch = getchar()) != EOF)
    {
        if (islower(ch))
            male++;
        else if (isupper(ch))
            duze++;
        else
            continue;
    }
    printf("Malych liter: %d i duzych: %d.\n", male, duze);
    return 0;
}

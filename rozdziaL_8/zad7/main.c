#include <stdio.h>
#include <stdlib.h>
#define STAWKA1 0.85
#define STAWKA2 0.8
#define STAWKA3 0.75
#define WYN1 35
#define WYN2 37
#define WYN3 40
#define WYN4 45
#define WYJSCIE 'w'

int main()
{
    char ch;
    int i;
    int godzinypr;      // godziny pracy w tygodniu
    int podstawa;       //zl/godzine
    float podatek;
    float wynnetto;
    float wynnbrutto;
    menu:
    for (i = 0; i <= 60; i++)
        putchar('*');
    putchar('\n');
    printf("Wybierz stawke:\n");
    printf("a) %d zl/godz               b) %d zl/godz\n", WYN1, WYN2);
    printf("c) %d zl/godz               d) %d zl/godz\n", WYN3, WYN4);
    printf("w) Wyjscie\n");
    for (i = 0; i <= 60; i++)
        putchar('*');
    putchar('\n');
    while ((ch = getchar()) != WYJSCIE)
    {
        switch (ch)
        {
        case 'a' :
            podstawa = WYN1;
            break;
        case 'b' :
            podstawa = WYN2;
            break;
        case 'c' :
            podstawa = WYN3;
            break;
        case 'd' :
            podstawa = WYN4;
            break;
        case 'w' :
            break;
        default :
            printf("Wybierz od a do d  albo w zeby wyjsc.\n");
            goto menu;
        }
        while (getchar() != '\n')
            continue;
        printf("Podaj liczbe przepracowanych godzin:\n");
        scanf("%d", &godzinypr);
        if (godzinypr > 40)
        {
            wynnbrutto = 40 * podstawa;
            wynnbrutto += ((float) godzinypr - 40) * podstawa * 1.5;
            if (wynnbrutto <= 1200)
                wynnetto = wynnbrutto * STAWKA1;
            else if (wynnbrutto > 1200 && wynnbrutto <= 1800)
                wynnetto = 1200.0 * STAWKA1 + (wynnbrutto - 1200.0) * STAWKA2;
            else
                wynnetto = 1200.0 * STAWKA1 + 600.0 * STAWKA2 +
                                            (wynnbrutto - 1800.0) * STAWKA3;
            podatek = wynnbrutto - wynnetto;
        }
        else
            {
                wynnbrutto = (float) godzinypr * podstawa;
                if (wynnbrutto <= 1200)
                    wynnetto = wynnbrutto * STAWKA1;
                else
                    wynnetto = 1200.0 * STAWKA1 + (wynnbrutto - 1200.0) * STAWKA2;
                podatek = wynnbrutto - wynnetto;
            }
        printf("Przepracowales %d godzin, nalezy Ci sie wiec %.2f na reke, \n"
               "mimo ze brutto masz %.2f, bo placisz %.2f podatku.\n",
               godzinypr, wynnetto, wynnbrutto, podatek);
    }

    return 0;
}

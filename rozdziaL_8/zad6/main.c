#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int pobierz_wybor();
char pobierz_pierwszy();
void liczenie();

int main()
{
    /*int wybor;
    int pobierz_wybor();
    void liczenie();

    while ((wybor = pobierz_wybor()) != 'q')
    {
        switch (wybor)
        {
            case 'a' :  printf("Kup po niskiej cenie, sprzedaj po wysokiej.\n");
                        break;
            case 'b' :  putchar('\a');
                        break;
            case 'c' :  liczenie();
                        break;
            default  :  printf("Blad programu!\n");
                        break;
        }
    }*/
    printf("Pierwszy napotkany znak drukowany to %c .\n", pobierz_pierwszy());
    return 0;
}

int pobierz_wybor()
{
    int ch;

    printf("Wpisz wybrana litere.\n");
    printf("a. porada           b. alarm\n");
    printf("c. liczenie         q. koniec\n");
    ch = pobierz_pierwszy();
    while ((ch < 'a' || ch > 'c') && ch != 'q')
    {
        printf("Wpisz a, b c lub q.\n");
        ch = pobierz_pierwszy();
    }
    return ch;
}

char pobierz_pierwszy()
{
    char ch;
    ch = getchar();
    while (getchar() != '\n')
        continue;

    return ch;
}

void liczenie()
{
    int n, i;

    printf("Jak daleko liczyc? Podaj liczbe calkowita:\n");

    if (scanf("%d", &n) != 1)
    {
        printf("Nastepnym razem uzyj cyfr, tym razem uzyje liczby 5.\n");
        n = 5;
    }
    for (i = 1; i <= n; i++)
        printf("%d\n", i);
    while (getchar() != '\n')
        continue;
}

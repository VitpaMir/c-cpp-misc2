#include <stdio.h>
#include <stdlib.h>

int main()
{
    int proba = 1;
    char odpowiedz;
    char odpowiedz2;
    char odpowiedz3;

    printf("Wybierz liczbe od 0 do 100. Sprobuje ja zgadnac.");
    printf("\nNapisz t, jesli moja proba jest udana lub");
    printf("\nn, jesli nie trafilem.\n");
    printf("Jesli chcesz wyjsc, zasymuluj koniec pliku ctrl+z.\n");
    printf("Hmm... czy Twoja liczba jest mniejsza od 50?\n");
    while ((odpowiedz = getchar()) != EOF)
    {
        if (odpowiedz == 't')
        {
            while (getchar() != '\n')
                continue;
            printf("Ok, czy jest wieksza od 25?\n");
            if (odpowiedz == 't')
            {
                while (getchar() != '\n')
                    continue;
                printf("Ok, czy jest wieksza od 35?\n");
                if (odpowiedz == 't')
                {
                    while (getchar() != '\n')
                        continue;
                    printf("Hmm.. czy jest wieksza od 40?\n");
                    if (odpowiedz == 't')
                    {
                        while (getchar() != '\n')
                            continue;
                        printf("A czy jest wieksza lub rowna 45?\n");
                        while ((odpowiedz = getchar()) != EOF)
                        {
                            if (odpowiedz == 't')                           // dla liczb od 45 do 49
                            {
                                while (getchar() != '\n')
                                    continue;
                                proba = 45;
                                printf("O, czy to jest %d\n", proba);
                                while ((odpowiedz = getchar()) != 't')
                                {
                                while (getchar() != '\n')
                                    continue;
                                if (odpowiedz == 't')
                                    goto przerwanie;
                                else if (odpowiedz == 'n')
                                    printf("Wiec moze %d?\n", ++proba);
                                else
                                    printf("Rozumiem tylko t lub n.\n");
                                }
                            }
                            else if (odpowiedz == 'n')                  // dla liczb od 41 do 44
                            {
                                while (getchar() != '\n')
                                    continue;
                                proba = 41;
                                printf("Moze wiec %d\n", proba);
                                while ((odpowiedz = getchar()) != 't')
                                {
                                    while (getchar() != '\n')
                                        continue;
                                    if (odpowiedz == 't')
                                        goto przerwanie;
                                    else if (odpowiedz == 'n')
                                        printf("Wiec moze %d?\n", ++proba);
                                    else
                                        printf("Rozumiem tylko t lub n.\n");
                                }
                            }
                        }
                    }
                    else if (odpowiedz == 'n')                  // dla liczb od 36 do 40
                    {
                        while (getchar() != '\n')
                            continue;
                        proba = 36;
                        printf("A wiec moze jest to %d\n", proba);
                        while ((odpowiedz = getchar()) != 't')
                        {
                            while (getchar() != '\n')
                                    continue;
                            if (odpowiedz == 't')
                                goto przerwanie;
                            else if (odpowiedz == 'n')
                                printf("A czy moze jest to %d\n", ++proba);
                            else
                                printf("Rozumiem tylko t lub n.\n");
                        }
                    }
                }
                else if (odpowiedz == 'n')
                {
                    while (getchar() != '\n')
                        continue;
                    printf("Czy jest wieksza od 30?\n");
                    if (odpowiedz == 't')                               //dla liczb 31 do 35
                    {
                        while (getchar() != '\n')
                            continue;
                        proba = 31;
                        printf("Czy jest to %d?\n", proba);
                        while ((odpowiedz = getchar()) != 't')
                        {
                            while (getchar() != '\n')
                                continue;
                            if (odpowiedz == 't')
                                goto przerwanie;
                            else if (odpowiedz == 'n')
                                printf("A czy moze jest to %d\n", ++proba);
                            else
                                printf("Rozumiem tylko t lub n.\n");
                        }
                    }
                    else if (odpowiedz == 'n')                          // dla liczb 26 do 30
                    {
                        while (getchar() != '\n')
                            continue;
                        proba = 26;
                        printf("Czy jest to %d?\n", proba);
                        while ((odpowiedz = getchar()) != 't')
                        {
                            while (getchar() != '\n')
                                continue;
                            if (odpowiedz == 't')
                                goto przerwanie;
                            else if (odpowiedz == 'n')
                                printf("A czy moze jest to %d\n", ++proba);
                            else
                                printf("Rozumiem tylko t lub n.\n");
                        }
                    }
                    else
                        printf("Rozumiem tylko t lub n.\n");
                }
                else
                    printf("Rozumiem tylko t lub n.\n");
            }
            else if (odpowiedz == 'n')
            {
                while (getchar() != '\n')
                    continue;
                printf("Ok, czy jest wieksza od 15?\n");
                if (odpowiedz == 't')
                {
                    while (getchar() != '\n')
                        continue;
                    printf("A czy jest wieksza od 20?\n");
                    if (odpowiedz == 't')                               // dla liczb od 21 do 25
                    {
                        while (getchar() != '\n')
                            continue;
                        proba = 21;
                        printf("Czy jest to %d?\n", proba);
                        while ((odpowiedz = getchar()) != 't')
                        {
                            while (getchar() != '\n')
                                continue;
                            if (odpowiedz == 't')
                                goto przerwanie;
                            else if (odpowiedz == 'n')
                                printf("A czy moze jest to %d\n", ++proba);
                            else
                                printf("Rozumiem tylko t lub n.\n");
                        }
                    }
                    else if (odpowiedz == 'n')                          // dla liczb od 15 do 20
                    {
                        while (getchar() != '\n')
                            continue;
                        proba = 15;
                        printf("Czy jest to %d?\n", proba);
                        while ((odpowiedz = getchar()) != 't')
                        {
                            while (getchar() != '\n')
                                continue;
                            if (odpowiedz == 't')
                                goto przerwanie;
                            else if (odpowiedz == 'n')
                                printf("A czy moze jest to %d\n", ++proba);
                            else
                                printf("Rozumiem tylko t lub n.\n");
                        }
                    }
                }
                else if (odpowiedz == 'n')
                {
                    while (getchar() != '\n')
                            continue;
                    printf("A czy jest wieksza od 10?\n");
                    if (odpowiedz == 't')                               // dla liczb od 11 do 14
                    {
                        while (getchar() != '\n')
                            continue;
                        proba = 11;
                        printf("O to moze to jest %d?\n", proba);
                        while ((odpowiedz = getchar()) != 't')
                        {
                            while (getchar() != '\n')
                                continue;
                            if (odpowiedz == 't')
                                goto przerwanie;
                            else if (odpowiedz == 'n')
                                printf("A czy moze jest to %d\n", ++proba);
                            else
                                printf("Rozumiem tylko t lub n.\n");
                        }
                    }
                    else if (odpowiedz == 'n')
                    {
                        while (getchar() != '\n')
                            continue;
                        printf("No wiec moze jest wieksza od 5?\n");
                        if (odpowiedz == 't')                           // dla liczb od 6 do 10
                        {
                            while (getchar() != '\n')
                                continue;
                            proba = 6;
                            printf("To moze jest to %d?\n", proba);
                            while ((odpowiedz = getchar()) != 't')
                            {
                                while (getchar() != '\n')
                                    continue;
                                if (odpowiedz == 't')
                                    goto przerwanie;
                                else if (odpowiedz == 'n')
                                    printf("A czy moze jest to %d\n", ++proba);
                                else
                                    printf("Rozumiem tylko t lub n.\n");
                            }
                        }
                        else if (odpowiedz == 'n')                      // dla liczb od 1 do 5
                        {
                            while (getchar() != '\n')
                                continue;
                            proba = 1;
                            printf("To moze jest to %d?\n", proba);
                            while ((odpowiedz = getchar()) != 't')
                            {
                                if (odpowiedz == 't')
                                    goto przerwanie;
                                else if (odpowiedz == 'n')
                                    printf("A czy moze jest to %d\n", ++proba);
                                else
                                    printf("Rozumiem tylko t lub n.\n");
                            }
                        }
                        else
                            printf("Rozumiem tylko t lub n.\n");
                    }
                    else
                        printf("Rozumiem tylko t lub n.\n");
                }
                else
                    printf("Rozumiem tylko t lub n.\n");
            }
            else
                printf("Rozumiem tylko t lub n.\n");
        }
        else if (odpowiedz == 'n')
        {
            while (getchar() != '\n')
                continue;
            printf("Czy jest wieksza od 75?\n");
            while ((odpowiedz2 = getchar()) != EOF)
            {
            if (odpowiedz == 't')
            {
                while (getchar() != '\n')
                    continue;
                printf("A czy jest wieksza od 85?\n");
                if (odpowiedz == 't')
                {
                    while (getchar() != '\n')
                        continue;
                    printf("A czy jest wieksza od 90?\n");
                    if (odpowiedz == 't')
                    {
                        while (getchar() != '\n')
                            continue;
                        printf("A czy jest wieksza lub rowna 95?\n");
                        if (odpowiedz == 't')                           // dla liczb od 95 do 100
                        {
                            while (getchar() != '\n')
                                continue;
                            proba = 95;
                            printf("Wiec czy to jest %d?\n", proba);
                            while ((odpowiedz = getchar()) != 't')
                            {
                                while (getchar() != '\n')
                                    continue;
                                if (odpowiedz == 't')
                                    goto przerwanie;
                                else if (odpowiedz == 'n')
                                    printf("A moze wiec %d?\n", ++proba);
                                else
                                    printf("Rozumiem tylko t lub n.\n");
                            }
                        }
                        else if (odpowiedz == 'n')
                        {
                            while (getchar() != '\n')
                                continue;
                            proba = 90;
                            printf("A wiec moze to jest %d?\n", proba); // dla liczb od 90 do 94
                            while ((odpowiedz = getchar()) != 't')
                            {
                                while (getchar() != '\n')
                                    continue;
                                if (odpowiedz == 't')
                                    goto przerwanie;
                                else if (odpowiedz == 'n')
                                    printf("O to moze %d?\n", ++proba);
                                else
                                    printf("Rozumiem tylko t lub n.\n");
                            }
                        }
                        else
                            printf("Rozumiem tylko t lub n.\n");
                    }
                    else if (odpowiedz == 'n')                          // dla liczb od 86 do 89
                    {
                        while (getchar() != '\n')
                                continue;
                        proba = 86;
                        printf("Czy to jest %d?\n", proba);
                        if (odpowiedz == 't')
                            goto przerwanie;
                        else if (odpowiedz == 'n')
                            printf("O to moze %d?\n", ++proba);
                        else
                            printf("Rozumiem tylko t lub n.\n");
                    }
                    else
                        printf("Rozumiem tylko t lub n.\n");
                }
                else if (odpowiedz == 'n')
                {
                    while (getchar() != '\n')
                        continue;
                    printf("Czy jest wieksza od 80?\n");
                    if (odpowiedz == 't')                               // dla liczb od 81 do 85
                    {
                        while (getchar() != '\n')
                            continue;
                        proba = 81;
                        printf("To w takim razie czy to jest %d\n", proba);
                        while ((odpowiedz = getchar()) != 't')
                        {
                            while (getchar() != '\n')
                                continue;
                            if (odpowiedz == 't')
                                goto przerwanie;
                            else if (odpowiedz == 'n')
                                printf("O to moze %d?\n", ++proba);
                        }
                    }
                    else if (odpowiedz == 'n')                          // dla liczb od 76 do 80
                    {
                        while (getchar() != '\n')
                                continue;
                        proba = 76;
                        printf("Hmm to moze to %d\n", proba);
                        while ((odpowiedz = getchar()) != 't')
                        {
                            while (getchar() != '\n')
                                continue;
                            if (odpowiedz == 't')
                                goto przerwanie;
                            else if (odpowiedz == 'n')
                                printf("O to moze %d?\n", ++proba);
                        }
                    }
                    else
                        printf("Rozumiem tylko t lub n.\n");
                }
                else
                    printf("Rozumiem tylko t lub n.\n");
            }
            else if (odpowiedz == 'n')
            {
                while (getchar() != '\n')
                    continue;
                printf("Ok, moze wiec wieksza od 65?\n");
                if (odpowiedz == 't')
                {
                    while (getchar() != '\n')
                        continue;
                    printf("Czy jest wieksza od 70?\n");
                    if (odpowiedz == 't')                           // dla liczb od 71 do 75
                    {
                        while (getchar() != '\n')
                            continue;
                        proba = 71;
                        printf("Czy to jest wiec %d?\n", proba);
                        while ((odpowiedz = getchar()) != 't')
                        {
                            if (odpowiedz == 't')
                                goto przerwanie;
                            else if (odpowiedz == 'n')
                                printf("A moze to jest %d\n", ++proba);
                        }
                    }
                    else if (odpowiedz == 'n')                      // dla liczb od 65 do 70
                    {
                        while (getchar() != '\n')
                            continue;
                        proba = 65;
                        printf("A wiec moze to jest %d?\n", proba);
                        while ((odpowiedz = getchar()) != 't')
                        {
                            while (getchar() != '\n')
                                continue;
                            if (odpowiedz == 't')
                                goto przerwanie;
                            else if (odpowiedz == 'n')
                                printf("No to moze %d?\n", ++proba);
                        }
                    }
                    else
                        printf("Rozumiem tylko t lub n.\n");
                }
                else if (odpowiedz == 'n')
                {
                    while (getchar() != '\n')
                        continue;
                    printf("Czy jest wieksza od 55?\n");
                    if (odpowiedz == 't')                           // dla liczb od 56 do 60
                    {
                        while (getchar() != '\n')
                            continue;
                        proba = 56;
                        printf("Czy to jest %d?\n", proba);
                        while ((odpowiedz = getchar()) != 't')
                        {
                            while (getchar() != '\n')
                                continue;
                            if (odpowiedz == 't')
                                goto przerwanie;
                            else if (odpowiedz == 'n')
                                printf("W takim razie czy to %d?\n", ++proba);
                        }

                    }
                    else if (odpowiedz == 'n')                      // dla liczb od 50 do 55
                    {
                        while (getchar() != '\n')
                            continue;
                        proba = 50;
                        printf("Czy to jest %d?\n", proba);
                        while ((odpowiedz = getchar()) != 't')
                        {
                            while (getchar() != '\n')
                                continue;
                            if (odpowiedz == 't')
                                goto przerwanie;
                            else if (odpowiedz == 'n')
                                printf("A wiec czy moze to %d?\n", ++proba);
                            else
                                printf("Rozumiem tylko t lub n.\n");
                        }
                    }
                    else
                        printf("Rozumiem tylko t lub n.\n");
                }
                else
                    printf("Rozumiem tylko t lub n.\n");
            }
            else
                printf("Rozumiem tylko t lub n.\n");
            }
        }
        else
            printf("Rozumiem tylko t lub n.\n");
        przerwanie: break;
        while ((odpowiedz = getchar()) != '\n')
            continue;
    }
    printf("\nWiedzialem, ze mi sie uda!\n");
    while (getchar() != '\n')
        continue;
    return 0;
}

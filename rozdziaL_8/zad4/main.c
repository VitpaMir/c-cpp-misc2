#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#define TAK 1
#define NIE 0

int main()
{
    char ch;
    float srednia = 0.0;
    int liczba_liter = 0;
    int liczba_slow = 0;
    int wslowie = NIE;

    while ((ch = getchar()) != EOF)
    {
        if (isalpha(ch))
            liczba_liter++;
        if (isalpha(ch) && wslowie == NIE)
            wslowie = TAK;
        if (isspace(ch) && wslowie == TAK)
        {
            liczba_slow++;
            wslowie = NIE;
        }
        if (ispunct(ch))
            continue;
        srednia = (float) liczba_liter/liczba_slow;
    }
    printf("Liczba slow %d, liczba liter %d, srednia liczba liter w slowie: %.2f", liczba_slow, liczba_liter, srednia);
    return 0;
}

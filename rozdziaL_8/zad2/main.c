#include <stdio.h>
#include <stdlib.h>

int main()
{
    char ch;
    int i = 0;

    while ((ch = getchar()) != EOF)
    {
        i++;
        if (ch < ' ')
        {
        if (ch == '\n')
            printf("%d - \\n ", ch);
        else if (ch == '\t')
            printf("%d - \\t ", ch);
        else
            printf("^%d - %c ", ch + 64, ch);
        }
        else
            printf("%d - %c ", ch, ch);
        if (i == 10)
        {
            putchar('\n');
            i = 0;
        }

    }
    putchar('\n');
    return 0;
}

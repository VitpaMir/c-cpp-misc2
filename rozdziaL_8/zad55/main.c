#include <stdio.h>
#include <stdlib.h>

int main()
{
    int proba = 50;
    char odpowiedz;
    char odpowiedz2;
    char odpowiedz3;
    int granica_dolna = 0;
    int granica_gorna = 0;
    int liczba;

    printf("Wybierz liczbe od 0 do 100. Sprobuje ja zgadnac.");
    printf("\nNapisz t, jesli moja proba jest udana lub");
    printf("\nn, jesli nie trafilem.\n");
    printf("Jesli chcesz wyjsc, zasymuluj koniec pliku ctrl+z.\n");
    printf("Hmm... czy Twoja liczba jest wieksza od %d?\n", proba); // czy jest wieksza od 50?
    printf("Jesli jest rowna %d wpisz z.\n", proba);
    while ((odpowiedz = getchar()) != 'z')
    {
        if (odpowiedz == 't')                       // dla liczb od 75 do 100
        {
            if (granica_dolna == 99)
            {
                liczba = 100;
                printf("Czyli to %d!\n", liczba);
                break;
            }
            granica_dolna = proba;
            proba = proba + (100 - proba) / 2;
            granica_gorna = proba;
            printf("Czy liczba jest wieksza od %d?\n", granica_gorna);
        }
        if (odpowiedz == 'n' && proba <= 50)   // dla liczb do 50
        {
            granica_gorna = proba;
            proba = (proba - 1)/ 2;
            granica_dolna = proba;
            printf("Czy liczba jest moze mniejsza od %d?\n", granica_dolna); // czy jest mniejsza od 25?

            while ((odpowiedz3 = getchar()) != 'z')
            {
                if (odpowiedz3 == 't')              // dla liczb od 0 do 25
                {
                    granica_gorna = proba;
                    proba = (proba - 1)/ 2;
                    granica_dolna = proba;
                    if (granica_dolna == 0)
                    {
                        liczba = 1;
                        printf("Wiec twoja liczba to %d! Wpisz z, aby zakonczyc.\n", liczba);
                        break;
                    }
                    printf("Czy Twoja liczba jest mniejsza od %d?\n", granica_dolna);
                }
                if (odpowiedz3 == 'n')
                {
                    printf("Twoja liczba miesci sie miedzy %d, a %d.\n", granica_dolna, granica_gorna);
                    printf("Czy Twoja liczba to %d?\n", granica_dolna);
                    while ((odpowiedz3 = getchar()) != 't')                                                 // dla liczb miedzy 6 a 12
                    {
                        if (granica_dolna < granica_gorna && odpowiedz3 == 'n')                                                  // dziala ok, narazie
                        {
                            granica_dolna++;
                            if (granica_dolna == granica_gorna)
                            {
                                printf("A wiec to %d! Wpisz z jak zakonczyc.\n", granica_dolna);
                                liczba = granica_dolna;
                                break;
                            }
                            printf("Czy twoja liczba to %d?\n", granica_dolna);
                        }
                    }
                    if (odpowiedz3 == 't')
                    {
                        printf("A wiec Twoja liczba to %d, aby zakonczyc wpisz z.\n", granica_dolna);
                        liczba = granica_dolna;
                        break;
                    }
                }
            }
        }
        if (odpowiedz == 'n' && proba > 50)        //dla liczb od 50 do 75
        {
            //granica_gorna = proba;
            //proba = (proba + 50) / 2;
            //granica_dolna = proba;
            printf("Twoja liczba miesci sie miedzy %d, a %d.\n", granica_dolna, granica_gorna);
            printf("Czy Twoja liczba to %d?\n", granica_dolna);
            while ((odpowiedz3 = getchar()) != 't')
            {
                if (granica_dolna < granica_gorna && odpowiedz3 == 'n')
                {
                    granica_dolna++;
                    if (granica_dolna == granica_gorna)
                    {
                        printf("Czyli Twoja liczba to %d! Aby zakonczyc wpisz z i kliknij enter.\n", granica_dolna);
                        liczba = granica_dolna;
                        break;
                    }
                    printf("Czy twoja liczba to %d?\n", granica_dolna);
                }
            }
            if (odpowiedz3 == 't')
            {
                printf("Zgadlem to %d.\n", granica_dolna);
                liczba = granica_dolna;
                break;
            }
        }
        while (getchar() != '\n')
            continue;
    }
    if(proba == 50)
    {
        liczba = proba;
    }
    printf("Wiedzialem, ze mi sie uda!, twoja liczba to %d\n", liczba);
    return 0;
}

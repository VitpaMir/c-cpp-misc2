#include <stdio.h>
#include <stdlib.h>

int main()
{
    char ch;
    int licznik = 0;

    printf("Wpisz ciag znakow i zakoncz znakiem konca pliku ctrl+z.\n");
    while ((ch = getchar()) != EOF)
    {
        licznik++;
    }
    printf("Jest %d znakow w tym ciagu znakow.\n", licznik);
    return 0;
}

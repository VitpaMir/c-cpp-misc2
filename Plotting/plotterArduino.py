#! python3
import re

#import numpy as np
import matplotlib.pyplot as plt
import serial
from drawnow import *

serialArduino = serial.Serial('COM7', 9600)
tempWire = []
tempFan1 = []
tempFan2 = []
temperatures = [[], [], []]
c = 0
flag = 0
plt.ion()

def plotValues():
    if flag == 0:
        plt.get_current_fig_manager().full_screen_toggle() # ctrl + f to disable
    plt.title('Cooling system temperatures')
    plt.grid(True)
    plt.ylabel('Temperatures [\xb0C]')
    plt.plot(temperatures[0], 'r.-', label='Wire temperature')
    plt.plot(temperatures[1], 'b--', label='Fan cold temperature')
    plt.plot(temperatures[2], 'g-', label='Fan hot temperature')
    plt.legend(loc='upper left')
    for i in range(len(temperatures[0]) - 1, len(temperatures[0])):
        plt.figtext(0.91, 0.60, 'Twire: ' + str(temperatures[0][i]) + '[\xb0C]', fontsize=8)
        plt.figtext(0.91, 0.55, 'TfanC: ' + str(temperatures[1][i]) + '[\xb0C]', fontsize=8)
        plt.figtext(0.91, 0.5, 'TfanH: ' + str(temperatures[2][i]) + '[\xb0C]', fontsize=8)
    plt.ticklabel_format(useOffset=False) 
    # - nie bedzie poprawial skali na biezaco

while True:
    while (serialArduino.inWaiting() == 0):
        pass
    valueRead = serialArduino.readline().decode('utf-8')
    try:
        dataArray = str(valueRead).split(sep=',')
        for i in range(0, len(dataArray)):
            if re.search(r'[-+]?\d+\.\d+', dataArray[i]):
                temperatures[i].append(float(dataArray[i]))
        drawnow(plotValues)
        flag += 1
        plt.pause(.000001)
    except IndexError:
        print('Index Error raised')
        c += 1
    except ValueError:
        print('Value Error raised')
        c += 1
    if c > 5:
        break
    # dodatkowe zerowanie list wartosci, 
    # wykres od zera bez starych danych
    # terminate button
    # clear data button in figure window
    # rownoczesny zapis do pliku .csv lub .xlsx
    # sprawdzanie wartosci, sprawdzanie ilosci danych

from PyPDF2 import PdfFileMerger
import re, os

pdfs = []
filePath = 'C:\\Users\\hj61t7\\Downloads\\docs'
regPdf = re.compile(r'.+\.pdf')

for file in os.listdir(filePath):
    if regPdf.match(file):
        pdfs.append(file)

merger = PdfFileMerger()

merger.append(filePath + '\\' + pdfs[0], pages=(0, 1))
merger.append(filePath + '\\' + pdfs[1], pages=(1, 2))
merger.append(filePath + '\\' + pdfs[0], pages=(1, 2))
merger.append(filePath + '\\' + pdfs[1], pages=(0, 1))


''' for pdf in pdfs:
    merger.append(filePath + '\\' + pdf, pages=(2, 1)) '''

merger.write(filePath + '\\' + "result2.pdf")
merger.close()
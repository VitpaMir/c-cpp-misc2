#! python3
# fusePlots.py - take data from .csv and make plots
# from fuse tests 11.08.2021

import matplotlib.pyplot as plt
import numpy as np
import funPlots

''' def fromCsvDataGet(measPath):
    measFile = open(measPath)
    measReader = csv.reader(measFile)
    measData = list(measReader)
    regVal = re.compile(r'[-]?(\d+\.)(\d+)?')
    RegChal = re.compile(r'CH10\d')
    regTime = re.compile(r'\w+ Time')
    data = []
    data_temp = []
    for i in measData:
        for j in range(len(i)):
            if regVal.match(str(i[j])):
                data_temp.append(float(i[j]))
        data.append(data_temp)
        data_temp = []
    data = list(filter(None, data))
    data_lin = []
    for i in range(len(data[0])):
        for j in range(len(data)):
            data_temp.append(data[j][i])
        data_lin.append(data_temp)
        data_temp = []
    print('Czy zapisac plik ?(Would you like to save in .xlsx file?')
    print('Wpisz y i enter jesli tak.')
    if input() == 'y':
        destPath = 'C:\\Users\hj61t7\\OneDrive - Aptiv\\Pulpit\\Measurements\\Test1'
        noww = datetime.now()
        sTime = noww.strftime('%Y%m%d_%H%M%S')
        wb = openpyxl.Workbook()
        sheet = wb.active
        sheet.title = 'testtest1'
        for ind1, i in enumerate(data_lin):
            for ind2, j in enumerate(i):
                cellRef = sheet.cell(row = ind2 + 1, column = ind1 + 1)
                cellRef.value = j
        wb.save(destPath + '\\' + 'fuseTest_' + sTime + '.xlsx')
    return data_lin

def plotTimeCut(data_lin, ind1, ind2):
    data_wind = []
    data_time = []
    for i in range(len(data_lin[1])):
        if data_lin[1][i] >= 35e-4 and len(data_time) < 10:
            data_time.append(data_lin[ind1][i - 10])
            data_wind.append(data_lin[ind2][i - 10])

    for i in range(len(data_lin[1])):
        if data_lin[1][i] >= 35e-4 and data_lin[1][i] < 37e-4:
            data_time.append(data_lin[ind1][i])
            data_wind.append(data_lin[ind2][i])
    j = 0
    for i in range(len(data_lin[1])):
        if j < 1:
            if data_lin[1][i] > 37e-4:
                data_time.append(data_lin[ind1][i])
                data_wind.append(data_lin[ind2][i])
                j += 1
    """ data_time = zaokragl(data_time)
    data_wind = zaokragl(data_wind) """
    return data_time, data_wind '''

""" def rysujMiWykres(data_time, data_wind):
    data_time_ind = range(len(data_time))
    data_time_ind1 = range(len(data_time1))

    plt.figure(0)
    plt.plot(data_time_ind, data_wind, 'b--')
    plt.plot(data_time_ind1, data_wind1, 'r--')
    plt.xticks(np.arange(0, 1200, 200))
    plt.yticks(np.arange(25, 75, 5))
    plt.title('Fuse thermal test - 35 A')
    plt.xlabel('Time [s]')
    plt.ylabel('Temperature [\xb0C]')
    plt.legend(['Fuse 1', 'Fuse 2'], loc='lower right')
    plt.grid(True)
    plt.show() """

""" measPath = 
    measFile = open(sys.argv[1]) """
data_lin = funPlots.fromCsvDataGet('C:\\Users\\hj61t7\\OneDrive - Aptiv\\Pulpit\\Measurements\\Test1\\fuseTest1_0811_164037.csv')
data_lin2 = funPlots.fromCsvDataGet('C:\\Users\\hj61t7\\OneDrive - Aptiv\\Pulpit\\Measurements\\Test2\\fuseTest1_0811_212906.csv')
destPath = 'C:\\Users\hj61t7\\OneDrive - Aptiv\\Pulpit\\Measurements\\Test1'

#bezpiecznik
data_time, data_wind = funPlots.plotTimeCut(data_lin, 0, 2)
data_time1, data_wind1 = funPlots.plotTimeCut(data_lin2, 0, 2)

data_time_ind = np.arange(len(data_time))*10
data_time_ind1 = np.arange(len(data_time1))*10

plt.figure(0)
plt.plot(data_time_ind, data_wind, 'b--')
plt.plot(data_time_ind1, data_wind1, 'r--')
plt.title('Fuse thermal test - 35 A (fuse internal)')
plt.xlabel('Time [s]')
plt.ylabel('Temperature [\xb0C]')
plt.legend(['Fuse 1', 'Fuse 2'], loc='lower right')
plt.grid(True)
plt.savefig(destPath + '\\' + 'InternalFuseTemperature.eps', format='eps')
plt.show()

#busbar
data_time3, data_wind3 = funPlots.plotTimeCut(data_lin, 0, 3)
data_time31, data_wind31 = funPlots.plotTimeCut(data_lin2, 0, 3)

plt.figure(1)
plt.plot(data_time_ind, data_wind3, 'b--')
plt.plot(data_time_ind1, data_wind31, 'r--')
plt.title('Fuse thermal test - 35 A (busbar)')
plt.xlabel('Time [s]')
plt.ylabel('Temperature [\xb0C]')
plt.legend(['Fuse 1', 'Fuse 2'], loc='lower right')
plt.grid(True)
plt.savefig(destPath + '\\' + 'BusbarTemperature.eps', format='eps')
plt.show()

#terminal
data_time4, data_wind4 = funPlots.plotTimeCut(data_lin, 0, 4)
data_time41, data_wind41 = funPlots.plotTimeCut(data_lin2, 0, 4)

plt.figure(2)
plt.plot(data_time_ind, data_wind4, 'b--')
plt.plot(data_time_ind1, data_wind41, 'r--')
plt.title('Fuse thermal test - 35 A (4 mm\u00b2 cable terminal)')
plt.xlabel('Time [s]')
plt.ylabel('Temperature [\xb0C]')
plt.legend(['Fuse 1', 'Fuse 2'], loc='lower right')
plt.grid(True)
plt.savefig(destPath + '\\' + 'Cable4mm2Temperature.eps', format='eps')
plt.show()
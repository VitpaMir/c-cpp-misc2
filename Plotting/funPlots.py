import csv, openpyxl, re
import numpy as np
from datetime import datetime
from scipy.interpolate.fitpack import splev, splrep

def zaokragl(data):
    for i in data:
        i = np.around(float(i), 4)
    return data

def plotTimeCut(data_lin, ind1, ind2):
    data_wind = []
    data_time = []
    for i in range(len(data_lin[1])):
        if data_lin[1][i] >= 35e-4 and len(data_time) < 10:
            data_time.append(data_lin[ind1][i - 10])
            data_wind.append(data_lin[ind2][i - 10])

    for i in range(len(data_lin[1])):
        if data_lin[1][i] >= 35e-4 and data_lin[1][i] < 37e-4:
            data_time.append(data_lin[ind1][i])
            data_wind.append(data_lin[ind2][i])
    j = 0
    for i in range(len(data_lin[1])):
        if j < 1:
            if data_lin[1][i] > 37e-4:
                data_time.append(data_lin[ind1][i])
                data_wind.append(data_lin[ind2][i])
                j += 1
    """ data_time = zaokragl(data_time)
    data_wind = zaokragl(data_wind) """
    return data_wind


def fromCsvDataGet(measPath):
    measFile = open(measPath)
    measReader = csv.reader(measFile)
    measData = list(measReader)
    regVal = re.compile(r'[-]?(\d+\.)(\d+)?')
    RegChal = re.compile(r'CH10\d')
    regTime = re.compile(r'\w+ Time')
    data = []
    data_temp = []
    for i in measData:
        for j in range(len(i)):
            if regVal.match(str(i[j])):
                data_temp.append(float(i[j]))
        data.append(data_temp)
        data_temp = []
    data = list(filter(None, data))
    data_lin = []
    for i in range(len(data[0])):
        for j in range(len(data)):
            data_temp.append(data[j][i])
        data_lin.append(data_temp)
        data_temp = []
    print('Czy zapisac plik ?(Would you like to save in .xlsx file?')
    print('Wpisz y i wcisnij enter jesli tak.')
    if input() == 'y':
        destPath = 'C:\\Users\hj61t7\\OneDrive - Aptiv\\Pulpit\\Measurements\\Test1'
        noww = datetime.now()
        sTime = noww.strftime('%Y%m%d_%H%M%S')
        wb = openpyxl.Workbook()
        sheet = wb.active
        sheet.title = 'testtest1'
        for ind1, i in enumerate(data_lin):
            for ind2, j in enumerate(i):
                cellRef = sheet.cell(row = ind2 + 1, column = ind1 + 1)
                cellRef.value = j
        wb.save(destPath + '\\' + 'fuseTest_' + sTime + '.xlsx')
    return data_lin

def data_from_workbooks(filePath):
    wb = openpyxl.load_workbook(filePath)
    measSheet = wb[wb.sheetnames[1]]
    reg = re.compile(r'[-]?[0]|(\d+\.)(\d+)?')
    data = []
    tempListOfvalues = []
    for col in measSheet.iter_cols(values_only=True):
        for value in col:
            if reg.match(str(value)):
                #tempListOfvalues.append(np.around(float(value), 4))
                tempListOfvalues.append(float(value))
        data.append(tempListOfvalues)
        tempListOfvalues = []
    return data

def data_from_workbooks1(filePath):
    wb = openpyxl.load_workbook(filePath)
    measSheet = wb[wb.sheetnames[0]]
    data = []
    tempListOfvalues = []
    for col in measSheet.iter_cols(values_only=True):
        for value in col:
                tempListOfvalues.append(value)
        data.append(tempListOfvalues)
        tempListOfvalues = []
    return data

def smoothin_1(dane1, dane2):
    deltaP = 5
    for i in range(1, len(dane2)):
        if abs(dane2[i] - dane2[i-1]) > deltaP:
            dane2[i] = (dane2[i - 1] + dane2[i + 1])/2
    m = len(dane1)
    smax = m-np.sqrt(2*m)
    smin = m+np.sqrt(2*m)
    sd = (smax + smin)/2
    data_smooth = splrep(dane1, dane2, s=sd)
    xnew = np.linspace(0, max(dane1)+100, len(dane1))
    ynew = splev(xnew, data_smooth)
    return xnew, ynew

def res_calc(dane):
    curr = 500
    res = []
    for i in dane:
        res.append(abs(i)/curr*1000)
    return res
#! python3
# fusePlots2.py - takes data from .csv and generates .xlsx
# with data columns and plots for chosen data
# also sheets need to be chosen
# (mozna jeszcze dopisac zeby dodawal naglowki czyli nazwy kolumn czego dotycza albo jakie kanaly)
import xlsxwriter
import funPlots
import numpy as np

data_lin = funPlots.fromCsvDataGet('C:\\Users\hj61t7\\OneDrive - Aptiv\\Pulpit\\Measurements\\Test1\\fuseTest1_0811_164037.csv')
data_lin2 = funPlots.fromCsvDataGet('C:\\Users\hj61t7\\OneDrive - Aptiv\\Pulpit\\Measurements\\Test2\\FuseTest_0823_151903.csv')
destPath = 'C:\\Users\hj61t7\\OneDrive - Aptiv\\Pulpit\\Measurements\\Test2'

workbook = xlsxwriter.Workbook(destPath + '\\' + 'fusePlotsTest2.xlsx')

worksheet = workbook.add_worksheet('FuseTest1')
worksheet1 = workbook.add_worksheet('FuseTest2')
worksheet2 = workbook.add_worksheet('FusePlot1')
worksheet3 = workbook.add_worksheet('FusePlot2')
plotsheet = workbook.add_worksheet('Plots')

for i in data_lin:
    data_start_loc = [0, data_lin.index(i)]
    data_end_loc = [data_start_loc[0] + len(i), data_lin.index(i)]
    worksheet.write_column(*data_start_loc, data=i)

for i in data_lin2:
    data_start_loc = [0, data_lin2.index(i)]
    data_end_loc = [data_start_loc[0] + len(i), data_lin2.index(i)]
    worksheet1.write_column(*data_start_loc, data=i)

data_wind1 = []
data_wind2 = []
data_wind1.append(funPlots.plotTimeCut(data_lin, 0, 2))
data_wind1.append(funPlots.plotTimeCut(data_lin, 0, 3))
data_wind1.append(funPlots.plotTimeCut(data_lin, 0, 4))
data_wind2.append(funPlots.plotTimeCut(data_lin2, 0, 2))
data_wind2.append(funPlots.plotTimeCut(data_lin2, 0, 3))
data_wind2.append(funPlots.plotTimeCut(data_lin2, 0, 4))

data_time_ind = np.arange(len(data_wind1[0]))*10
data_time_ind1 = np.arange(len(data_wind2[0]))*10
print(data_wind1)
print(data_wind2)
data_start_loc = []
data_end_loc = []
data_start_loc1 = []
data_end_loc1 = []

data_start_loc_t = ([0, 0])
data_end_loc_t = [data_start_loc_t[0] + len(data_time_ind), 0]
worksheet2.write_column(*data_start_loc_t, data=data_time_ind)

data_start_loc1_t = ([0, 0])
data_end_loc1_t = ([data_start_loc1_t[0] + len(data_time_ind1), 0])
worksheet3.write_column(*data_start_loc1_t, data=data_time_ind1)

for i in data_wind1:
    data_start_loc.append([0, data_wind1.index(i) + 1])
    data_end_loc.append([data_start_loc[0][0] + len(i), data_wind1.index(i) + 1])
    worksheet2.write_column(*data_start_loc[data_wind1.index(i)], data=i)

for i in data_wind2:
    data_start_loc1.append([0, data_wind2.index(i) + 1])
    data_end_loc1.append([data_start_loc[0][0] + len(i), data_wind2.index(i) + 1])
    worksheet3.write_column(*data_start_loc[data_wind2.index(i)], data=i)

#plot1
chart1 = workbook.add_chart({'type': 'line'})
chart1.set_y_axis({'name': 'Temperature [\xb0C]'})
chart1.set_x_axis({'name': 'Time [s]'})
chart1.set_title({'name': 'Fuse thermal test - 35 A (fuse internal)'})

chart1.add_series({
    'categories': [worksheet2.name] + data_start_loc[0] + data_end_loc[0],
    'values': [worksheet2.name] + data_start_loc[0] + data_end_loc[0],
    'name': "Test1",
})
chart1.add_series({
    'categories': [worksheet3.name] + data_start_loc1[0] + data_end_loc1[0],
    'values': [worksheet3.name] + data_start_loc1[0] + data_end_loc1[0],
    'name': "Test2",
})
plotsheet.insert_chart('B1', chart1)

#plot2
chart2 = workbook.add_chart({'type': 'line'})
chart2.set_y_axis({'name': 'Temperature [\xb0C]'})
chart2.set_x_axis({'name': 'Time [s]'})
chart2.set_title({'name': 'Fuse thermal test - 35 A (busbar)'})

chart2.add_series({
    'categories': [worksheet2.name] + data_start_loc[1] + data_end_loc[1],
    'values': [worksheet2.name] + data_start_loc[1] + data_end_loc[1],
    'name': "Test1",
})
chart2.add_series({
    'categories': [worksheet3.name] + data_start_loc1[1] + data_end_loc1[1],
    'values': [worksheet3.name] + data_start_loc1[1] + data_end_loc1[1],
    'name': "Test2",
})
plotsheet.insert_chart('C1', chart2)

#plot3
chart3 = workbook.add_chart({'type': 'line'})
chart3.set_y_axis({'name': 'Temperature [\xb0C]'})
chart3.set_x_axis({'name': 'Time [s]'})
chart3.set_title({'name': 'Fuse thermal test - 35 A (4 mm\u00b2 cable terminal)'})

chart3.add_series({
    'categories': [worksheet2.name] + data_start_loc[2] + data_end_loc[2],
    'values': [worksheet2.name] + data_start_loc[2] + data_end_loc[2],
    'name': "Test1",
})
chart3.add_series({
    'categories': [worksheet3.name] + data_start_loc1[2] + data_end_loc1[2],
    'values': [worksheet3.name] + data_start_loc1[2] + data_end_loc1[2],
    'name': "Test2",
})
plotsheet.insert_chart('D1', chart3)

workbook.close()  # Write to file
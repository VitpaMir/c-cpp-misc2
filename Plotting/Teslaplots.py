#! python3
from funPlots import data_from_workbooks
from funPlots import smoothin_1
#import numpy as np
from matplotlib import pyplot as plt
destPath = 'C:\\Users\\hj61t7\\OneDrive - Aptiv\\Dokumenty\\TeslaBusbar Test\\TeslaBusbar600Atest 2021-11-19T14.58.11.xlsx'
data = data_from_workbooks(destPath)

plt.figure(0)
""" plt.plot(data[0], data[1], 'b.')
plt.plot(data[0], data[5], 'mo')
plt.plot(data[0], data[7], 'r+')
plt.plot(data[0], data[8], 'cx')
plt.plot(data[0], data[9], 'gs')
plt.plot(data[0], data[11], 'ks') """
""" plt.plot(data[0], data[1], 'b-')
plt.plot(data[0], data[11], 'k-')
plt.plot(data[0], data[5], 'm-')
plt.plot(data[0], data[7], 'g-')
plt.plot(data[0], data[8], 'r-')
plt.plot(data[0], data[9], 'c-') """
""" xnew0, ynew0 = smoothin_1(data[1], data[2])
xnew1, ynew1 = smoothin_1(data[1], data[14])
xnew2, ynew2 = smoothin_1(data[1], data[6])
xnew3, ynew3 = smoothin_1(data[1], data[8])
xnew4, ynew4 = smoothin_1(data[1], data[9])
xnew5, ynew5 = smoothin_1(data[1], data[10]) """
plt.plot(data[0], data[2], 'b-')
plt.plot(data[0], data[3], 'm-')
plt.plot(data[0], data[4], 'g-')
''' plt.plot(xnew0, ynew0, 'b--')
plt.plot(xnew1, ynew1, 'm--')
plt.plot(xnew2, ynew2, 'g--')
plt.plot(xnew3, ynew3, 'r--')
plt.plot(xnew4, ynew4, 'c--')
plt.plot(xnew5, ynew5, 'k--') '''
plt.title('Tesla Busbar temperatures rise - inside')
plt.xlabel('Time [s]')
plt.ylabel('\u0394 Temperature [\xb0C]')
''' plt.xticks(np.arange(0, 1200, 200))
plt.yticks(np.arange(25, 75, 5)) '''
#plt.legend(['1.1', '1.3', '1.6', '2.1', '2.2', '2.3'], loc='lower right')
plt.legend(['1.2', '1.4', '1.5'], loc='lower right')
plt.grid(True)
#plt.savefig(destPath + '\\' + 'Busbar_TemperatureInTime.png')
plt.show()
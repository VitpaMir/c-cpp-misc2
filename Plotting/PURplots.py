#! Python3
# PURplots.py - makes plots from PUR cables measurements
import openpyxl, matplotlib.pyplot as plt, re, numpy as np
from scipy.interpolate.fitpack import splev, splrep

def data_from_workbooks(filePath):
    wb = openpyxl.load_workbook(filePath)
    measSheet = wb[wb.sheetnames[0]]
    reg = re.compile(r'[-]?(\d+\.)(\d+)?')
    data = []
    tempListOfvalues = []
    for col in measSheet.iter_cols(values_only=True):
        for value in col:
            if reg.match(str(value)):
                tempListOfvalues.append(float(value))
        data.append(tempListOfvalues)
        tempListOfvalues = []
    return data

def smoothin_1(dane1, dane2, type):
    deltaP = 2
    for i in range(1, len(dane1)):
        if dane1[i] < dane1[i - 1]:
            dane1[i] = dane1[i - 1]
    for i in range(1, len(dane2)):
        if abs(dane2[i] - dane2[i - 1]) > deltaP:
            dane2[i] = (dane2[i - 1] + dane2[i + 1])/2
    #m = len(dane1)
    #smax = m-np.sqrt(2*m)
    #smin = m+np.sqrt(2*m)
    #sd = (smax + smin)/2
    data_smooth = splrep(dane1, dane2, s=type)
    xnew = np.linspace(min(dane1), max(dane1)+max(dane1)*0.1, len(dane1))
    ynew = splev(xnew, data_smooth)
    return xnew, ynew

def res_calc2(dane):
    curr = 400
    res = []
    for i in dane:
        res.append(abs((i))/curr*1000)
    return res

destPath = 'C:\\Users\\hj61t7\\OneDrive - Aptiv\\Pulpit\\Pur_termo_22.07.2021'
measPath1 = 'C:\\Users\\hj61t7\\OneDrive - Aptiv\\Pulpit\\Pur_termo_22.07.2021\\300A_0723_161507.xlsx'
data = data_from_workbooks(measPath1)
# smooth data curves plots
plt.figure(0)
plt.plot(data[0], data[1], 'b.')
plt.plot(data[0], data[2], 'r.')
plt.plot(data[0], data[3], 'g.')
xnew0, ynew0 = smoothin_1(data[0], data[1], 3)
xnew1, ynew1 = smoothin_1(data[0], data[2], 3)
xnew2, ynew2 = smoothin_1(data[0], data[3], 3)
plt.plot(xnew0, ynew0, 'b--')
plt.plot(xnew1, ynew1, 'r--')
plt.plot(xnew2, ynew2, 'g--')
plt.title('PUR heating charasteristic in time - 300 A')
plt.xlabel('Time [s]')
plt.ylabel('Temperature [\xb0C]')
''' plt.xticks(np.arange(0, 1200, 200))
plt.yticks(np.arange(25, 75, 5)) '''
plt.legend(['Sample no.1', 'Sample no.3', 'Sample no.5'], loc='lower right')
plt.grid(True)
plt.savefig(destPath + '\\' + 'PUR_time_char_300A.png')
plt.show()

#resistance plots in temperature
#current = []
''' for i, j in zip(data[6], data[7]):
    current.append((abs(i) + abs(j))*500/0.05) '''
print('Calculate resistances? y - yes, other character - no.')
if (input() == 'y'):
    res0 = res_calc2(data[4])
    res1 = res_calc2(data[5])
    res2 = res_calc2(data[8])
    plt.figure(1)
    plt.plot(data[1], res0, 'b.')
    plt.plot(data[2], res1, 'r.')
    plt.plot(data[3], res2, 'g.')
    try:
        z1 = np.polyfit(data[1], res0, 1)
        z2 = np.polyfit(data[2], res1, 1)
        z3 = np.polyfit(data[3], res2, 1)
        yres0 = np.poly1d(z1)
        yres1 = np.poly1d(z2)
        yres2 = np.poly1d(z3)
        plt.plot(data[1], yres0(data[1]), 'b--')
        plt.plot(data[2], yres1(data[2]), 'r--')
        plt.plot(data[3], yres2(data[3]), 'g--')
    except TypeError:
        print('TypeError')
    plt.title('PUR thermal characteristics resistance - 300 A')
    plt.xlabel('Temperature [\xb0C]')
    plt.ylabel('Resistance [m\u03A9]')
    plt.legend(['Sample no.1', 'Sample no.3', 'Sample no.5'], loc='lower right')
    plt.grid(True)
    plt.savefig(destPath + '\\' + 'PUR_ResInTemp_300A.png')
    plt.show()
else:
    print('Done')
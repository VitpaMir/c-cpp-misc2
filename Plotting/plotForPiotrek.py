#! Python3
# plotForPiotrek.py - makes plots from busbar measurements
# TODO: dodac otwieranie z konsoli konkretne pliku jak wszystko pojdzie ok
import openpyxl, matplotlib.pyplot as plt, re, numpy as np
from scipy.interpolate.fitpack import splev, splrep

def data_from_workbooks(filePath):
    wb = openpyxl.load_workbook(filePath)
    measSheet = wb[wb.sheetnames[0]]
    reg = re.compile(r'[-]?(\d+\.)(\d+)?')
    data = []
    tempListOfvalues = []
    for col in measSheet.iter_cols(values_only=True):
        for value in col:
            if reg.match(str(value)):
                #tempListOfvalues.append(np.around(float(value), 4))
                tempListOfvalues.append(float(value))
        data.append(tempListOfvalues)
        tempListOfvalues = []
    return data

def smoothin_1(dane1, dane2):
    deltaP = 5
    for i in range(1, len(dane2)):
        if abs(dane2[i] - dane2[i-1]) > deltaP:
            dane2[i] = (dane2[i - 1] + dane2[i + 1])/2
    m = len(dane1)
    smax = m-np.sqrt(2*m)
    smin = m+np.sqrt(2*m)
    sd = (smax + smin)/2
    data_smooth = splrep(dane1, dane2, s=sd)
    xnew = np.linspace(0, max(dane1)+100, len(dane1))
    ynew = splev(xnew, data_smooth)
    return xnew, ynew

def res_calc(dane):
    curr = 500
    res = []
    for i in dane:
        res.append(abs(i)/curr*1000)
    return res


destPath = 'C:\\Users\\hj61t7\\OneDrive - Aptiv\\Pulpit\\busbar_thermo_26.07.2021'
measPath1 = 'C:\\Users\\hj61t7\\OneDrive - Aptiv\\Pulpit\\busbar_thermo_26.07.2021\\busBend_S1_0726_190322.xlsx'
data = data_from_workbooks(measPath1)
measPath2 = 'C:\\Users\\hj61t7\\OneDrive - Aptiv\\Pulpit\\busbar_thermo_26.07.2021\\busStraight_S1_0726_173928.xlsx'
data1 = data_from_workbooks(measPath2)
measPath3 = 'C:\\Users\\hj61t7\\OneDrive - Aptiv\\Pulpit\\busbar_thermo_26.07.2021\\busBend_S2_0726_204330.xlsx'
data2 = data_from_workbooks(measPath3)
measPath4 = 'C:\\Users\\hj61t7\\OneDrive - Aptiv\\Pulpit\\busbar_thermo_26.07.2021\\busStraight_S2_0726_195908.xlsx'
data3 = data_from_workbooks(measPath4)
#zapis w pliku, do dodanie pozniej (lepiej zapisac w excelu albo dat jakims)
""" dataFile = open(destPath + '\\' + 'dataFile.txt', 'w')
for i in data:
    for j in i:
        dataFile.write(j)
dataFile.close() """

# smooth data curves plots
plt.figure(0)
plt.plot(data[0], data[3], 'b.')
plt.plot(data1[0], data1[3], 'r.')
plt.plot(data2[0], data2[3], 'g.')
plt.plot(data3[0], data3[3], 'k.')
xnew0, ynew0 = smoothin_1(data[0], data[3])
xnew1, ynew1 = smoothin_1(data1[0], data1[3])
xnew2, ynew2 = smoothin_1(data2[0], data2[3])
xnew3, ynew3 = smoothin_1(data3[0], data3[3])
plt.plot(xnew0, ynew0, 'r--')
plt.plot(xnew1, ynew1, 'b--')
plt.plot(xnew2, ynew2, 'g--')
plt.plot(xnew3, ynew3, 'k--')
plt.title('Busbar thermal characteristics comparison - 500 A')
plt.xlabel('Time [s]')
plt.ylabel('Temperature [\xb0C]')
plt.xticks(np.arange(0, 1200, 200))
plt.yticks(np.arange(25, 75, 5))
plt.legend(['Bent sample no.1', 'Straight sample no.1', 'Bent sample no.2', 'Straight sample no.2'], loc='lower right')
plt.grid(True)
#plt.savefig(destPath + '\\' + 'Busbar_TemperatureInTime.png')
plt.show()

#resistance plots in temperature
res0 = res_calc(data[4])
res1 = res_calc(data1[4])
res2 = res_calc(data2[4])
res3 = res_calc(data3[4])
plt.figure(1)
plt.plot(data[3], res0, 'b-.')
plt.plot(data1[3], res1, 'r-.')
plt.plot(data2[3], res2, 'g-.')
plt.plot(data3[3], res3, 'k-.')
plt.title('Busbar thermal characteristics resistance - 500 A')
plt.xlabel('Temperature [\xb0C]')
plt.ylabel('Resistance [m\u03A9]')
plt.legend(['Bent sample no.1', 'Straight sample no.1', 'Bent sample no.2', 'Straight sample no.2'], loc='lower right')
plt.grid(True)
plt.savefig(destPath + '\\' + 'Busbar_ResInTemp.png')
plt.show()
#! python3
# smoothing.py - calculate splines for given data
# saves data to a .xlsx file
#mozna rozszerzyc o dane z pliku np. excela

from scipy.interpolate.fitpack import splev, splrep
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import UnivariateSpline
import openpyxl, datetime
def smoothin_1(dane1, dane2):
    """ deltaP = 5
    for i in range(1, len(dane2)):
        if abs(dane2[i] - dane2[i-1]) > deltaP:
            dane2[i] = (dane2[i - 1] + dane2[i + 1])/2 """
    m = len(dane1)
    smax = m-np.sqrt(2*m)
    smin = m+np.sqrt(2*m)
    sd = (smax + smin)/2
    data_smooth = splrep(dane1, dane2, s=2)
    xnew = np.linspace(0, max(dane1)+1, len(dane1)*100)
    ynew = splev(xnew, data_smooth)
    return xnew, ynew

def smoothin_2(dane1, dane2):
    xnew = np.linspace(min(dane1)-10, max(dane1), len(dane1)*100)
    s = UnivariateSpline(dane1, dane2)
    s.set_smoothing_factor(0.5)
    ynew = s(xnew)
    return xnew, ynew

datac = [0, 10, 16, 22, 24, 27, 29, 31, 33, 35, 37]
datac1 = [37, 35, 33, 31, 29, 27, 24, 22, 16, 10, 0]
tempAv = [0.00,	8.36,	22.84,	45.48,	55.57,	72.84,	86.69,	102.07,	119.04,	138.47,	160.88]
temp_marg = [0.00, 10.04, 27.40, 54.57, 66.68, 87.41, 104.03, 122.48, 142.85, 166.17, 193.06]
derating = [-23.06, 3.83, 27.15, 47.52, 65.97, 82.59, 103.32, 115.43, 142.6, 159.96, 170.0]
xnew0, ynew0 = smoothin_1(datac, tempAv)
xnew1, ynew1 = smoothin_1(datac, temp_marg)
xnew2, ynew2 = smoothin_2(derating, datac1)

plt.figure(0)
plt.plot(datac, tempAv, 'b.')
plt.plot(datac, temp_marg, 'r.')

plt.plot(xnew0, ynew0, 'b--')
plt.plot(xnew1, ynew1, 'r--')
plt.title('Temperature increase curves of 40 A 10EV Fuse')
plt.xlabel('Current [A]')
plt.ylabel('Temperature [\xb0C]')
plt.xticks(np.arange(0, 40, 5))
plt.yticks(np.arange(0, 250, 50))
plt.legend(['without safety margin', '20%% safety margin added'], loc='lower right')
plt.grid(True)
plt.show()

data_lin = []
data_lin.append(xnew0)
data_lin.append(ynew0)
data_lin.append(xnew1)
data_lin.append(ynew1)
data_lin.append(xnew2)
data_lin.append(ynew2)

destPath = 'C:\\Users\\hj61t7\\OneDrive - Aptiv\\Pulpit\\Measurements\\2wayFuseSplice'
noww = datetime.datetime.now()
sTime = noww.strftime('%Y%m%d_%H%M%S')
wb = openpyxl.Workbook()
sheet = wb.active
sheet.title = 'testtest1'

for ind1, i in enumerate(data_lin):
    for ind2, j in enumerate(i):
            cellRef = sheet.cell(row = ind2 + 1, column = ind1 + 1)
            cellRef.value = j
wb.save(destPath + '\\' + 'smooth_data' + sTime + '.xlsx')



print(xnew2)
print(ynew2)
plt.figure(1)
plt.plot(derating, datac1, 'b.')
plt.plot(xnew2, ynew2, 'r--')
plt.title('Derating curve of 40 A 10EV Fuse')
plt.ylabel('Current [A]')
plt.xlabel('Temperature [\xb0C]')
plt.grid(True)
plt.show()
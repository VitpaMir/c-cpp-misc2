#! python3
# csvHandler.py - make .csv file easy to work with
# csvHandler.py <csvFilePath> - load .csv file from given file path
# converts csv, take first elements from row and make column
# finally make .xlsx with time and value columns

import csv, openpyxl, re, os
from datetime import datetime

def fromCsvDataGet(measPath):
    measFile = open(measPath)
    measReader = csv.reader(measFile)
    measData = list(measReader)
    regVal = re.compile(r'[-]?(\d+\.)(\d+)?')
    RegChal = re.compile(r'CH10\d')
    regTime = re.compile(r'\w+ Time')
    data = []
    data_temp = []
    for i in measData:
        for j in range(len(i)):
            if regVal.match(str(i[j])):
                data_temp.append(float(i[j]))
            ''' elif regTime.match(str(i[j])) or RegChal.match(str(i[j])):
                data_temp.append(str(i[j])) '''
        data.append(data_temp)
        data_temp = []
    data = list(filter(None, data))
    data_lin = []
    for i in range(len(data[0])):
        for j in range(len(data)):
            data_temp.append(data[j][i])
        data_lin.append(data_temp)
        data_temp = []
    print(data_lin)
    print('Czy zapisac plik ? (Would you like to save data in .xlsx file?')
    print('Wpisz y i enter jesli tak (enter y if agree).')
    if input() == 'y':
        destPath = 'C:\\Users\\hj61t7\\OneDrive - Aptiv\\Pulpit\\Measurements\\2wayFuseSplice\\Test1'
        noww = datetime.now()
        sTime = noww.strftime('%Y%m%d_%H%M%S')
        wb = openpyxl.Workbook()
        sheet = wb.active
        sheet.title = 'testtest1'

        for ind1, i in enumerate(data_lin):
            for ind2, j in enumerate(i):
                cellRef = sheet.cell(row = ind2 + 1, column = ind1 + 1)
                cellRef.value = j
        wb.save(destPath + '\\' + '2waySplicefuseTest_' + sTime + '.xlsx')
    return data_lin

""" measPath = 
    measFile = open(sys.argv[1]) """
# 23.09.2021 - update for test with multiple csv files in one folder - added for each loop to make it faster.
filePath = 'C:\\Users\\hj61t7\\OneDrive - Aptiv\\Pulpit\\Measurements\\2wayFuseSplice\\Test1'
for filename in os.listdir(filePath):
    data_lin = fromCsvDataGet(os.path.join(filePath + '\\' + filename))
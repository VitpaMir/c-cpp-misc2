#! python3
# quick writed program which sorted data from Kickstart
# Make one time column and values columns for each measured magnitude

from datetime import datetime
from funPlots import data_from_workbooks1
import openpyxl


measpath = 'C:\\Users\\hj61t7\\OneDrive - Aptiv\\Pulpit\\Pomiary 22.09.2021 Fuse29A\\csv\\defbuffer1_0922_125321.csv'
data = data_from_workbooks1(measpath)

chosen_data = []
for i in range(0, len(data)):
    if (i != 0):
        chosen_data.append(data[i])

wb = openpyxl.load_workbook(measpath)
wb.create_sheet(title='DataForPlot')
sheet = wb['DataForPlot']
destPath = 'C:\\Users\\hj61t7\\OneDrive - Aptiv\\Pulpit\\Pomiary 22.09.2021 Fuse29A\\xlsx'
noww = datetime.now()
sTime = noww.strftime('%Y%m%d_%H%M%S')
for ind1, i in enumerate(chosen_data):
    for ind2, j in enumerate(i):
        cellRef = sheet.cell(row = ind2 + 1, column = ind1 + 1)
        cellRef.value = j

wb.save(destPath + '\\' + 'defbuffer1_' + sTime + '.xlsx')
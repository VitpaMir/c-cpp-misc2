#! python3
# toOneFile.py - open multiple .xlsx files, takes columns
# and put them in new .xlsx file to make plots in one file
# ignore index columns

import openpyxl, os, re
from datetime import datetime

def data_from_workbooks2(folderPath):
    daata = []
    regIndex = re.compile(r'Index')
    for filename in os.listdir(folderPath): 
        filePath = os.path.join(folderPath + '\\' + filename)
        wb = openpyxl.load_workbook(filePath)
        measSheet = wb[wb.sheetnames[0]]
        data = []
        tempListOfvalues = []
        for col in measSheet.iter_cols(values_only=True):
            for value in col:
                if regIndex.match(str(value)):
                    break
                else:
                    tempListOfvalues.append(value)
            if tempListOfvalues:
                data.append(tempListOfvalues)
            tempListOfvalues = []
        daata.append(data)
    destPath = 'C:\\Users\\hj61t7\\OneDrive - Aptiv\\Pulpit\\Measurements\\2wayFuseSplice\\Thermocouple_effect'
    noww = datetime.now()
    sTime = noww.strftime('%Y%m%d_%H%M%S')
    wb = openpyxl.Workbook()
    sheet = wb.active
    sheet.title = 'testtest1'
    ind1 = 0
    ind2 = 0
    for fileCol in daata:
        for i in fileCol:
            for j in i:
                cellRef = sheet.cell(row = ind2 + 1, column = ind1 + 1)
                cellRef.value = j
                ind2 += 1
            ind1 += 1
            ind2 = 0
        wb.save(destPath + '\\' + 'thermocoupleEffectAll' + sTime + '.xlsx')
    return data

folderPath = 'C:\\Users\\hj61t7\\OneDrive - Aptiv\\Pulpit\\Measurements\\2wayFuseSplice\\Thermocouple_effect'
columns = data_from_workbooks2(folderPath)
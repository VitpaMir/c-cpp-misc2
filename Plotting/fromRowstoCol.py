#! Python3
# fromRowstoCol.py - looking for data in rows and makes it in column in new file,
# data_from_wb#Time functions gather data about channel from device,
# channel means one measurement path - data from multimeter magnitude,
# then knowing channels, programs fit name of channel and write it to the array
# array dimensions depend on data from workbook
# finally, from data array new workbook file with time and measured magnitudes is created
# one time vector for each or separately time vectors for each magnitude is created
# in case of using my plot programs later, I recommend to make one vector of time

from typing import NewType
import openpyxl, re

def data_from_wbDiffTime(filePath):
    # creates list of lists with measuring point for one channel
    # in configuration for one point: [time, value, channel]
    wb = openpyxl.load_workbook(filePath)
    measSheet = wb[wb.sheetnames[0]]
    reg = re.compile(r'Channel')
    channels = []
    data = []
    flag = False
    # list of value from columns
    for col in measSheet.iter_cols(values_only=True):
        data.append(list(col))
    # channels reading
    for i in data:
        for j in i:
            if reg.match(str(j)):
                flag = True
            elif flag == True:
                Channel = set([x for x in i if i.count(x) > 1])
        flag = False
    for i in Channel:
        channels.append(i)
    # value data sort, if time is different for each measured value
    data_row = []
    data_all = []
    # regVal = re.compile(r'[-]?(\d+\.)(\d+)?')
    for i in channels:
        regc = re.compile(str(i))
        for row in measSheet.iter_rows(values_only=True):
            for j in row:
                if regc.match(str(j)):
                    data_row.append(list(row))
        data_all.append(data_row)
        data_row = []
    return data_all

def data_from_wb1Time(filePath):
    #different method, if time is same for each measured value
    # time column for each measure point, if time is different, does not matter
    # creates list with list of time, and values depend on what values were measured
    wb = openpyxl.load_workbook(filePath)
    measSheet = wb[wb.sheetnames[0]]
    reg = re.compile(r'Channel')
    channels = []
    data = []
    flag = False
    # list of value from columns
    for col in measSheet.iter_cols(values_only=True):
        data.append(list(col))
    # channels reading
    for i in data:
        for j in i:
            if reg.match(str(j)):
                flag = True
            elif flag == True:
                Channel = set([x for x in i if i.count(x) > 1])
        flag = False
    for i in Channel:
        channels.append(i)
    
    data_timeTemp = []
    data_time = []
    regTime = re.compile(r'Relative Time')
    for i in data:
        for j in i:
            if regTime.match(str(j)):
                flag = True
            elif flag == True:
                data_timeTemp.append(j)
        flag = False
    for i in range(0, len(data_timeTemp), len(channels)):
        data_time.append(data_timeTemp[i])
    # Reading values from sensors
    data_ValTemp = []
    data_Val1 = []
    data_Val = []
    regVal = re.compile(r'Reading')
    for i in data:
        for j in i:
            if regVal.match(str(j)):
                flag = True
            elif flag == True:
                data_ValTemp.append(j)
        flag = False
    for i in range(len(channels)):
        for j in range(i, len(data_ValTemp), len(channels)):
            data_Val1.append(data_ValTemp[j])
        data_Val.append(data_Val1)
        data_Val1 = []
    data_all = []
    data_all.append(data_time)
    for i in data_Val:
        data_all.append(i)
    return data_all

def saveToXlsx_DiffTime(data):
    # saves data list in .xlxs
    # intended for different time values for measuring point
    # in configuration [time, value, channel]
    reg = re.compile(r'[-]?(\d+\.)(\d+)?')
    newWb = openpyxl.Workbook()
    sheet = newWb.active
    sheet.title = 'measPUR_inCol200A'

    for i in range(len(data)):
        for j in range(len(data[i])):
            for k in range(len(data[i][j])):
                if reg.match(str(data[i][j][k])):
                    cellref = sheet.cell(row = j + 1, column = 1 + 2*i + k)
                    cellref.value = data[i][j][k]
    #newWb.save(destPath + '\\ ' + 'measPUR_inCol200A_test.xlsx')

def saveToXlsx_1Time(data):
    # saves data list in .xlxs
    # intended for one time vector for each measured magnitude
    # values, data is in columns
    newWb = openpyxl.Workbook()
    sheet = newWb.active
    sheet.title = 'measPUR1_inCol200A'

    for ind1, i in enumerate(data):
        for ind2, j in enumerate(i):
            cellref = sheet.cell(row = ind2 + 1, column = ind1 + 1)
            cellref.value = j
    newWb.save(destPath + '\\ ' + 'measPUR_inCol200A_test1.xlsx')

destPath = 'C:\\Users\\hj61t7\\OneDrive - Aptiv\\Pulpit\\Pur_termo_22.07.2021'
measPath1 = 'C:\\Users\\hj61t7\\OneDrive - Aptiv\\Pulpit\\Pur_termo_22.07.2021\\First_mes.xlsx'

data = data_from_wb1Time(measPath1)
print(data)
saveToXlsx_1Time(data)

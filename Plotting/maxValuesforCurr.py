#! python3
# maxValuesforcurr.py - return max values from .xlsx files with measurements
# for elaboration fuse splice test results 
import re, openpyxl

filePath = 'C:\\Users\\hj61t7\\OneDrive - Aptiv\\Pulpit\\Measurements\\3wayFuseSplice\\Test2\\Sekw\\fuseTest_20210827_083048.xlsx'
wb = openpyxl.load_workbook(filePath)
measSheet = wb[wb.sheetnames[0]]

data = []
tempListOfvalues = []
for col in measSheet.iter_cols(values_only=True):
    for value in col:
        tempListOfvalues.append(value)
    data.append(tempListOfvalues)
    tempListOfvalues = []

reg = re.compile(r'[-]?(\d+\.)(\d+)?')
data_val = []
tempListOfvalues = []
for col in measSheet.iter_cols(values_only=True):
    for value in col:
        if reg.match(str(value)):
            tempListOfvalues.append(float(value))
    data_val.append(tempListOfvalues)
    tempListOfvalues = []

regI = re.compile(r'I_shunt')
for i in data:
    for j in i:
        if regI.match(str(j)):
            ind1 = data.index(i)
print(ind1)
Curr = [10, 16, 22, 24, 27, 29, 31, 33, 35, 37]
maxes = []
toMax = []
for c in range(len(Curr)):
    for k in data_val:
        toMax = []
        for i in range(0, len(k)):
            if c != len(Curr) - 1:
                if data_val[ind1][i] >= Curr[c]*1e-4 and data_val[ind1][i] < Curr[c + 1]*1e-4:
                    toMax.append(k[i])
            else:
                if data_val[ind1][i] >= Curr[c]*1e-4:
                    toMax.append(k[i])
        if len(toMax) != 0:
            maxes.append(max(toMax))
print(maxes)
print(len(maxes))
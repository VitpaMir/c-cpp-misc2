#include <stdio.h>
#include <ctype.h>
#include <string.h>
#define SIZE 50
#define REC 10

typedef struct
{
    int rollNumber;
    char name[SIZE];
    char branch[SIZE];
    char dob[SIZE];
    int semister;
}STUDENT_t;

STUDENT_t records[REC] = {{0}};

void menu();
int get_ans();
void display(STUDENT_t * rec);
void add(STUDENT_t * rec);
void delete(STUDENT_t * rec);
void stars();
int i_check(int num);

int main()
{
    int ans = 0;

    menu();
    while ((ans = get_ans()) != '0')
    {
        switch (ans)
        {
        case '1':
            display(records);
            break;
        case '2':
            add(records);
            break;
        case '3':
            delete(records);
            break;
        default:
            printf("Wrong input, type 1, 2, 3 or 0.\n");
            break;
        }
        menu();
    }

    return 0;
}

void menu()
{
    printf("Student record management program\n");
    printf("1) Display all records");
    printf("    2) Add new record\n");
    printf("3) Delete record");
    printf("    0) Exit application\n");
    printf("Enter your option here: ");
}
int get_ans()
{
    int ans;
    ans = getchar();
    while (getchar() != '\n')
        continue;
    return ans;
}

void display(STUDENT_t * rec)
{
    int i = 0, j = 0;
    int cnt = 0;
    int indexes[REC];

    while (1)
    {
        if (rec[i].rollNumber != 0)
        {
            indexes[j] = i;
            j++;
        }
        i++;
        if (i == REC)
            break;
    }
    if (j == 0)
    {
        printf("No records to display.\n");
        return;
    }
    
    printf("Displaying all student records\n");
    for (i = 0; i < j; i++)
    {
        stars();
        printf("rollNumber      : %d\n", rec[indexes[i]].rollNumber);
        printf("studentSemister : %d\n", rec[indexes[i]].semister);
        printf("studentDOB      : %s\n", rec[indexes[i]].dob);
        printf("studentBranch   : %s\n", rec[indexes[i]].branch);
        printf("studentName     : %s\n", rec[indexes[i]].name);
    }
}

void add(STUDENT_t * rec)
{
    int i = 0;
    int index = 0;
    int rollNum;
    char str[12];

    while (1)
    {
        if (rec[i].rollNumber == 0)
        {
            index = i;
            break;
        }
        if (i == REC)
        {
            printf("No place to add new student\n");
            return;
        }
        i++;
    }

    printf("Add new record\n");
    printf("Enter rollNumber        : ");
    if (scanf("%d", &rollNum) != 1)
    {
        printf("Wrong input, try to add record again.\n");
        while (getchar() != '\n')
            continue;
        return;
    }
    if (rollNum <= 0)
    {
        printf("Wrong input, try to add record again.\n");
        printf("RollNumber cannot be 0 or less than zero.\n");
        while (getchar() != '\n')
            continue;
        return;
    }
    if (i_check(rollNum) == 1)
    {
        printf("Wrong input, try to add record again.\n");
        while (getchar() != '\n')
            continue;
        return;
    }

    for (i = 0; i < REC; i++)
    {
        if (rollNum == rec[i].rollNumber)
        {
            while (getchar() != '\n')
                continue;
            printf("Wrong input, this rollNumber already exists!\n");
            printf("Back to manu and try again.\n");
            return;
        }
    }
    rec[index].rollNumber = rollNum;
    printf("Enter student semister  : ");
    scanf("%d", &rec[index].semister);
    printf("Enter DOB (mm/dd/yyyy)  : ");
    getchar();
    gets(rec[index].dob);
    printf("Enter studentBranch     : ");
    gets(rec[index].branch);
    printf("Enter studentName       : ");
    gets(rec[index].name);

    if (rec[index].rollNumber == 0 || rec[index].semister == 0 || rec[index].dob[0] == '\0' || rec[index].branch[0] == '\0' || rec[index].name[0] == '\0')
    {
        rec[index].rollNumber = 0;
        rec[index].semister = 0;
        rec[index].dob[0] = '\0';
        rec[index].branch[0] = '\0';
        rec[index].name[0] = '\0';
        printf("Wrong input, try to add record again.\n");
    }
}

void delete(STUDENT_t * rec)
{
    int i = 0, j;
    int cnt = 0;
    int delNum;
    int flag = 0;

    while (1)
    {
        if (rec[i].rollNumber != 0)
            cnt++;
        i++;
        if (i == REC)
            break;
    }
    if (cnt == 0)
    {
        printf("No records to delete.\n");
        return;
    }

    printf("Enter rollNumber of student: ");
    scanf("%d", &delNum);

    for (i = 0; i < REC; i++)
    {
        if (delNum == rec[i].rollNumber)
        {

            rec[i].rollNumber = 0;
            rec[i].semister = 0;
            rec[i].dob[0] = '\0';
            rec[i].branch[0] = '\0';
            rec[i].name[0] = '\0';
            flag = 1;
            printf("Deleting complete. %d student deleted.\n", delNum);
            while (getchar() != '\n')
                continue;
            return;
        }
    }
    if (flag == 0)
    {
        while (getchar() != '\n')
                continue;
        printf("RollNumber incorrect! This student does not exist.\n");
    }
}

void stars()
{
    int i;

    for (i = 0; i < 20; i++)
        printf("*");
    printf("\n"); 
}

int i_check(int num)
{
    int i = 0;
    char str[40];
    while (getchar() != '\n')
        return 1;

    return 0;
}
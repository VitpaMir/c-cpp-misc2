#include <stdio.h>
#include <stdlib.h>
double min (double x, double y);

int main()
{
    printf("Mniejsza to %.2f.\n", min(-1.8, -16.9));
    return 0;
}

double min (double x, double y)
{
    double mniejsza;
    mniejsza = x < y ? x : y;

    return mniejsza;
}

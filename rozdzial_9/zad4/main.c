#include <stdio.h>
#include <stdlib.h>
double harmonia (double a, double b);

int main()
{
    double x;
    double y;
    printf("Podaj dwie liczby typu double.\n");
    while (scanf("%lf %lf", &x, &y) == 2)
    {
        printf("Srednia harmoniczna liczb %.2f i %.2f to: %.2f.\n", x, y, harmonia(x, y));
        printf("Podaj jeszcze a nie to zakoncz jakos sensownie.\nQ nacisnij czy tam cos.\n");
    }

    return 0;
}

double harmonia (double a, double b)
{
    double suma;
    double harm;

    a = 1/a;
    b = 1/b;

    suma = (a + b)/2;
    harm = 1/suma;

    return harm;
}

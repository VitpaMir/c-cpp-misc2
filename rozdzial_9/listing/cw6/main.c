#include <stdio.h>
#include <stdlib.h>
int zmien (int *x, int *y);

int main()
{
    int a = 10;
    int b = 5;
    zmien(&a, &b);
    printf("%d %d.\n", a, b);
    return 0;
}

int zmien (int *x, int *y)
{
    int temp;
    temp = *x;
    *x = *x + *y;
    *y = temp - *y;
    return *x;
    return *y;
}

#include <stdio.h>
#include <stdlib.h>
// pokazuje jak dzialaja wskazniki, komunikacja miedzy funkcjami
void zamiana (int * u, int * v); // jesli wysylamy adresy nalezy zmienne zadeklarowac
// jako wskazniki do zmienych ktorych adresy beda przesylane

int main()
{
    int x = 5, y = 10;

    printf("Poczatkowo x = %d, a y = %d.\n", x, y);
    zamiana(&x, &y); // wysyla adresy do funkcji zamieniajacej wartosc x na y i y na x
    printf("A teraz x = %d, a y = %d.\n", x, y);
    return 0;
}

void zamiana(int * u, int * v)
{
    int temp;

    temp = *u;  // temp otrzymuje wartosc na ktora wskazuje u, nie mozna napisac
    *u = *v;    // temp = u; bo wtedy temp przypisano by adres zmiennej x a nie wartosc
    *v = temp;  //
}
// przekazujac adresy do funkcji zamiana dalismy dostep do tych zmiennych by je
// zamienic, ale nadal potrzebna jest oczywiscie zmienna pomocnicza temp

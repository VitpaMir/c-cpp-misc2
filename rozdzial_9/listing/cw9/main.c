#include <stdio.h>
#include <stdlib.h>
#define ZAKONCZ 4
void menu ();
int pobieram(int dolna, int gorna);

int main()
{
    int ch;

    menu();
    while ((ch = pobieram(1, 4)) != ZAKONCZ)
    {
        switch (ch)
        {
        case 1 :    printf("Kopiuje.\n");
                    break;
        case 2 :    printf("Przenosze.\n");
                    break;
        case 3 :    printf("Usuwam.\n");
                    break;
        default :   printf("Nie rozumiem.\n");
                    break;
        }
    }
    return 0;
}

void menu() //tylko w sumie wyswietla menu
{
    printf("1) kopiowanie plikow            2) przenoszenie plikow\n");
    printf("3) usuwanie plikow              4) koniec             \n");
    printf("Podaj numer wybranej opcji:\n");
}

int pobieram (int dolna, int gorna) //pobiera granice i zwraca wybrana wartosc menu
{
    int liczba;
    int stan;
    while (((stan = scanf("%d", &liczba)) != 1) || (liczba < dolna || liczba > gorna))
    {
        if (stan != 1)
            scanf("%*s");
        printf("Podaj liczbe z przedzialu 1 do 4.\n");
        menu();
    }
    return liczba;
}

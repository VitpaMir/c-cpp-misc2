#include <stdio.h>
#include <stdlib.h>
double potega(double a, int b);

int main()
{
    double x, xpot;
    int n;

    printf("Podaj liczbe oraz potege naturalna,");
    printf(" do ktorej podniesiona\nzostanie liczba. Wpisz q, aby zakonczyc program.\n");
    while (scanf("%lf%d", &x, &n) == 2)
    {
        xpot = potega (x, n);
        printf("%.3g do potegi %d to %.5g\n", x, n, xpot);
        printf("Podaj kolejna pare liczb lub wpisz q, aby zakonczyc.\n");
    }
    printf("zycze milego dnia!\n");
    return 0;
}

double potega (double a, int b)
{
    double pot = 1;
    int i;
    if (a == 0)
        return 0;
    if (b == 0)
        return 1;
    for (i = 1; i <= b; i++)
        pot *= a;   //pot = pot*a;
    if (b < 0)
    {
        b = -b;
        for (i = 1; i <= b; i++)
            pot *= 1.0/a;
    }
    return pot;
}

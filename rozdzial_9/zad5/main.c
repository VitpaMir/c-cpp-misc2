#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
int numer_alfabet (char ch);

int main()
{
    char ch;
    int numer;

    while ((ch = getchar()) != EOF)
    {
        numer = numer_alfabet(ch);
        printf("%c : %d\n", ch, numer);
    }
    return 0;
}

int numer_alfabet (char ch)
{
    int i;
    int liczba = 0;
    if (isalpha(ch))
    {
        if (isupper(ch))
        ch = tolower(ch);
        for (i = 'a'; i < 'z'; i++)
        {
            if (i == ch)
                liczba = i - 96;
        }
    return liczba;
    }
    else
        return -1;
}

#include <stdio.h>
#include <stdlib.h>
#define BLAD "Prosze wpisac od 2 do 10"
void do_binar (int n, int m);

int main()
{
    int liczba;
    int podstawa;

    printf("Podaj liczbe calkowita i liczbe od 2 do 10 po spacji"
           " (litera konczy program):\n");
    while (scanf("%d%d", &liczba, &podstawa) == 2)
    {
        printf("Odpowiednik w systemie %d: ", podstawa);
        do_binar(liczba, podstawa);
        putchar('\n');
        printf("Podaj liczbe calkowita (litera konczy program):\n");
    }
    return 0;
}

void do_binar (int n, int m)
{
    int r;
    if (m == 1)
    {
        printf("%s.\n", BLAD);
        return;
    }
    r = n % m;
    if (n >= m)
        do_binar(n / m, m);
    putchar('0' + r);
    return;
}

#include <stdio.h>
#include <stdlib.h>
void f1 (char ch, int a, int b);

int main()
{
    char znak;
    int x = 0;
    int y = 0;

    printf("podaj znak i liczbe ile razy ten znak chcesz wyswietlic,\n");
    printf("a nastepnie ile wierszy chcesz wyswietlic.\n");
    printf("Aby zakonczyc wkliknij end of file.\n");
    while ((znak = getchar()) != EOF)
    {
        scanf("%d%d", &x, &y);
        f1(znak, x, y);
        printf("Wpisz ponownie, aby zakonczyc wkliknij eof.\n");
        while (getchar() != '\n')
            continue;
    }
    //f1('*', 3, 9);
    return 0;
}

void f1 (char ch, int a, int b)
{
    int i, j;
    for (i = 0; i < b; i++)
    {
        for (j = 0; j < a; j++)
            putchar(ch);
        putchar('\n');
    }
    return;
}

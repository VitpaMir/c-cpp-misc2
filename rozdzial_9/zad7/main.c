#include <stdio.h>
#include <stdlib.h>
double potega(double a, int b);

int main()
{
    double x, xpot;
    int n;

    printf("Podaj liczbe oraz potege naturalna,");
    printf(" do ktorej podniesiona\nzostanie liczba. Wpisz q, aby zakonczyc program.\n");
    while (scanf("%lf%d", &x, &n) == 2)
    {
        xpot = potega (x, n);
        printf("%.3g do potegi %d to %.5g\n", x, n, xpot);
        printf("Podaj kolejna pare liczb lub wpisz q, aby zakonczyc.\n");
    }
    printf("zycze milego dnia!\n");
    return 0;
}

double potega (double a, int b)
{
    double pot = 1;
    int i = 0;

    if (a == 0)
        return 0;
    if (b == 0)
        return 1;
    if (b > 0)
        pot = potega(a, b - 1);
    pot *= a;
    return pot;
}

#include <stdio.h>
int bityOn(int liczba);
char * dobinar(int n, char * w1);

int main()
{
    int numer;
    printf("Podaj liczbe, policzymi ile wlacza bitow: ");
    scanf("%d", &numer);
    printf("\nLiczba wlaczonych bitow: %d\n", bityOn(numer));

    return 0;
}

int bityOn(int liczba)
{
    int i, licznik;
    char lancuch[8 * sizeof(int)];
    dobinar(liczba, lancuch);
    
    for (i = 0, licznik = 0; i < 8 * sizeof(int); i++)
    {
        if (lancuch[i] == '1')
            licznik++;
    }
    return licznik;
}

char * dobinar(int n, char * w1)
{
    int i;

    static int rozmiar = 8 * sizeof(int);

    for (i = rozmiar - 1; i >= 0; i--, n >>= 1)
        w1[i] = (01 & n) + '0';
    w1[rozmiar] = '\0';
    return w1;
}
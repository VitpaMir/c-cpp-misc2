#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int toDecimal(char * wbin);
int potega2(int num);

int main()
{
    char wbin[30];
    printf("Podaj swoj lancuch binarny:\n");
    scanf("%s", wbin);
    printf("%s to tak naprawde: %d.\n", wbin, toDecimal(wbin));

    return 0;
}

int toDecimal(char * wbin)
{
    int i, liczba;
    int rozmiar = strlen(wbin);
    int c;
    
    for (i = rozmiar - 1, liczba = 0; i >= 0; i--)
    {
        c = (int) wbin[i] - 48;
        liczba += c * potega2(rozmiar - i);
    }
    return liczba;
}
int potega2(int num)
{
    int i;
    int res = 1;

    for (i = 1; i < num; i++)
        res *= 2;
    return res;
}
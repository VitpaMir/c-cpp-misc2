#include <stdio.h>
int lookAtBit(int wartosc, int bit);

int main()
{
    int value;
    int bit;
    int stan;
    printf("Podaj wartosc: ");
    scanf("%d", &value);
    printf("Podaj bit, ktorego stan chcesz sprawdzic: ");
    scanf("%d", &bit);
    stan = lookAtBit(value, bit);
    printf("%c to stan tegoz bitu.\n", stan);

    return 0;
}

int lookAtBit(int wartosc, int bit)
{
    int i;
    int rozmiar = 8 * sizeof(int);
    int result;
    wartosc >>= bit;
    result = (01 & wartosc) + '0';

    return result;
}
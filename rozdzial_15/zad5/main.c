#include <stdio.h>
unsigned int obroc_l(unsigned int a, int shift);
char * dobinar(int n, char * w1);

int main()
{
    unsigned int value;
    int bit;
    int stan;
    char lancuch4[8 * sizeof(int)];
    printf("Podaj wartosc: ");
    scanf("%d", &value);
    printf("Podaj bit, od ktorego chcesz obrocic: ");
    scanf("%d", &bit);
    printf("Lancuch binarny dany: %s\n", dobinar(value, lancuch4));
    printf("%d\n", obroc_l(value, bit));

    return 0;
}
// do poprawki, trzeba ogarnac jak wytworzyc maske zeby tylko znaczace bity byly brane pod uwage i wtedy uzyskac z liczby te bity ktore sa znaczace bez tych ktore sa poza zakresem
unsigned int obroc_l(unsigned int a, int shift)
{
    int rozmiar = 8 * sizeof(int);
    unsigned int b;
    char lancuch1[8 * sizeof(int)];
    char lancuch2[8 * sizeof(int)];
    char lancuch3[8 * sizeof(int)];

    b = a >> (32 - shift);
    dobinar(b, lancuch1);
    printf("%s\n", lancuch1);
    a <<= shift;
    dobinar(a, lancuch2);
    printf("%s\n", lancuch2);
    a |= b;
    dobinar(a, lancuch3);
    printf("%s\n", lancuch3);

    return a;
}

char * dobinar(int n, char * w1)
{
    int i;

    static int rozmiar = 8 * sizeof(int);

    for (i = rozmiar - 1; i >= 0; i--, n >>= 1)
        w1[i] = (01 & n) + '0';
    w1[rozmiar] = '\0';
    return w1;
}
#include <stdio.h>
#include <string.h>

#define MASK 0xff

int toDecimal(char * wbin);
int potega2(int num);
char * dobinar(int n, char * w1);

int main(int argc, char *argv[])
{
    char bin_lan[8 * sizeof(int)];

    if (argc < 3)
        printf("Nie podano wystarczajacej liczby argumentow.\n");
    else
    {
        int lan1 = toDecimal(argv[1]);
        int lan2 = toDecimal(argv[2]);
        /* const int lan1 = 199;
        const int lan2 = 117; */
        char lan1Neg[8 * sizeof(int)];
        char lan2Neg[8 * sizeof(int)];
        char orLan[8 * sizeof(int)];
        char andLan[8 * sizeof(int)];
        char xorLan[8 * sizeof(int)];
        printf("Pierwszy lancuch: %s\n", argv[1]);
        printf("Drugi  lancuch: %s\n", argv[2]);
        

        printf("Pierwszy  lancuch dziesietnie: %d\n", lan1);
        printf("Drugi  lancuch dziesietnie: %d\n", lan2);

        printf("Negacja pierwszego lancucha: %s.\n", dobinar(~lan1 & MASK, lan1Neg));
        printf("%d\n", lan1);
        printf("Negacja drugiego lancucha: %s.\n", dobinar(~lan2 & MASK, lan2Neg));
        printf("%d\n", lan2);
        printf("Operacja OR: %s\n", dobinar(lan1 | lan2, orLan));
        printf("Operacja AND: %s\n", dobinar(lan1 & lan2, andLan));
        printf("Operacja XOR: %s\n", dobinar(lan1 ^ lan2, xorLan));
    }
    return 0;
}

int toDecimal(char * wbin)
{
    int i, liczba;
    int rozmiar = strlen(wbin);
    int c;
    
    for (i = rozmiar - 1, liczba = 0; i >= 0; i--)
    {
        c = (int) wbin[i] - 48;
        liczba += c * potega2(rozmiar - i);
    }
    return liczba;
}
int potega2(int num)
{
    int i;
    int res = 1;

    for (i = 1; i < num; i++)
        res *= 2;
    return res;
}

char * dobinar(int n, char * w1)
{
    int i;

    static int rozmiar = 8 * sizeof(int);

    for (i = rozmiar - 1; i >= 0; i--, n >>= 1)
        w1[i] = (01 & n) + '0';
    w1[rozmiar] = '\0';
    return w1;
}
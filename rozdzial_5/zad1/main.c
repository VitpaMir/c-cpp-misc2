#include <stdio.h>
#include <stdlib.h>
#define STALA 60
int main()
{
    int minuty;
    int godziny;
    int reszta;
    printf("Podaj czas w minutach, przeliczymy na godziny i minuty:\n");
    printf("Aby zakonczyc wpisz zero.\n");
    scanf("%d", &minuty);
    while (minuty != 0)
    {
        godziny = minuty/STALA;
        reszta = minuty%STALA;
        printf("%d minut to %d godzin i %d minut.\n", minuty, godziny, reszta);
        printf("Podaj kolejna wartosc, jesli nie to zakoncz wpisujac zero.\n");
        scanf("%d", &minuty);
    }
    return 0;
}

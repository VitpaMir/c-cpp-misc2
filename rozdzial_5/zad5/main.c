#include <stdio.h>
#include <stdlib.h>

int main()
{
    int licznik, suma, liczba;
    licznik = 0;
    suma = 0;
    scanf("%d", &liczba);
    while (licznik++ < liczba)
        suma = suma + licznik*licznik;
    printf("suma = %d\n", suma);
    return 0;
}

#include <stdio.h>
#include <stdlib.h>

int main()
{
    /* 1. Klasy tworzace zmienne lokalne dla zawierajacej je funkcji to klasa:
        - auto (standardowa deklaracja), statyczna (tylko czas trwania przez caly plik)
        2. Klasy tworzace zmienne, ktore istnieja przez caly czas dzialania programu:
        - static, zewn, static zewn (extern),
        3. Klasy tworzace zmienne ktore moga byc wykorzystywane przez kilka plikow:
        - extern (zewn)
        4. Klasy tworzace zmienne ktore moga byc wykorzystywane w ramach jednego pliku:
        - static jako zewn
        5. Program musi kolejno przejsc przez cala tablice za kazdy razem
            Kazda zmienna ulega przesunieciu za kazdym cyklem petli, do tego
            zostaja przesuniete na odlegle miejsca od swojego przeznaczonego czasami
        6. zmienic linijke w if (znak < zamiast >)
        7. trzeba by dac %s zamiast %d bo to lancuchy, nalezy sprawdzic poprawnosc wpisania
            liczby szestanstkowej jesli mialy by byc przyjmowane liczby 16 (%x)
        8. Stokrotka jest znana w obu plikach, lilia w main i platek, roza w pliku 2, ale
            tylko w korzeniu roza jest tez auto w mainie i bedzie miala swoja wlasna roze,
        9. W pierwszej jest odrebna zmienna kolor, natomiast w main jest jako B potem po
            funkcji druga jako znak G
        10. a) plink bedzie widoczny tylko w ramach tego pliku a prototyp funkcji wskzuje
            ze pobiera ona tablice, jakies wartosci wartosc i n i zostanie uzyta w tym pliku
            b) ogolnie beda to wartosci o zasiegu prototypowym w definicji moga sie inaczej
                nazywac wiec nic to nie da
    */
    return 0;
}

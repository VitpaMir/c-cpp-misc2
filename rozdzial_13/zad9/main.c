/* Zadanie 9 i 10 (modyfikacja z minusami)
Wykrywa minus przed pierwsza liczba, pozniejsze ignoruje */
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#define MAX 40
int get_cypher(int *liczba, FILE *wp);

int main()
{
    FILE *wp;
    char tab[MAX];
    static int liczba = 0;
    if ((wp = fopen("tekstowy.txt", "r")) == 0)
    {
        fprintf(stderr, "Nie udalo sie otworzyc pliku.\n");
        exit(EXIT_FAILURE);
    }
    while (get_cypher(&liczba, wp) == 1);
    printf("Uzyskana liczba:\n");
    printf("%d\n", liczba);
    if (fclose(wp) != 0)
    {
        fprintf(stderr, "Nie udalo sie poprawnie zamknac.\n");
        exit(EXIT_FAILURE);
    }
    return 0;
}

int get_cypher(int *liczba, FILE *wp)
{
    static int i = 0;
    char ch;
    char znak1 = ' ';
    static char tab_cyf[MAX];
    static int ujemna = 1;
    static int znak = 1;
    static int w_liczbie = 0;
    while (ch = getc(wp))
    {
        if (isdigit(ch))
        {
            if (znak1 == '-')
            {
                znak = -1;
                ujemna = 0;
            }
            tab_cyf[i] = ch;
            i++;
            w_liczbie = 1;
            return 1;
        }
        else if (ch == '-' && ujemna == 1 && w_liczbie == 0)
            znak1 = ch;
        else if (ch == EOF)
        {   
            *liczba = atoi(tab_cyf);
            *liczba *= znak;
            return EOF;
        }
        else
        {
            ungetc(ch, stdin);
            znak1 = ' ';
        }
            
    }
}
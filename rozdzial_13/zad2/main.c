#include <stdio.h>
#define MAX_ROZM 5
extern int pobierz(float tab[], int n);
extern void sort(float tab[], int n);
extern void wyswietl(const float tab[], int n);

int main()
{
    float liczby[MAX_ROZM];
    int rozmiar;

    rozmiar = pobierz(liczby, MAX_ROZM);
    printf("\nPierwotne dane (%d wartosci):\n", rozmiar);
    wyswietl(liczby, rozmiar);
    sort(liczby, rozmiar);
    puts("Posortowane dane: ");
    wyswietl(liczby, rozmiar);
    return 0;
}

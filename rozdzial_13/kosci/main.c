#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>
#include "kosci.c"
extern int rzut (int);

int main()
{
    int kosci, licznik, wynik, kolejki;
    int sciany, licznik1;

    srand((unsigned int) time(0));
    printf("Podaj liczbe kolejek, wpisz q, aby zakonczyc.\n");
    while (scanf("%d", &kolejki) == 1)
    {
        printf("Ile scian i kosci?\n");
        if (scanf("%d %d", &sciany, &kosci) != 2)
        {
            printf("Podano nieprawidlowe wartosci. Podaj jeszcze raz sciany i kosci\n");
            scanf("%*s %*s");
            if (scanf("%d %d", &sciany, &kosci) != 2)
            {
                printf("Podaj liczbe kolejek, wpisz q, aby zakonczyc.\n");
                continue;
            }
        }
        printf("Oto %d kolejek rzutow %d %d-sciennymi kostkami:\n", kolejki, kosci, sciany);
        for (licznik1 = 0; licznik1 < kolejki; licznik1++)
        {
            for (wynik = 0, licznik = 0; licznik < kosci; licznik++)
            {
                wynik += rzut(sciany);
            }
            printf("%d ", wynik);
        }
        printf("\nPodaj liczbe kolejek, wpisz q, aby zakonczyc.\n");
    }
    printf("Zycze duzo szczescia!\n");
    return 0;
}
//dopisac if - jezeli dane sa nieliczbowe to

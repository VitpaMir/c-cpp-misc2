#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#define MAX 20
#define ROZMIAR 40
#define ROZMIAR1 500
/* #define TAK 1
#define NIE 0
void pobierz_slowo (FILE *fp, char slowo[]); */
void strlan(char *lancuchy[], int num);

int main()
{
    FILE *fp;
    int i, j, licz = 0; //liczy wpisane slowa
    char odpowiedz;
    char znak;
    char *wsklan[MAX];
    char slowa[MAX][ROZMIAR];
    char nazwa[ROZMIAR];
    printf("Prosze podac do dwudziestu slow.\n");
    i = 0;
    while(licz < MAX && gets(slowa[licz]) && slowa[licz][0] != '\0')
    {
        wsklan[licz] = slowa[licz];
        licz++;
    }
    strlan(wsklan, licz);
    printf("Wpisano nastepujace slowa (w kolejnosci alfabetycznej):\n");
    for(i = 0; i < licz; i++)
        puts(slowa[i]);
    printf("Czy zapisac slowa w pliku? Wpisz t jesli tak lub jakas inna litere jesli nie.\n");
    while((odpowiedz = getchar()) != 'n')
    {
        if(odpowiedz == 't')
        {
            while (getchar() != '\n')
                continue;
            printf("Wpisales t, wiec podaj ponizej nazwe pliku do, ktorego chcesz dokonac zapisu slow:\n");
            gets(nazwa);
            if((fp = fopen(nazwa, "a+")) == NULL)
               fprintf(stderr, "Nie udalo sie poprawnie otworzyc pliku.\n");
            for(i = 0; i < licz; i++)
            {
                fputs(slowa[i], fp);
                fputs("\n", fp);
            }
            if(fclose(fp) != 0)
                fprintf(stderr, "Nie udalo sie poprawnie zapisac.\n");
            break;
        }
        else
            printf("Wpisz t lub n.\n");
        while (getchar() != '\n')
            continue;
    }
    printf("Koniec programu.\n");
    return 0;
}
// potrzebna funkcja prawdopodobnie jesli chcemy brac slowa ze stringow (z tablic), normalnie wpisujemy pojedyncze slowa naciskajac enter
/* void pobierz_slowo (FILE *fp, char slowo[]) 
{
    char ch;
    int i = 0;
    int w_slowie = NIE;
    while (!feof(fp))
    {
        ch = getc(fp);
        if (isalpha(ch) && w_slowie == NIE)
            w_slowie = TAK;
        if (w_slowie == TAK)
            slowo[i] = ch;
        if (isspace(ch) && w_slowie == TAK)
            break;
        i++;
    }
    ungetc(' ', fp);
    slowo[i] = '\0';
} */

void strlan(char *lancuchy[], int num)
{
    char *temp;
    int dol, szuk;

    for(dol = 0; dol < num - 1; dol++)
    {
        for(szuk = dol + 1; szuk < num; szuk++)
        {
            if(strcmp(lancuchy[dol], lancuchy[szuk]) > 0)
            {
                temp = lancuchy[dol];
                lancuchy[dol] = lancuchy[szuk];
                lancuchy[szuk] = temp;
            }
        }
    }
}
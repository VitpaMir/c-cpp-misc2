#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#define MAX 10
#define LINIA 50
int get_cypher_mod(int *liczba, FILE *wp);
static char ch;

int main()
{
    FILE *wp;
    static int liczba = 0;
    int suma = 0;
    int stan = 0;
    float srednia[MAX];
    int i = 0;
    int licznik, nast;
    int tempi;
    char *tempc;
    char bufor[MAX][LINIA];
    char *wsklan[MAX];
    if ((wp = fopen("tekstowy.txt", "r")) == 0)
    {
        fprintf(stderr, "Nie udalo sie otworzyc pliku.\n");
        exit(EXIT_FAILURE);
    }
    while(!feof(wp))
    {
        fgets(bufor[i], LINIA, wp);
        wsklan[i] = bufor[i];
        i++;
    }
    i = 0;
    puts("Do kurwy smiecia pierdolonego.");
    rewind(wp);
    while ((stan = get_cypher_mod(&liczba, wp)))
    {
        suma += liczba;
        puts("KURWOWOWOOWOOW");
        if (stan == 2 || stan == EOF)
        {
            srednia[i] = (float) suma / 3.0;
            suma = 0;
            i++;
            if (ch == EOF)
                break;
        }
    }
    puts("No kurwo jebana suko skurwiala.");
    for (i = 0; i < MAX; i++)
        printf("%.2f\n", srednia[i]);
    for (licznik = 0, nast = 0; licznik < MAX - 1; licznik++)
    {
        for(nast = licznik + 1; nast < MAX; nast++)
        {
            printf("W sortowaniu ale przed ifem\n");
            printf("Srednia[nast] %.2f, srednia[i] %.2f.\n", srednia[nast], srednia[licznik]);
            if (srednia[nast] > srednia[licznik])
            {
                tempc = wsklan[nast];
                tempi = srednia[nast];
                wsklan[nast] = wsklan[licznik];
                srednia[nast] = srednia[licznik];
                wsklan[licznik] = tempc;
                srednia[licznik] = tempi;
                puts("Sortuje\n");
            }
        }
    }
    printf("Po forze\n");
    for (i = 0; i < MAX; i++)
    {
        fputs(wsklan[i], stdout);
        printf(". Srednia wynikow: %.2f\n", srednia[i]);
    }
    if (fclose(wp) != 0)
    {
        fprintf(stderr, "Nie udalo sie poprawnie zamknac.\n");
        exit(EXIT_FAILURE);
    }
    return 0;
}

int get_cypher_mod(int *liczba, FILE *wp)
{
    static int i = 0;
    static int j = 0;

    static char tab_cyf[MAX][MAX];
    static int w_liczbie = 0;
    while (ch = getc(wp))
    {
        if (isdigit(ch))
        {
            tab_cyf[j][i] = ch;
            i++;
            w_liczbie = 1;
        }
        else if ((isspace(ch) || ch == EOF) && w_liczbie == 1)
        {
            *liczba = atoi(tab_cyf[j]);
            j++;
            i = 0;
            w_liczbie = 0;
            if (ch == '\n' || ch == EOF)
                return 2;
            return 1;
        }
        else
            ungetc(ch, stdin);
    }
}

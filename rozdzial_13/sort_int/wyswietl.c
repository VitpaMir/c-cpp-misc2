#include <stdio.h>
void wyswietl(const float tablica[], int granica)
{
    int index;

    for (index = 0; index < granica; index++)
    {
        printf("%.2f ", tablica[index]);
        if (index % 10 == 9)
            putchar('\n');
    }
    if (index % 10 != 0)
        putchar('\n');
}

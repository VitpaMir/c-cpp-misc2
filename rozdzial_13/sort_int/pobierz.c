#include <stdio.h>
#include <string.h>
#define NIENUM 0
#define TAKNUM 1
#define KONIEC '#'
//zad2
int pobierz(float tablica[], int granica)
{
    float num, stan;

    int index = 0;

    printf("Ten program konczy odczytywanie liczb po pobraniu %d ", granica);
    printf("wartosci\nlub w przypadku wpisania znaku #. Pierwsza wartosc: ");
    stan = scanf("%f", &num);
    while (index < granica) // standardowo jeszcze bylo && stan != EOF
    {
        if (stan == TAKNUM)
        {
            tablica[index++] = num;
            printf("%.2f przyjete. ", num);
            if (index < granica)
            {
                printf("Nastepna wartosc: ");
                stan = scanf("%f", &num);
            }
        }
        else if (stan == NIENUM)
        {
            if (num != '#') // dodane w zadaniu 1
                break;
            scanf("%*s");
            printf("To nie byla liczba zmiennoprzecinkowa! Wpisz liczbe zmiennoprzecinkowa, aby\n");
            printf("kontynuowac lub cos, co nie jest liczba, aby zakonczyc: ");
            if ((stan = scanf("%f", &num)) == NIENUM)
                break;
        }
        else
        {
            printf("Ups! Program nigdy nie powinien dotrzec w to miejsce!\n");
            break;
        }
    }
    if (index == granica)
        printf("Wszystkie %d elementow tablicy zostalo wypelnione.\n", granica);
    return index;
}

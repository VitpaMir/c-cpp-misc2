void sort(float tablica[], int granica)
{
    int gora, szuk, naj;
    float temp;

    for (gora = 0; gora < granica - 1; gora++)
    {
        for (szuk = gora + 1, naj = gora; szuk < granica; szuk++)
        {
            if (tablica[szuk] < tablica[naj])
                naj = szuk;
        }
        temp = tablica[naj];
        tablica[naj] = tablica[gora];
        tablica[gora] = temp;
        naj = gora + 1;
    }
}

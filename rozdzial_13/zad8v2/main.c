#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#define MAX 20
#define ROZMIAR 40
#define ROZMIAR1 500
#define TAK 1
#define NIE 0

int main()
{
    FILE *fp;
    int i, j, licz = 0; //liczy wpisane slowa
    char odpowiedz;
    char znak;
    char bufor[MAX][ROZMIAR1];
    char temp[ROZMIAR1];
    char slowa[MAX][ROZMIAR];
    char nazwa[ROZMIAR];
    printf("Prosze podac do dwudziestu slow.\n");
    i = 0;
    while(licz < MAX && gets(slowa[licz]) && slowa[licz][0] != '\0')
    {
        licz++;
    }
    // sortowanie
    printf("Wpisano nastepujace slowa (w kolejnosci alfabetycznej):\n");
    for(i = 0; i < licz; i++)
        printf("%s\n", slowa[i]);
    if ((odpowiedz = getchar()) == 't')
    {
        while (getchar() != '\n')
        {
            continue;
        }
        gets(nazwa);
        if((fp = fopen(nazwa, "a+")) == NULL)
            fprintf(stderr, "Nie udalo sie poprawnie otworzyc pliku.\n");
        for(i = 0; i < licz; i++)
        {
            fputs(slowa[i], fp);
            fputs("\n", fp);
        }
        if(fclose(fp) != 0)
            fprintf(stderr, "Nie udalo sie poprawnie zapisac.\n");
    }
    printf("Koniec programu.\n");
    return 0;
}

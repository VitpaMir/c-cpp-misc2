#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#define MAX 10
#define LINIA 50
int get_cypher_mod(int *liczba, FILE *wp);
void usun_od_znak(char lancuch[]);
static char ch;

int main()
{
    FILE *wp;
    static int liczba = 0;
    int suma = 0;
    int stan = 0;
    float srednia[MAX];
    int i = 0;
    int licznik, nast;
    int tempi;
    char *tempc;
    char bufor[MAX][LINIA];
    char *wsklan[MAX];
    if ((wp = fopen("tekstowy.txt", "r")) == 0)
    {
        fprintf(stderr, "Nie udalo sie otworzyc pliku.\n");
        exit(EXIT_FAILURE);
    }
    while(!feof(wp))
    {
        fgets(bufor[i], LINIA, wp);
        wsklan[i] = bufor[i];
        i++;
    }
    for(i = 0; i < MAX; i++)
        usun_od_znak(wsklan[i]);
    i = 0;
    rewind(wp);
    while ((stan = get_cypher_mod(&liczba, wp)))
    {
        suma += liczba;
        if (stan == 2 || stan == EOF)
        {
            srednia[i] = (float) suma / 3.0;
            suma = 0;
            i++;
            if (ch == EOF)
                break;
        }
    }
    for (licznik = 0, nast = 0; licznik < MAX - 1; licznik++)
    {
        for(nast = licznik + 1; nast < MAX; nast++)
        {
            if (srednia[nast] < srednia[licznik])
            {
                tempc = wsklan[nast];
                tempi = srednia[nast];
                wsklan[nast] = wsklan[licznik];
                srednia[nast] = srednia[licznik];
                wsklan[licznik] = tempc;
                srednia[licznik] = tempi;
            }
        }
    }
    for (i = 0; i < MAX; i++)
    {
        fputs(wsklan[i], stdout);
        printf(" Srednia wynikow: %.2f\n", srednia[i]);
    }
    if (fclose(wp) != 0)
    {
        fprintf(stderr, "Nie udalo sie poprawnie zamknac.\n");
        exit(EXIT_FAILURE);
    }
    return 0;
}
/* Wystepuje jeden blad, w pliku tekstowym nie zapisuje jednego wyniku (jednej liczby) nie wiadomo dlaczego, reszta super dziala, dziwne i zmiana liczb nic nie daje */
int get_cypher_mod(int *liczba, FILE *wp)
{
    static int i = 0;
    static int j = 0;

    static char tab_cyf[MAX][MAX];
    static int w_liczbie = 0;
    while (ch = getc(wp))
    {
        if (isdigit(ch))
        {
            tab_cyf[j][i] = ch;
            i++;
            w_liczbie = 1;
        }
        else if ((isspace(ch) || ch == EOF) && w_liczbie == 1)
        {
            *liczba = atoi(tab_cyf[j]);
            j++;
            i = 0;
            w_liczbie = 0;
            if (ch == '\n' || ch == EOF)
                return 2;
            return 1;
        }
        else
            ungetc(ch, stdin);
    }
}

void usun_od_znak(char lancuch[])
{
    char * pos;
    char znak_n = '\n';
    while(*lancuch)
    {
        pos = strchr(lancuch, znak_n);
        if(pos == NULL)
            break;
        *(pos) = '.';
        lancuch++;
    }
}

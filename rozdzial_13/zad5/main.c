#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define MAX 100
void generuj(int tablica[], int rozmiar, unsigned int nast);
void sortuj(int tablica[], int rozmiar);
void wyswietl(int tablica[], int rozmiar);
int main()
{
    int tablica[MAX];
    int rozmiar = MAX;

    generuj(tablica, rozmiar, time(0));
    wyswietl(tablica, rozmiar);
    sortuj(tablica, rozmiar);
    wyswietl(tablica, rozmiar);
}

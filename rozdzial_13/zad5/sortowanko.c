void sortuj(int tablica[], int rozmiar)
{
    int gora, szuk, naj, temp;

    for (gora = 0; gora < rozmiar - 1; gora++)
    {
        for (szuk = gora + 1, naj = gora; szuk < rozmiar; szuk++)
        {
            if (tablica[szuk] > tablica[naj])
                naj = szuk;
        }
        temp = tablica[naj];
        tablica[naj] = tablica[gora];
        tablica[gora] = temp;
        naj = gora + 1;
    }
}

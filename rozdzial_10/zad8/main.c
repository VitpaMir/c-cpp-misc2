#include <stdio.h>
#include <stdlib.h>
#define ROZMIAR1 6
int sumuj2tab (int tab1[], int tab2[], int sumowa[], int rozmiar);

int main()
{
    int i;
    int pierwsza [ROZMIAR1] = {9, 4, 5, 1, 4, 3};
    int druga [ROZMIAR1] = {1, 6, 5, 9, 6, 7};
    int sumaryczna [ROZMIAR1];

    sumaryczna[ROZMIAR1] = sumuj2tab(pierwsza, druga, sumaryczna, ROZMIAR1);

    for (i = 0; i < ROZMIAR1; i++)
        printf("%d ", pierwsza[i]);
    printf("\n");
    for (i = 0; i < ROZMIAR1; i++)
        printf("%d ", druga[i]);
    printf("\n");
    for (i = 0; i < ROZMIAR1; i++)
        printf("%d ", sumaryczna[i]);
    printf("\n");

    return 0;
}

int sumuj2tab (int tab1[], int tab2[], int sumowa[], int rozmiar)
{
    int i;

    for (i = 0; i < rozmiar; i++)
        sumowa[i] = tab1[i] + tab2[i];
    return sumowa[rozmiar];
}

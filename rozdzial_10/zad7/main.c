#include <stdio.h>
#include <stdlib.h>
#define ROZMIAR1 3
#define ROZMIAR2 7
int tablicowa (int tab[], int nowatab[], int rozmiar);

int main()
{
    int i;
    int wykaz[ROZMIAR2] = {11, 12, 10, 15, 16, 30, 31};
    int nowatab[ROZMIAR1];

    nowatab[ROZMIAR1] = tablicowa(&wykaz[2], nowatab, 3);
    for (i = 0; i < ROZMIAR2; i++)
        printf("%d ", wykaz[i]);
    printf("\n");
    for (i = 0; i < ROZMIAR1; i++)
        printf("%d ", nowatab[i]);
    printf("\n");

    return 0;
}

int tablicowa (int tab[], int nowatab[], int rozmiar)
{
    int i;
    for (i = 0; i < rozmiar; i++)
        nowatab[i] = tab [i];
    return nowatab [rozmiar];
}

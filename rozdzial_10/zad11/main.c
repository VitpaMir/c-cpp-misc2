#include <stdio.h>
#include <stdlib.h>
#define ROZMIAR1 3
#define ROZMIAR2 5
int zapis (double tab[][ROZMIAR2], int rozmiar1, int rozmiar2);
void pokaz_tablice (double tablica[][ROZMIAR2], int rozmiar1, int rozmiar2);
double srednia_osobno (double tab[][ROZMIAR2], int rozmiar1, int rozmiar2, double nowatab[ROZMIAR1]);
double srednia_calosc (double tab[][ROZMIAR2], int rozmiar1, int rozmiar2);
double najwiekszy (double tab[][ROZMIAR2], int rozmiar1, int rozmiar2);
void wyswietl_wyniki (double srednia1[], double rozmiar1, double srednia2, double cmax);

int main()
{
    int ilosc;
    double tablica[ROZMIAR1][ROZMIAR2];
    double srednia [ROZMIAR1];
    double csrednia;
    double cmax;
    ilosc = zapis (tablica, ROZMIAR1, ROZMIAR2);
    if (ilosc == 0)
        printf("Brak danych. Czesc.\n");
    else
        pokaz_tablice(tablica, ROZMIAR1, ROZMIAR2);
    srednia[ROZMIAR1] = srednia_osobno(tablica, ROZMIAR1, ROZMIAR2, srednia); //nic nie zwraca :/
    csrednia = srednia_calosc (tablica, ROZMIAR1, ROZMIAR2);
    cmax = najwiekszy (tablica, ROZMIAR1, ROZMIAR2);
    wyswietl_wyniki (srednia, ROZMIAR1, csrednia, cmax);
    return 0;
}

int zapis (double tab[][ROZMIAR2], int rozmiar1, int rozmiar2)
{
    int j = 0;

    printf("Podaj 3 zbiory liczbowe po 5 liczb typu double.\n");
    while (j < rozmiar1*rozmiar2 && scanf("%lf", &tab[0][j]) == 1)
        j++;
    return j;
}

void pokaz_tablice (double tablica[][ROZMIAR2], int rozmiar1, int rozmiar2)
{
    int i, j;

    printf("Wpisano nastepujace liczby:\n");
    for (i = 0; i < rozmiar1; i++)
    {
        for (j = 0; j < rozmiar2; j++)
            printf("%.2lf ", tablica[i][j]);
        printf("\n");
    }
}

double srednia_osobno (double tab[][ROZMIAR2], int rozmiar1, int rozmiar2, double nowatab[ROZMIAR1])
{
    int i, j;
    double sumka;
    for (i = 0; i < rozmiar1; i++)
    {
        for (j = 0, sumka = 0; j < rozmiar2; j++)
            sumka += tab[i][j];
        nowatab[i] = sumka / rozmiar2;
        sumka = 0;
    }
    return nowatab[rozmiar1];
}

double srednia_calosc (double tab[][ROZMIAR2], int rozmiar1, int rozmiar2)
{
    int i, j;
    double sumka, srednie;
    for (i = 0; i < rozmiar1; i++)
    {
        for (j = 0; j < rozmiar2; j++)
            sumka += tab[i][j];
    }
        srednie = sumka / (rozmiar1 * rozmiar2);
    return srednie;
}

double najwiekszy (double tab[][ROZMIAR2], int rozmiar1, int rozmiar2)
{
    int i, j;
    double maks;
    maks = tab[0][0];
    for (i = 0; i < rozmiar1; i++)
    {
        for (j = 0; j < rozmiar2; j++)
        {
            if (tab[i][j] > maks)
                maks = tab [i][j];
        }
    }

    return maks;
}

void wyswietl_wyniki (double srednia1[], double rozmiar1, double srednia2, double cmax)
{
    int i;

    for (i = 0; i < ROZMIAR1; i++)
        printf("Srednia %d rzedu to: %.2lf\n", i + 1, srednia1[i]);
    printf("Srednia calosci to: %.2lf.\n", srednia2);
    printf("Najwieksza wartosc w tej tablicy to: %.2lf.\n", cmax);
}

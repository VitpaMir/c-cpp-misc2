#include <stdio.h>
#include <stdlib.h>
#define ROZMIAR1 3
#define ROZMIAR2 5
void wyswietlanie (int tab[][ROZMIAR2], int rzedy);
void podwawajam (int tab[][ROZMIAR2], int rzedy);

int main()
{
    int tabliczka [ROZMIAR1][ROZMIAR2] = {{2, 4, 6, 8, 10},
                                          {3, 6, 9, 12, 15},
                                          {4, 5, 6, 7, 8}
                                            };

    wyswietlanie(tabliczka, ROZMIAR1);
    podwawajam(tabliczka, ROZMIAR1);
    printf("Podwojona tabliczka:\n");
    wyswietlanie(tabliczka, ROZMIAR1);
    return 0;
}

void wyswietlanie (int tab[][ROZMIAR2], int rzedy)
{
    int i, j;

    for (i = 0; i < rzedy; i++)
    {
        for (j = 0; j < ROZMIAR2; j++)
            printf("%d ", tab[i][j]);
        printf("\n");
    }
}

void podwawajam (int tab[][ROZMIAR2], int rzedy)
{
    int i, j;

    for (i = 0; i < rzedy; i++)
    {
        for (j = 0; j < ROZMIAR2; j++)
            tab[i][j] *= 2;
    }
}

#include <stdio.h>
#include <stdlib.h>
#define ROZMIAR 6
int maxifmam (double tab[], int m);

int main()
{
    double tab[ROZMIAR] = {9.2, 1.3, 10.2, 1.1, 4.3, 3.0};

    printf("Tablica ma element o najwiekszej wartosci"
           " o indeksie %d\n", maxifmam(tab, ROZMIAR));
    return 0;
}

int maxifmam (double tab[], int m)
{
    int i, j;
    double maks;
    maks = tab[0];
    for (i = 0, j = 0; i < m; i++)
    {
        if (tab[i] > maks)
        {
            maks = tab [i];
            j = i;
        }
        else
            continue;
    }
    return j;
}

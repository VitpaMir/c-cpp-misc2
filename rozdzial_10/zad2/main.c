#include <stdio.h>
#include <stdlib.h>
#define ROZMIAR 5
int tablicowa (int tab[], int nowatab[], int rozmiar);
int wskaznikowa (int *tab, int *nowtab, int rozmiar);

int main()
{
    int i;
    int pierwsza[ROZMIAR] = {1, 7, 5, 6, 10};
    int druga[ROZMIAR];
    int trzecia [ROZMIAR];

    druga [ROZMIAR] = tablicowa(pierwsza, druga, ROZMIAR);
    *trzecia = wskaznikowa(pierwsza, trzecia, ROZMIAR);

    for (i = 0; i < ROZMIAR; i++)
        printf("%d ", pierwsza[i]);
    printf("\n");
    for (i = 0; i < ROZMIAR; i++)
        printf("%d ", druga[i]);
    printf("\n");
    for (i = 0; i < ROZMIAR; i++)
        printf("%d ", trzecia[i]);

    return 0;
}

int tablicowa (int tab[], int nowatab[], int rozmiar)
{
    int i;
    for (i = 0; i < rozmiar; i++)
        nowatab[i] = tab [i];
    return nowatab [rozmiar];
}

int wskaznikowa (int *tab, int *nowtab, int rozmiar)
{
    int i;
    for (i = 0; i < rozmiar; i++)
        *(nowtab + i) = *(tab + i);
    return *nowtab;
}

#include <stdio.h>
#include <stdlib.h>
#define ROZMIAR1 2
#define ROZMIAR2 5
int tablicowa (int tab[], int nowatab[], int rozmiar);

int main()
{
    int i, j;
    int wykaz[ROZMIAR1][ROZMIAR2] = {{11, 12, 12, 15, 16}, {90, 91, 98, 97, 94}};
    int nowatab[ROZMIAR1][ROZMIAR2];

    nowatab [0][ROZMIAR2] = tablicowa(wykaz[0], nowatab[0], ROZMIAR2);
    nowatab [1][ROZMIAR2] = tablicowa(wykaz[1], nowatab[1], ROZMIAR2);

    for (i = 0; i < ROZMIAR1; i++)
    {
        for (j = 0; j < ROZMIAR2; j++)
            printf("%d ", wykaz[i][j]);
        printf("\n");
    }
    for (i = 0; i < ROZMIAR1; i++)
    {
        for (j = 0; j < ROZMIAR2; j++)
            printf("%d ", nowatab[i][j]);
        printf("\n");
    }
    return 0;
}

int tablicowa (int tab[], int nowatab[], int rozmiar)
{
    int i;
    for (i = 0; i < rozmiar; i++)
        nowatab[i] = tab [i];
    return nowatab [rozmiar];
}

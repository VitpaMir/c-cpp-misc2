#include <stdio.h>
#include <stdlib.h>
int *getTable(int n);
int *getTableCalloc(int n);
int *getTableRealloc(int n);
void allocTable(int **tab, int n);
int **get2dTable(int row, int col);
void free2d(int **tab, int row);

int main()
{
    int *tab1 = getTableRealloc(10);
    int *tab2;
    allocTable(&tab2, 10);
    int **tab2d = get2dTable(3, 3);
    int i = 0;
    for (i = 0; i < 10; i++)
    {
        tab1[i] = i;
        tab2[i] = i;
        printf("tab1[%d] = %d, tab2[%d] = %d\n", i, tab1[i], i, tab2[i]);
    }
    
    free(tab1);
    free(tab2);
    free2d(tab2d, 3);

    return 0;
}



int *getTable(int n)
{
    return (int*)malloc(n*sizeof(int));
}

int *getTableCalloc(int n)
{
    return (int*)calloc(n, sizeof(n));
}

int *getTableRealloc(int n)
{
    return (int*)realloc(NULL, n*sizeof(int));
}

void allocTable(int **tab, int n)
{
    *tab = (int*)malloc(n*sizeof(int));
}

int **get2dTable(int row, int col)
{
    int **tab = (int**)malloc(row*sizeof(int));
    int i = 0;
    for(i = 0; i < row; i++)
        tab[i] = (int*)malloc(col*sizeof(int));
    return tab;
}

void free2d(int **tab, int row)
{
    int i = 0;
    for (i = 0; i < row; i++)
        free(tab[i]);
    free(tab);
}
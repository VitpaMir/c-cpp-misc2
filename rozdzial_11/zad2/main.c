#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#define MAX 100
#define ROZMIAR 20
void pobierajn (int n, char tablica[]);

int main()
{
    char znaki[MAX];
    pobierajn(ROZMIAR, znaki);
    puts(znaki);
    printf("\n");
    return 0;
}

void pobierajn (int n, char tablica[])
{
    int i = 0;
    char ch;
    while ((i < n) && !isspace(ch) && (ch = getchar()))
    {
        tablica[i] = ch;
        i++;
    }
    tablica[i] = '\0'; // znak zerowy na koncu lancucha zeby puts nie dawal na koncu smieci
}

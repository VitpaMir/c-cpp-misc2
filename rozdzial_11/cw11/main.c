#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#define ODPOWIEDZ "GRANT"
#define MAX 40
void naduze (char *lancuch);
char *znajdzgranta (char *lancuch);

int main()
{
    char proba[MAX];
    char *wsk;
    puts("Kto jest pochowany w grobowcu Granta?");
    gets(proba);
    naduze(proba);
    wsk = znajdzgranta(proba);
    if (wsk != NULL)
        *proba = *wsk;
    while (strcmp(proba, ODPOWIEDZ))
    {
        puts("Niestety to nie jest dobra odpowiedz. Sprobuj jeszcze raz.");
        gets(proba);
        naduze(proba);
        wsk = znajdzgranta(proba);
        if (wsk != NULL)
            *proba = *wsk;
    }
    puts("Tak jest!");
    return 0;
}

void naduze (char *lancuch)
{
    while (*lancuch != '\0')
    {
        *lancuch = toupper(*lancuch);
        lancuch++;
    }

}

char *znajdzgranta (char *lancuch) // nie dziala, funkcja miala wykrywac ciag znakow grant
{                                  // ale trzeba chyba uzyc innych funkcji lub napisac
    //char *wsk;                      to samemu jakimis ifami i forem chyba przesuwajac sie
                                    //po znakach 'g','r','a','n','t' i jesli sa w takim
    while (*lancuch++)              //ciagu to zwrocic grant albo prawde
    {
        if (strstr(lancuch, "grant") != NULL)
        {
            strcpy(lancuch, "grant");
            return lancuch;
        }
    }
    return NULL;
}

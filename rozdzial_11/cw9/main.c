#include <stdio.h>
#include <stdlib.h>
#define STALA "Miroslaw"
int dlugosc_lancucha (char * lancuch);

int main()
{
    char imie[] = "Miroslaw. Gorol. Pin i zielone.";
    int d;
    d = dlugosc_lancucha(imie);
    printf("%d\n", d);
    return 0;
}

int dlugosc_lancucha (char * lancuch)
{
    int i = 0;
    while (*lancuch++)
        i++;
    return i;
}

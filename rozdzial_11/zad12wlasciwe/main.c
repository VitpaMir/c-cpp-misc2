#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    int licznik;

    printf("Wiersz polecen zawiera %d argumentow:\n", argc - 1);
    for (licznik = 1; licznik < argc; licznik++)
        printf("%d: %s\n", licznik, argv[licznik]);
    printf("\n");
    printf("W odwrotnej kolejnosci:\n");
    for (licznik = argc - 1; licznik > 0; licznik--)
        printf("%s ", argv[licznik]);
    printf("\n");
    return 0;
}

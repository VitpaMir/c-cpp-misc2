#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#define TAK 1
#define NIE 0
#define MAX 81
int licz_slowa (char *ch[], int wiersze);
int licz_spacje (char *ch[], int wiersze);
int licz_przest (char *ch[], int wiersze);
int licz_cyfry (char *ch[], int wiersze);
int licz_male (char *ch[], int wiersze);
int licz_duze (char *ch[], int wiersze);

int main()
{
    int i = 0;
    char lancuch[MAX][MAX];
    char *wsklan[MAX];
    while (gets(lancuch[i]) != NULL)
    {
        wsklan[i] = lancuch[i];
        i++;
    }
    printf("Liczba spacji to %d.\n", licz_spacje(wsklan, i));
    printf("Liczba slow to %d.\n", licz_slowa(wsklan, i));
    printf("Liczba znakow przestankowych to %d.\n", licz_przest(wsklan, i));
    printf("Liczba malych liter to: %d.\n", licz_male(wsklan, i));
    printf("Liczba duzych liter to: %d.\n", licz_duze(wsklan, i));
    printf("Liczba cyfr to: %d.\n", licz_cyfry(wsklan, i));

    return 0;
}

int licz_slowa (char *ch[], int wiersze)
{
    int i, j;
    int w_slowie = TAK;
    int slowa = 0;

    for (i = 0; i < wiersze; i++)
    {
        w_slowie = TAK;
        for (j = 0; j < strlen(ch[i]); j++)
        {
            if (w_slowie == TAK && isalpha(ch[i][j]))
            {
                w_slowie = NIE;
                slowa++;
            }
            else if (isspace(ch[i][j]))
                w_slowie = TAK;
        }
    }
    return slowa;
}

int licz_spacje (char *ch[], int wiersze)
{
    int i, j;
    int spacje = 0;

    for (i = 0; i < wiersze; i++)
    {
        for (j = 0; j < strlen(ch[i]); j++)
        {
            if (ch[i][j] == ' ')
                spacje++;
        }
    }
    return spacje;
}

int licz_przest (char *ch[], int wiersze)
{
    int i, j;
    int przest = 0;

    for (i = 0; i < wiersze; i++)
    {
        for (j = 0; j < strlen(ch[i]); j++)
        {
            if (ispunct(ch[i][j]))
                przest++;
        }
    }
    return przest;
}

int licz_cyfry (char *ch[], int wiersze)
{
    int i, j;
    int cyfry = 0;

    for (i = 0; i < wiersze; i++)
    {
        for (j = 0; j < strlen(ch[i]); j++)
        {
            if (isdigit(ch[i][j]))
                cyfry++;
        }
    }
    return cyfry;
}

int licz_male (char *ch[], int wiersze)
{
    int i, j;
    int cyfry = 0;

    for (i = 0; i < wiersze; i++)
    {
        for (j = 0; j < strlen(ch[i]); j++)
        {
            if (islower(ch[i][j]))
                cyfry++;
        }
    }
    return cyfry;
}

int licz_duze (char *ch[], int wiersze)
{
    int i, j;
    int cyfry = 0;

    for (i = 0; i < wiersze; i++)
    {
        for (j = 0; j < strlen(ch[i]); j++)
        {
            if (isupper(ch[i][j]))
                cyfry++;
        }
    }
    return cyfry;
}

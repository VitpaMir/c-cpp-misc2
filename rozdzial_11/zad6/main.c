#include <stdio.h>
#include <stdlib.h>
#include <string.h>
char * kopiuj2 (char *lancuch, const char *lancuch2, int ilosc);

int main()
{
    char lan[] = "Odgadniemy wtedy co bedzie?";
    const char lan2[] = "Odgodniemy kazik co bydzie? Nie wiem..";

    //puts("Wynik dzialania funkcji strncpy");
    //strncpy(lan, lan2, 60);
    //puts(lan);
    puts("Wynik dzialania funkcji kopiuj2:");
    kopiuj2 (lan, lan2, 27);
    puts(lan);
    return 0;
}

char * kopiuj2 (char *lancuch, const char *lancuch2, int ilosc)
{
    int i = 0;
    while (i < ilosc)
    {
        *lancuch = *lancuch2;
        i++;
        lancuch++;
        lancuch2++;
    }
    return lancuch;
}

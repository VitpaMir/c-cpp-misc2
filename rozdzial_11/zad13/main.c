#include <stdio.h>
#include <stdlib.h>
double potega (double podstawa, int wykladnik);

int main(int argc, char *argv[])
{
    int licznik;
    double podstawa;
    int wykladnik;
    double wynik;
    printf("Wiersz polecen zawiera %d argumentow:\n", argc - 1);
    for (licznik = 1; licznik < argc; licznik++)
        printf("%d: %s\n", licznik, argv[licznik]);
    printf("\n");
    printf("Potegowanie: ");
    if (argc < 3 || (podstawa = atof(argv[1])) < 1 || (wykladnik = atoi(argv[2])) < 1)
        printf("Podaj dwie liczby: podstawa double, wykladnik calkowity\n");
    else
    {
        wynik = potega(podstawa, wykladnik);
        printf("%.2lf to wynik.\n", wynik);
    }
    return 0;
}

double potega (double podstawa, int wykladnik)
{
    int n = 1;
    int i;
    for (i = 0; i < wykladnik; i++)
        n *= podstawa;
    return n;
}

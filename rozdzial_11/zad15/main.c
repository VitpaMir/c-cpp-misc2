#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#define MAX 81

int main(int argc, char *argv[])
{
    int j, k, i = 0;
    char dane[MAX][MAX];
    char *wsklan[MAX];
    while (gets(dane[i]) != NULL)
    {
        wsklan[i] = dane[i];
        i++;
    }
    if (strcmp(argv[1], "-p") == 0)
    {
        for (k = 0; k < i; k++)
            puts(wsklan[k]);
    }
    else if (strcmp(argv[1], "-u") == 0)
    {
        for (k = 0; k < i; k++)
        {
            for (j = 0; j < strlen(wsklan[k]); j++)
                printf("%c", toupper(wsklan[k][j]));
            printf("\n");
        }
    }
    else if (strcmp(argv[1], "-l") == 0)
    {
        for (k = 0; k < i; k++)
        {
            for (j = 0; j < strlen(wsklan[k]); j++)
                printf("%c", tolower(wsklan[k][j]));
            printf("\n");
        }
    }
    else
    {
        printf("Prosze podac jako argument, jedna z opcji:\n");
        printf("-p to wyswietlenie danych,\n");
        printf("-u to zamiana wszystkich liter na duze,\n");
        printf("-l to zamiana wszystkich liter na male.\n");
    }
    return 0;
}

#include <stdio.h>
#include <stdlib.h>
#define MAX 100
void pobierajn (char tablica[], int n);

int main()
{
    //int i;
    char znaki[MAX];
    pobierajn(znaki, 20);
    puts(znaki);
    return 0;
}

void pobierajn (char tablica[], int n)
{
    int i = 0;
    char ch;
    while ((i < n) && (ch = getchar())) // gets sie nie nadaje bo dojedzie do konca tablicy
    {                                   // i dopiero wtedy ogarnie zeby konczyc
        tablica[i] = ch;
        i++;
    }
    tablica[i] = '\0';
}

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#define ROZMIAR 81
#define GRAN 10
int menu_wys ();
int pobierz_liczbe ();
void pierwotna_lista (char * lancuch[], int ilosc);
void lista_ascii (char * lancuch[], int num);
void lista_rosnaco (char * lancuch[], int num);
int dl_pierwszego (char lancuch[]);
void najdl_slowo (char * lancuch[], int num);

int main()
{
    char lancuchy[GRAN][ROZMIAR];
    char *wsklan[ROZMIAR];
    int ch;
    int i = 0;
    printf("Podaj najwyzej 10 wierszy (lancuchow znakowych):\n");
    while (i < GRAN && gets(lancuchy[i]) != EOF && lancuchy[i][0] != '\0')
    {
        wsklan[i] = lancuchy[i];
        i++;
    }
    while ((ch = menu_wys()) != '5')
    {
        switch (ch)
        {
            case '1' :  pierwotna_lista(wsklan, i);
                        break;
            case '2' :  lista_ascii(wsklan, i);
                        break;
            case '3' :  lista_rosnaco(wsklan, i);
                        break;
            case '4' :  najdl_slowo(wsklan, i);
                        break;
            default:
                printf("Prosze wpisac od 1 do 5.\n");
        }
    }
    return 0;
}

int menu_wys ()
{
    int ch;
    printf("\n");
    printf("(1) Wyswietlenie pierwotnej listy lancuchow,\n");
    printf("(2) Wyswietlenie lancuchow w porzadku ASCII,\n");
    printf("(3) Wyswietlenie lancuchow wedlug dlugosci (rosnaco),\n");
    printf("(4) Wyswietlenie lancuchow wg dlugosci pierwszego slowa,\n");
    printf("(5) KONIEC.\n");
    printf("\n");
    while ((ch < '1' || ch > '4') && ch != '5')
    {
        printf("Prosze wpisac od 1 do 5.\n");
        ch = pobierz_liczbe();
    }
    return ch;
}

int pobierz_liczbe ()
{
    int ch;

    ch = getchar();
    while (getchar() != '\n')
        continue;
    return ch;
}

void pierwotna_lista (char * lancuch[], int ilosc)
{
    int i;
    for (i = 0; i < ilosc; i++)
        puts(lancuch[i]);
    return;
}

void lista_ascii (char * lancuch[], int num)
{
    int i, j, k = 0;
    char *temp[GRAN];
    char *y[GRAN];

    for(i = 0; i < num; i++)
    {
        y[i] = lancuch[i];
    }

    for(i = 0; i < num; i++)
    {
        for(j = i + 1; j < num - 1; j++)
        {
            for(k = 0; k < num; k++)
            {
                if(y[i][k] > y[j][k])
                {
                    temp[i] = y[i];
                    y[i] = y[j];
                    y[j] = temp[i];
                }
            }
        }

    }

    for(i = 0; i < num; i++)
        printf("%s\n", y[i]);
}

void lista_rosnaco (char * lancuch[], int num)
{
    int i, j;
    char *rosnaco;
    char *rlan[GRAN];
    for (i = 0; i < GRAN; i++)
        rlan[i] = lancuch[i];
    for (i = 0; i < num - 1; i++)
    {
        for (j = i + 1; j < num; j++)
        {
            if (strlen(rlan[i]) > strlen(rlan[j]))
            {
                rosnaco = rlan[i];
                rlan[i] = rlan[j];
                rlan[j] = rosnaco;
            }
        }
    }
    for (i = 0; i < num; i++)
        puts(rlan[i]);
}

void najdl_slowo (char * lancuch[], int num)
{
    int i, j;
    char *rosnaco;
    char *rlan[GRAN];
    for (i = 0; i < GRAN; i++)
        rlan[i] = lancuch[i];
    for (i = 0; i < num - 1; i++)
    {
        for (j = i + 1; j < num; j++)
        {
            if (dl_pierwszego(rlan[i]) > dl_pierwszego(rlan[j]))
            {
                rosnaco = rlan[i];
                rlan[i] = rlan[j];
                rlan[j] = rosnaco;
            }
        }
    }
    for (i = 0; i < num; i++)
        puts(rlan[i]);
}

int dl_pierwszego (char lancuch[])
{
    int k;
    int dlugosc;
    char spacja = ' ';
    for (k = 0; k <= strlen(lancuch); k++)
    {
        if (lancuch[k] == spacja || lancuch[k] == '\0')
        {
            dlugosc = k;
            break;
        }
    }
    return dlugosc;
}
